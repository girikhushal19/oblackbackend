const User = require('../../models/user/User');
const orderModel=require("../../models/orders/order");
const OrganizerModel=require("../../models/events/Organizer");
const NotificationPermissionModel=require("../../models/NotificationPermission");
const Notifications = require("../../models/Notifications");

const {Product } =require("../../models/products/Product");
const subscriptions = require("../../models/subscriptions/Subscription");
const sellerModel=require("../../models/seller/Seller");
const bookingmodel=require("../../models/events/Booking")
const {sendpushnotificationtouser}=require("../../modules/Fcm");
const {savetransactionclient} =require("../../routes/admin/Transaction.controller");
const {sendmail}=require("../../modules/sendmail");
const VariationsPrice =require("../../models/products/VariationPrice");
const OrderModel = require("../../models/orders/order");
const nodemailer = require("nodemailer");
const Event=require("../../models/events/Event");
const mongoose=require("mongoose")
const async = require("async");
const path = require("path");
const CoupensModel=require("../../models/seller/Coupens");
const seller=require("../../models/seller/Seller");
const AdminModel = require("../../models/admin/AdminModel");
 
const STRIPE_PUBLISHABLE_KEY_TEST = process.env.STRIPE_PUBLISHABLE_KEY_TEST;
const paymentkeys=require("../../models/admin/Paymentsettings")
const key = "pk_test_ZYZNOam8KTCxbRXa3RaBSeKk"
const YOUR_DOMAIN = process.env.BASE_URL;
const customConstant = require('../../helpers/customConstant');
let cinetpaykey;


const pdf = require('html-pdf');
const ejs = require("ejs"); 
const fs = require('fs');

let envp = "sandbox";
const setKeys = async () => {
    const cinetpaysettings = await paymentkeys.findOne({ name: "cinetpay" });

    if (cinetpaysettings) {
        if (cinetpaysettings?.currenvtype == "sandbox") {
            envp = "sandbox"
            cinetpaykey = cinetpaysettings.testapikey


        } else if (cinetpaysettings?.currenvtype == "live") {
            envp = "live"
            cinetpaykey = cinetpaysettings.liveapikey


        } else {
            cinetpaykey = process.env.cinetpay_api_key


        }
    } else {
        cinetpaykey = process.env.cinetpay_api_key
    }
}
setKeys();

const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});

module.exports = {
    getPayment: async(req, res) => {
        const amount = req.params.amount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        const order_id = req.params.id;
        const method = req.params.method;
        let user_rec = await User.findOne({_id:user_id});
        let new_user_rec = {
            _id: user_rec._id,
            first_name: user_rec.first_name,
            last_name: user_rec.last_name,
            email: user_rec.email,
            phone: user_rec.phone
        }
        // console.log("new_user_rec " , new_user_rec);
        // return false;
        res.render('admin-panel/cinetpay', {
            key: key,
            amount: amount,
            currency: "eur",
            user_id: user_id,
            order_id:order_id,
            method:method,
            new_user_rec:new_user_rec,
            url: "/api/payment/postStripe",
            base_url: process.env.BASE_URL


        })
    },
    //postPayment ,cinetpaysuccess
    postPayment:async(req,res)=>{

        //651a9b2c1e1fc14b8979285a  /245  /641c3f7deaa2ea2bb402d45b / cart

        // const amount = req.params.amount;
        // const finalamount = req.params.amount;
        // const key = cinetpaykey;
        // const user_id = req.params.user_id;
        // const id = req.params.id;
        // const method = req.params.method;


        // id: '6594f4171b86eff45d75eadc',
        // finalamount: '93.1',
        // promo: 'null',
        // amount: '93.1',
        // transaction_id: 'transaction_id'
        // console.log(req.body);
        // return false;

        const id = req.body.id;
        const finalamount = req.body.finalamount;
        const amount = req.body.amount;
        const method = req.body.method;
        const transaction_id= "transaction_id";
        const promo = 'null';
        // console.log("id",req.body)
        const order=await orderModel.findById(id);
        let admin_record = await AdminModel.findOne({});
        

        const user = await User.findById(order.userId);
        if(!user || !order){
            return res.send({
                status:false,
                message:"L'utilisateur ou l'ordre n'existe pas"
            })
        }

        // console.log("order ", order);
        // console.log("user ", user);
        // return false;
        const productids=order.products?.map((elem)=>mongoose.Types.ObjectId(elem.product._id));
        const newProductsSet=new Set(productids);
        const newProductArray=Array.from(newProductsSet.values());
        console.log("newProductsSet",newProductArray)
        const products=await Product.aggregate([{$match:{_id:{$in:newProductArray}}}]);
        //console.log("products",products)
        const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
        const pathtofileforuser = path.resolve("views/notifications/updateuseraboutorderconfirmation.ejs");
        let pushmessagetouser=``;
        
      if (order) 
      {

        // let provider_id = "";
        // let seller_totoal_profit = 0;
        // let chekc_condition2 = [];
        // let o_ID = id;
        // //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
        // chekc_condition2 = order.products.map((val)=>{
        //     provider_id = val.provider_id;
        //     //console.log("val ",val);
        //     //  return val;
        //     seller_totoal_profit = seller_totoal_profit + val.sellerprofit;
        //     val.is_paid = true;
        //     return val;
        // });
        // // console.log("chekc_condition2 ",chekc_condition2);
        // // console.log("seller_totoal_profit ",seller_totoal_profit);
        // // console.log("provider_id ",provider_id);
        // // console.log("o_ID ",o_ID);
        // const sellerdoc=await seller.findById(provider_id);
        // sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+seller_totoal_profit;
        // sellerdoc.save();
        // await OrderModel.updateOne({_id:o_ID},{products:chekc_condition2});
        // //return false;


        const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.userId });
        let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.teacher_id });
        
        
        
        if(order.products.length>0)
        { 
            
             
            // console.log("order ",order.products[0].variation_id );
            // console.log("order ",order.products[0].variation_value );
            if(order.products[0].variation_id != "" && order.products[0].variation_id != null && order.products[0].variation_id != undefined)
            {
                let vari_rec = await VariationsPrice.findOne({_id:order.products[0].variation_id,variation_option_2:order.products[0].variation_value},{variation_option_2:1,stock:1,price:1});
               // console.log("vari_rec ",vari_rec);
                if(vari_rec)
                {
                    let stock_array = [];
                    stock_array = vari_rec.stock;
                    if(vari_rec.variation_option_2 && vari_rec.price && vari_rec.stock)
                    {
                        if(vari_rec.variation_option_2.length > 0 && vari_rec.price.length > 0 && vari_rec.stock.length > 0)
                        {
                            let indexxx = vari_rec.variation_option_2.indexOf(order.products[0].variation_value);
                            
                            if(vari_rec.price.length >= indexxx && vari_rec.stock.length >= indexxx)
                            {
                                
                                priceee = vari_rec.price[indexxx];
                                quantity = vari_rec.stock[indexxx];
                                if(vari_rec.stock[indexxx] >= order.products[0].quantity)
                                {
                                    let update_quantity = vari_rec.stock[indexxx] - order.products[0].quantity;
                                    if(stock_array.length >= indexxx)
                                    {
                                        stock_array[indexxx] = update_quantity;
                                        //console.log("stock_array ",stock_array);
                                        await VariationsPrice.updateOne({_id:order.products[0].variation_id},{ stock:stock_array });
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }else{
                const pro_rec = await Product.findOne({ _id: order.products[0].product._id });
                //console.log("pro_rec 421 line no payment - controller",pro_rec);
                if (pro_rec)
                {
                    if(pro_rec.stock >=order.products[0].quantity )
                    {
                        pro_rec.stock = pro_rec.stock - order.products[0].quantity;
                        pro_rec.save()
                    }
                    
                }
            }
            for(let m=0; m<order.products.length; m++)
            {
                //console.log("order ",order.products[0] );
                // console.log("provider_id ",order.products[0].provider_id );
                // console.log("part_id ",order.products[0].part_id );
                // console.log("admin_record ",admin_record);
                let part_id = order.products[0].part_id;
                let provider_id = order.products[0].provider_id;

                let message = "Nouvelle commande passée par un utilisateur dont l'ID de commande est - "+part_id;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Nouvelle commande",
                    message:message,
                    status: 'unread',
                    notification_type:"new_order"
                }
                await Notifications.create(data_save);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Nouvelle commande",
                        message:message,
                        status: 'unread',
                        notification_type:"new_order"
                    }
                    await Notifications.create(data_save_1);

                }
            }
        }

        
        // return false;


        //return false;
        //order.payment_status = true;
        order.transaction_id = transaction_id;
        order.date_of_transaction = new Date();
        order.payment_method = "Cinetpay";
        // order.payment_amount = amount;
         //order.promo_code = promo;
        // order.discounted_amount=finalamount;
        order.status=0;
            order.save().then(async (result) => {
                if(method=="cart"){
                    await User.findByIdAndUpdate(order.userId,{
                        cart:[]
                    });
                }
                let messageToUser;
                if(order.products.length>1){
                    messageToUser=`Paiement réussi pour ${result.products[0].title} et plus dans le panier`;
                }else{
                    messageToUser=`Paiement réussi pour ${result.products[0].title} dans le panier`;
                }
                await savetransactionclient(order, "");
                
                const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                // console.log("user.email",user.email)
                if (notipermissionPermissionforUser) {
                    if (notipermissionPermissionforUser.push_reservation) {
                       // sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    } else {
                        console.log("push notification is disabled for user");
                        console.log("line No. 268 ");
                    } if (notipermissionPermissionforUser.email_reservation) {
                        sendmail(pathtofileforuser,
                            { name: user.first_name+" "+user.last_name, message:messageToUser},
                            "Succès du paiement",
                            user.email);
                            console.log("line No. 273 ");
                    } else {
                        console.log("email notification is disabled for user");
                        console.log("line No. 276 ");
                    }
                } else {
                    //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    // sendmail(pathtofileforuser,
                    //     { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //     "Succès du paiement",
                    //     user.email);
                    //     console.log("pathtofileforuser ---->>>>>  " , pathtofileforuser);
                    //     console.log("user.first_name ---->>>>>  " , user.first_name);
                    //     console.log("user.last_name ---->>>>>  " , user.last_name);
                    //     console.log("user.email ---->>>>>  " , user.email);
                    //     console.log("messageToUser ---->>>>>  " , messageToUser);
                    //     console.log("line No. 283 ");
                }
                //console.log("products " , products);
                // console.log("single order 87");
                // console.log("order ",order);


                try{
                    //console.log("req.body.id ", req.body.id);
                    var base_url_server = customConstant.base_url;
                    var imagUrl = base_url_server+"public/uploads/logo.png";
                    //let id = req.body.id;
                    let order_array = [];
                    let seller_array = [];
                    let order_rec_combo = await OrderModel.findOne({_id:id},{combo_id:1});
                    if(order_rec_combo)
                    {
                       if(!order_rec_combo.combo_id)
                       {
                          //here single record
                          //console.log("no combo id  order_rec_combo.combo_id ", order_rec_combo.combo_id);
                          let all_order_combo = await OrderModel.find({_id:id} );
                          //order_array.push(all_order_combo);
                          order_array = all_order_combo;
                       }else{
                          //here multiple record
                          //console.log("order_rec_combo.combo_id ", order_rec_combo.combo_id);
                          let all_order_combo = await OrderModel.find({combo_id:order_rec_combo.combo_id} );
                          //order_array.push(all_order_combo);
                          order_array = all_order_combo;
                       }
                       for(let x=0; x<order_array.length; x++)
                       {
                          console.log(order_array[x]._id);
                          console.log(order_array[x].combo_id);
                          let provider_id = order_array[x].products[0].provider_id;
                          let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
                          seller_array.push(result_seller);
                       }
                       // first_name: 'Edward ',
                       // last_name: 'Auditore',
                       // email: 'edward@mailinator.com',
                       // phone: '0897631256',
                       let userId = order_array[0].userId;
                       let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );
                       //console.log("result_seller ", result_seller);
                       // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
                       const today = new Date(order_array[0].date_of_transaction);
                       const yyyy = today.getFullYear();
                       let mm = today.getMonth() + 1; // Months start at 0!
                       let dd = today.getDate();
                       if (dd < 10) dd = '0' + dd;
                       if (mm < 10) mm = '0' + mm;
                       let formattedToday = dd + '.' + mm + '.' + yyyy;
                       let hourss = today.getHours();
                       let minutess = today.getMinutes();
                       // console.log("hourss ",hourss);
                       // console.log("minutess ",minutess);
                       formattedToday = formattedToday +" "+hourss+"h"+minutess;
                       // // console.log("formattedToday ",formattedToday);
                       // // return false;
                       // invoice=JSON.parse(JSON.stringify(invoice));
                       order_array[0]["frenchdate"]=formattedToday;
                       //console.log(order_array);return false;
                       const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
                       //console.log(filepath)
                       
                        let less_val = order_array.length - 1;
                       const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
                       let subject = "Succès de la commande";
                       var mainOptions = {
                           from:process.env.MAILER_FROM_EMAIL_ID ,
                           to: result_user.email,
                           subject:subject ,
                           html: html
                       };
                       // console.log("html data ======================>", mainOptions);
               
                       //smtpTransport.sendMail(mainOptions, function (err, info) {
                       transport.sendMail(mainOptions, function(err, info) {
                           if (err) {
                               console.log(err);
                               //return true
                           } else {
                               console.log('Message sent: ' + info);
                               //return true
                           }
                       });
                       // return res.send({
                       //    status:true,
                       //    message:"Succès",
                       //    data:order_array
                       // });
                    }
                 }catch(e){
                    console.log(e);
                 }
                 //return false;

                // for(let i=0;i<products.length;i++)
                // {

                    
                //     let product=products[i];
                //     let pushmessagetoprovider=``;
                //     let messageToProvider=``;
                //     let provider=await sellerModel.findById(product.provider_id);
                //     if (notipermissionPermissionforProvider)
                //     {
                //         if (notipermissionPermissionforProvider.push_reservation) {
                //             //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //         } else {
                //             console.log("push notification is disabled for user")
                //         } if (notipermissionPermissionforProvider.email_reservation) {
                //             sendmail(pathtofileforuser,
                //                 { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                //                 "Un nouveau produit a été acheté",
                //                 provider.email);
                //         } else {
                //             console.log("email notification is disabled for user")
                //         }
                //     } else {
                //         //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //         sendmail(pathtofileforuser,
                //             { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                //             "Un nouveau produit a été acheté",
                //             provider.email);
                //     }
                // }
                // coupon code stuff 
                const coupen = await CoupensModel.findOne({ code: promo });
                if (coupen) {
                    coupen.used = coupen.used + 1;
                    coupen.save()
                }


                // var redirect_url_web = customConstant.base_url_user_site+"order_confirm/"+id;
                // console.log("redirect_url_web ",redirect_url_web );
                // //return false;
                // res.redirect(303, redirect_url_web);


                //<%=base_url%>/api/common/paymentsucess/success=true&id=<%=order_id%>
                return res.send({
                    status: true,
                    order_id:id,
                    message: "payemtn success"
                })
            })
        } else {
            return res.send({
                status: false,
                message: "user not found"
            })
        }
    
    },

    cinetpaysuccess:async(req,res)=>{

        //651a9b2c1e1fc14b8979285a  /245  /641c3f7deaa2ea2bb402d45b / cart

        const amount = req.params.amount;
        const finalamount = req.params.finalamount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        const id = req.params.id;
        let method = req.params.method;
        method = "cart";
        const transaction_id= "transaction_id";
        const promo = 'null';

        // console.log(req.params);
        // console.log("amount ------>>>>>> ", req.params.amount);
        // console.log("finalamount ------>>>>>> ", req.params.finalamount);
        // console.log("user_id ------>>>>>> ", req.params.user_id);
        // console.log("id ------>>>>>> ", req.params.id);
        // console.log("method ------>>>>>> ", req.params.method);
        //return false;

        // id: '6594f4171b86eff45d75eadc',
        // finalamount: '93.1',
        // promo: 'null',
        // amount: '93.1',
        // transaction_id: 'transaction_id'
        // console.log(req.body);
        // return false;

        // const id = req.body.id;
        // const finalamount = req.body.finalamount;
        // const amount = req.body.amount;
        // const method = req.body.method;
        // const transaction_id= "transaction_id";
        // const promo = 'null';
        // console.log("id",req.body)
        const order=await orderModel.findById(id);
        let admin_record = await AdminModel.findOne({});
        

        const user = await User.findById(order.userId);
        if(!user || !order){
            return res.send({
                status:false,
                message:"L'utilisateur ou l'ordre n'existe pas"
            })
        }

        // console.log("order ", order);
        // console.log("user ", user);
        // return false;
        const productids=order.products?.map((elem)=>mongoose.Types.ObjectId(elem.product._id));
        const newProductsSet=new Set(productids);
        const newProductArray=Array.from(newProductsSet.values());
        console.log("newProductsSet",newProductArray)
        const products=await Product.aggregate([{$match:{_id:{$in:newProductArray}}}]);
        //console.log("products",products)
        const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
        const pathtofileforuser = path.resolve("views/notifications/updateuseraboutorderconfirmation.ejs");
        let pushmessagetouser=``;
        
      if (order) 
      {

        // let provider_id = "";
        // let seller_totoal_profit = 0;
        // let chekc_condition2 = [];
        // let o_ID = id;
        // //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
        // chekc_condition2 = order.products.map((val)=>{
        //     provider_id = val.provider_id;
        //     //console.log("val ",val);
        //     //  return val;
        //     seller_totoal_profit = seller_totoal_profit + val.sellerprofit;
        //     val.is_paid = true;
        //     return val;
        // });
        // // console.log("chekc_condition2 ",chekc_condition2);
        // // console.log("seller_totoal_profit ",seller_totoal_profit);
        // // console.log("provider_id ",provider_id);
        // // console.log("o_ID ",o_ID);
        // const sellerdoc=await seller.findById(provider_id);
        // sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+seller_totoal_profit;
        // sellerdoc.save();
        // await OrderModel.updateOne({_id:o_ID},{products:chekc_condition2});
        // //return false;


        const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.userId });
        let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.teacher_id });
        
        
        
        if(order.products.length>0)
        { 
            
             
            // console.log("order ",order.products[0].variation_id );
            // console.log("order ",order.products[0].variation_value );
            if(order.products[0].variation_id != "" && order.products[0].variation_id != null && order.products[0].variation_id != undefined)
            {
                let vari_rec = await VariationsPrice.findOne({_id:order.products[0].variation_id,variation_option_2:order.products[0].variation_value},{variation_option_2:1,stock:1,price:1});
               // console.log("vari_rec ",vari_rec);
                if(vari_rec)
                {
                    let stock_array = [];
                    stock_array = vari_rec.stock;
                    if(vari_rec.variation_option_2 && vari_rec.price && vari_rec.stock)
                    {
                        if(vari_rec.variation_option_2.length > 0 && vari_rec.price.length > 0 && vari_rec.stock.length > 0)
                        {
                            let indexxx = vari_rec.variation_option_2.indexOf(order.products[0].variation_value);
                            
                            if(vari_rec.price.length >= indexxx && vari_rec.stock.length >= indexxx)
                            {
                                
                                priceee = vari_rec.price[indexxx];
                                quantity = vari_rec.stock[indexxx];
                                if(vari_rec.stock[indexxx] >= order.products[0].quantity)
                                {
                                    let update_quantity = vari_rec.stock[indexxx] - order.products[0].quantity;
                                    if(stock_array.length >= indexxx)
                                    {
                                        stock_array[indexxx] = update_quantity;
                                        //console.log("stock_array ",stock_array);
                                        await VariationsPrice.updateOne({_id:order.products[0].variation_id},{ stock:stock_array });
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }else{
                const pro_rec = await Product.findOne({ _id: order.products[0].product._id });
                //console.log("pro_rec 421 line no payment - controller",pro_rec);
                if (pro_rec)
                {
                    if(pro_rec.stock >=order.products[0].quantity )
                    {
                        pro_rec.stock = pro_rec.stock - order.products[0].quantity;
                        pro_rec.save()
                    }
                    
                }
            }
            for(let m=0; m<order.products.length; m++)
            {
                //console.log("order ",order.products[0] );
                // console.log("provider_id ",order.products[0].provider_id );
                // console.log("part_id ",order.products[0].part_id );
                // console.log("admin_record ",admin_record);
                let part_id = order.products[0].part_id;
                let provider_id = order.products[0].provider_id;

                let message = "Nouvelle commande passée par un utilisateur dont l'ID de commande est - "+part_id;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Nouvelle commande",
                    message:message,
                    status: 'unread',
                    notification_type:"new_order"
                }
                await Notifications.create(data_save);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Nouvelle commande",
                        message:message,
                        status: 'unread',
                        notification_type:"new_order"
                    }
                    await Notifications.create(data_save_1);

                }
            }
        }

        
        // return false;


        //return false;
        order.payment_status = true;
        order.transaction_id = transaction_id;
        order.date_of_transaction = new Date();
        order.payment_method = "Cinetpay";
        // order.payment_amount = amount;
         //order.promo_code = promo;
        // order.discounted_amount=finalamount;
        order.status=0;
            order.save().then(async (result) => {
                // console.log("hereeee 679 ");
                // return false;
                if(method=="cart"){
                    await User.findByIdAndUpdate(order.userId,{
                        cart:[]
                    });
                }
                let messageToUser;
                if(order.products.length>1){
                    messageToUser=`Paiement réussi pour ${result.products[0].title} et plus dans le panier`;
                }else{
                    messageToUser=`Paiement réussi pour ${result.products[0].title} dans le panier`;
                }
                await savetransactionclient(order, "");
                
                const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                // console.log("user.email",user.email)
                if (notipermissionPermissionforUser) {
                    if (notipermissionPermissionforUser.push_reservation) {
                       // sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    } else {
                        console.log("push notification is disabled for user");
                        console.log("line No. 268 ");
                    } if (notipermissionPermissionforUser.email_reservation) {
                        sendmail(pathtofileforuser,
                            { name: user.first_name+" "+user.last_name, message:messageToUser},
                            "Succès du paiement",
                            user.email);
                            console.log("line No. 273 ");
                    } else {
                        console.log("email notification is disabled for user");
                        console.log("line No. 276 ");
                    }
                } else {
                    //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    // sendmail(pathtofileforuser,
                    //     { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //     "Succès du paiement",
                    //     user.email);
                    //     console.log("pathtofileforuser ---->>>>>  " , pathtofileforuser);
                    //     console.log("user.first_name ---->>>>>  " , user.first_name);
                    //     console.log("user.last_name ---->>>>>  " , user.last_name);
                    //     console.log("user.email ---->>>>>  " , user.email);
                    //     console.log("messageToUser ---->>>>>  " , messageToUser);
                    //     console.log("line No. 283 ");
                }
                //console.log("products " , products);
                // console.log("single order 87");
                // console.log("order ",order);


                try{
                    //console.log("req.body.id ", req.body.id);
                    var base_url_server = customConstant.base_url;
                    var imagUrl = base_url_server+"public/uploads/logo.png";
                    //let id = req.body.id;
                    let order_array = [];
                    let seller_array = [];
                    let order_rec_combo = await OrderModel.findOne({_id:id},{combo_id:1});
                    if(order_rec_combo)
                    {
                       if(!order_rec_combo.combo_id)
                       {
                          //here single record
                          //console.log("no combo id  order_rec_combo.combo_id ", order_rec_combo.combo_id);
                          let all_order_combo = await OrderModel.find({_id:id} );
                          //order_array.push(all_order_combo);
                          order_array = all_order_combo;
                       }else{
                          //here multiple record
                          //console.log("order_rec_combo.combo_id ", order_rec_combo.combo_id);
                          let all_order_combo = await OrderModel.find({combo_id:order_rec_combo.combo_id} );
                          //order_array.push(all_order_combo);
                          order_array = all_order_combo;
                       }
                       for(let x=0; x<order_array.length; x++)
                       {
                          console.log(order_array[x]._id);
                          console.log(order_array[x].combo_id);
                          let provider_id = order_array[x].products[0].provider_id;
                          let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
                          seller_array.push(result_seller);
                       }
                       // first_name: 'Edward ',
                       // last_name: 'Auditore',
                       // email: 'edward@mailinator.com',
                       // phone: '0897631256',
                       let userId = order_array[0].userId;
                       let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );
                       //console.log("result_seller ", result_seller);
                       // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
                       const today = new Date(order_array[0].date_of_transaction);
                       const yyyy = today.getFullYear();
                       let mm = today.getMonth() + 1; // Months start at 0!
                       let dd = today.getDate();
                       if (dd < 10) dd = '0' + dd;
                       if (mm < 10) mm = '0' + mm;
                       let formattedToday = dd + '.' + mm + '.' + yyyy;
                       let hourss = today.getHours();
                       let minutess = today.getMinutes();
                       // console.log("hourss ",hourss);
                       // console.log("minutess ",minutess);
                       formattedToday = formattedToday +" "+hourss+"h"+minutess;
                       // // console.log("formattedToday ",formattedToday);
                       // // return false;
                       // invoice=JSON.parse(JSON.stringify(invoice));
                       order_array[0]["frenchdate"]=formattedToday;
                       //console.log(order_array);return false;
                       const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
                       //console.log(filepath)
                       
                        let less_val = order_array.length - 1;
                       const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
                       let subject = "Succès de la commande";
                       var mainOptions = {
                           from:process.env.MAILER_FROM_EMAIL_ID ,
                           to: result_user.email,
                           subject:subject ,
                           html: html
                       };
                       // console.log("html data ======================>", mainOptions);
               
                       //smtpTransport.sendMail(mainOptions, function (err, info) {
                       transport.sendMail(mainOptions, function(err, info) {
                           if (err) {
                               console.log(err);
                               //return true
                           } else {
                               console.log('Message sent: ' + info);
                               //return true
                           }
                       });
                       // return res.send({
                       //    status:true,
                       //    message:"Succès",
                       //    data:order_array
                       // });
                    }
                 }catch(e){
                    console.log(e);
                 }
                 //return false;

                // for(let i=0;i<products.length;i++)
                // {

                    
                //     let product=products[i];
                //     let pushmessagetoprovider=``;
                //     let messageToProvider=``;
                //     let provider=await sellerModel.findById(product.provider_id);
                //     if (notipermissionPermissionforProvider)
                //     {
                //         if (notipermissionPermissionforProvider.push_reservation) {
                //             //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //         } else {
                //             console.log("push notification is disabled for user")
                //         } if (notipermissionPermissionforProvider.email_reservation) {
                //             sendmail(pathtofileforuser,
                //                 { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                //                 "Un nouveau produit a été acheté",
                //                 provider.email);
                //         } else {
                //             console.log("email notification is disabled for user")
                //         }
                //     } else {
                //         //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //         sendmail(pathtofileforuser,
                //             { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                //             "Un nouveau produit a été acheté",
                //             provider.email);
                //     }
                // }
                // coupon code stuff 
                const coupen = await CoupensModel.findOne({ code: promo });
                if (coupen) {
                    coupen.used = coupen.used + 1;
                    coupen.save()
                }


                var redirect_url_web = customConstant.base_url_user_site+"order_confirm/"+id;
                console.log("redirect_url_web ",redirect_url_web );
                //return false;
                res.redirect(303, redirect_url_web);


                //<%=base_url%>/api/common/paymentsucess/success=true&id=<%=order_id%>
                // return res.send({
                //     status: true,
                //     order_id:id,
                //     message: "payemtn success"
                // })
            })
        } else {
            return res.send({
                status: false,
                message: "user not found"
            })
        }
    
    },
    getPaymentForSub: async(req, res) => {
        const amount = req.params.amount;
        const key = cinetpaykey;
        const sub_id = req.params.id;
        
        const sub = await subscriptions.findById(sub_id);
        //console.log(sub);return false;
        const user_rec = await sellerModel.findById(sub.provider_id);

        let new_user_rec = {
            _id: user_rec._id,
            fullname: user_rec.fullname,
            email: user_rec.email,
            phone: user_rec.phone
        }
        //  console.log("new_user_rec " , new_user_rec);
        //  return false;
        res.render('admin-panel/cinetpayForSub', {
            key: key,
            amount: amount,
            currency: "eur",
            sub_id:sub_id,
            new_user_rec:new_user_rec,
            url: "/api/common/postPaymentForSub",
            base_url: process.env.BASE_URL


        })
    },

    sucessSellerSubscription: async(req, res) => {
        const id = req.params.id;
        
        const amount = req.params.amount;
        
        const transaction_id=req.params.transaction_id;
        // console.log("id",req.body)
        const sub=await subscriptions.findById(id);
        //console.log(sub);return false;
        const seller = await sellerModel.findById(sub.provider_id);
        if(!sub || !seller){
            return res.send({
                status:false,
                message:"L'utilisateur ou l'abonnement n'existe pas"
            })
        }
        
        const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
        
        let pushmessagetouser=``;
        
      if (sub) {
        // console.log("seller ",seller);
        // seller.sub_id=sub._id;
        //     seller.save();
        //mongoose.set("debug",true);
        await sellerModel.updateOne({_id:sub.provider_id},{sub_id:sub._id,vendortype:sub.name_of_package});
        //return false;
        let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: sub.provider_id });
        
        sub.payment_status = true;
        sub.transaction_id = transaction_id;
        sub.date_of_transaction = new Date();
        sub.payment_method = "Cinetpay";
        sub.payment_amount = amount;
        sub.save().then(async (result) => {
            
            
            await savetransactionclient(sub, "");
            
            const message = process.env.MESSAGE_PAYMENT_SUCCESS;
            const messageToUser="somethinb"
            // console.log("user.email",user.email)
            if (notipermissionPermissionforProvider) {
                if (notipermissionPermissionforProvider.push_reservation) {
                    sendpushnotificationtouser(message, seller, seller._id);
                } else {
                    console.log("push notification is disabled for user")
                } if (notipermissionPermissionforProvider.email_reservation) {
                    sendmail(pathtofileforprovider,
                        { name: seller.first_name+" "+seller.last_name, message:messageToUser},
                        "Succès du paiement",
                        seller.email);
                } else {
                    console.log("email notification is disabled for user")
                }
            } else {
                sendpushnotificationtouser(message, seller, seller._id);
                sendmail(pathtofileforprovider,
                    { name: seller.first_name+" "+seller.last_name, message:messageToUser},
                    "Succès du paiement",
                    seller.email);
            }
            
        })
        return res.send({
            status: true,
            message: "payment success",
        })
        // return res.redirect("/api/common/paymentsucess/success=true");
        } else {
            return res.send({
                status: false,
                message: "user not found"
            })
        }
    },
    postPaymentForSub: async(req, res) => {
        const id = req.body.id;
        
        const amount = req.body.amount;
        
        const transaction_id=req.body.transaction_id;
        // console.log("id",req.body)
        const sub=await subscriptions.findById(id);
        //console.log(sub);return false;
        const seller = await sellerModel.findById(sub.provider_id);
        if(!sub || !seller){
            return res.send({
                status:false,
                message:"L'utilisateur ou l'abonnement n'existe pas"
            })
        }
        
        const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
        
        let pushmessagetouser=``;
        
      if (sub) {
        // console.log("seller ",seller);
        // seller.sub_id=sub._id;
        //     seller.save();
        //mongoose.set("debug",true);
        await sellerModel.updateOne({_id:sub.provider_id},{sub_id:sub._id,vendortype:sub.name_of_package});
        //return false;
        let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: sub.provider_id });
        
        sub.payment_status = true;
        sub.transaction_id = transaction_id;
        sub.date_of_transaction = new Date();
        sub.payment_method = "Cinetpay";
        sub.payment_amount = amount;
        sub.save().then(async (result) => {
            
            
            await savetransactionclient(sub, "");
            
            const message = process.env.MESSAGE_PAYMENT_SUCCESS;
            const messageToUser="somethinb"
            // console.log("user.email",user.email)
            if (notipermissionPermissionforProvider) {
                if (notipermissionPermissionforProvider.push_reservation) {
                    sendpushnotificationtouser(message, seller, seller._id);
                } else {
                    console.log("push notification is disabled for user")
                } if (notipermissionPermissionforProvider.email_reservation) {
                    sendmail(pathtofileforprovider,
                        { name: seller.first_name+" "+seller.last_name, message:messageToUser},
                        "Succès du paiement",
                        seller.email);
                } else {
                    console.log("email notification is disabled for user")
                }
            } else {
                sendpushnotificationtouser(message, seller, seller._id);
                sendmail(pathtofileforprovider,
                    { name: seller.first_name+" "+seller.last_name, message:messageToUser},
                    "Succès du paiement",
                    seller.email);
            }
            
        })
        return res.send({
            status: true,
            message: "payment success",
        })
        // return res.redirect("/api/common/paymentsucess/success=true");
        } else {
            return res.send({
                status: false,
                message: "user not found"
            })
        }
    },
    paymentsucess:(req,res)=>{
        var idd = "";
        var typeeee = "";
        console.log("req.query");
        console.log(req.params.payment_status);
        console.log(req.params.payment_status.success);
        if(req.params.payment_status)
        { }
            let aa = req.params.payment_status.split("&");
            if(aa)
            {
                
                if(aa.length > 1)
                {
                    //console.log(aa[1]);
                    idd = aa[1].replace("id=","");
                    //console.log(idd);
                }
                if(aa.length > 2)
                {
                    console.log(aa[2]);
                    typeeee = aa[2].replace("type=","");
                    //console.log(idd);
                }
            }
            // console.log("typeeee ",typeeee);
            // console.log(aa );return false;
        
        var urlll = customConstant.base_url_user_site;
        //console.log("urlll ",urlll );return false;
        if(!typeeee)
        {
            var redirect_url_web = customConstant.base_url_user_site+"order_confirm/"+idd;
        }else{
            var redirect_url_web = customConstant.base_url_user_site+"order_confirm_combo/"+idd;
        }
        console.log("redirect_url_web ",redirect_url_web );
        //return false;
        res.redirect(303, redirect_url_web);
        // res.render('admin-panel/stripeSuccess', {
        //     // i18n: res,
        //     title: "Stripe succuss",
        //     succuss: true
        // })
    },
    paymentsucessEvent:(req,res)=>{
        
        var urlll = customConstant.base_url_user_site;
        var redirect_url_web = urlll+"event_confirm/";
        
        res.redirect(303, redirect_url_web);
        // res.render('admin-panel/stripeSuccess', {
        //     // i18n: res,
        //     title: "Stripe succuss",
        //     succuss: true
        // })
    },

    paymentsucess_sub:(req,res)=>{
        
        var urlll = customConstant.base_url_seller_site;
        var redirect_url_web = urlll+"payment_confirm/";
        
        res.redirect(303, redirect_url_web);
        // res.render('admin-panel/stripeSuccess', {
        //     // i18n: res,
        //     title: "Stripe succuss",
        //     succuss: true
        // })
    },
    getpaymentforcombo: async(req, res) => {
        const amount = req.params.amount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        const order_id = req.params.id;
        const method = req.params.method;
        let user_rec = await User.findOne({_id:user_id});
        let new_user_rec = {
            _id: user_rec._id,
            first_name: user_rec.first_name,
            last_name: user_rec.last_name,
            email: user_rec.email,
            phone: user_rec.phone
        }

        res.render('admin-panel/cinetpayforcombo', {
            key: key,
            new_user_rec:new_user_rec,
            amount: amount,
            currency: "eur",
            user_id: user_id,
            order_id:order_id,
            method:method,
            url: "/api/payment/postPaymentforcombo",
            base_url: process.env.BASE_URL


        })
    },
    cinetpaysuccessCombo: async(req, res) => {
        // const id = req.body.id;
        // const finalamount = req.body.finalamount;
        // const promo = req.body.promo;
        // const amount = req.body.amount;
        // const method = req.body.method;
        const transaction_id= "transaction_id";
        const promo = "";
        const finalamount = req.params.amount;
        const amount = req.params.amount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        const id = req.params.id;
        const method = req.params.method;

        // {
        //     id: 'LJOcnlUADNGFD0jcU2WX',
        //     user_id: '64d4de8eb561063024242531',
        //     finalamount: '1343.1',
        //     promo: 'null'
        //   }

        // console.log(req.params);return false;

        let admin_record = await AdminModel.findOne({});
        const allorders=await orderModel.find({combo_id:id});



        try{
            //console.log("req.body.id ", req.body.id);
            var base_url_server = customConstant.base_url;
            var imagUrl = base_url_server+"public/uploads/logo.png";
           // let id = req.body.id;
            let order_array = [];
            let seller_array = [];
            let order_rec_combo = await OrderModel.findOne({combo_id:id},{combo_id:1});

            if(order_rec_combo)
            {
               if(!order_rec_combo.combo_id)
               {
                  //here single record
                  //console.log("no combo id  order_rec_combo.combo_id ", order_rec_combo.combo_id);
                  let all_order_combo = await OrderModel.find({_id:id} );
                  //order_array.push(all_order_combo);
                  order_array = all_order_combo;
               }else{
                  //here multiple record
                  //console.log("order_rec_combo.combo_id ", order_rec_combo.combo_id);
                  let all_order_combo = await OrderModel.find({combo_id:order_rec_combo.combo_id} );
                  //order_array.push(all_order_combo);
                  order_array = all_order_combo;
               }
               for(let x=0; x<order_array.length; x++)
               {
                  console.log(order_array[x]._id);
                  console.log(order_array[x].combo_id);
                  let provider_id = order_array[x].products[0].provider_id;
                  let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
                  seller_array.push(result_seller);
               }
               // first_name: 'Edward ',
               // last_name: 'Auditore',
               // email: 'edward@mailinator.com',
               // phone: '0897631256',
               let userId = order_array[0].userId;
               let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );
               //console.log("result_seller ", result_seller);
               // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
               const today = new Date(order_array[0].date_of_transaction);
               const yyyy = today.getFullYear();
               let mm = today.getMonth() + 1; // Months start at 0!
               let dd = today.getDate();
               if (dd < 10) dd = '0' + dd;
               if (mm < 10) mm = '0' + mm;
               let formattedToday = dd + '.' + mm + '.' + yyyy;
               let hourss = today.getHours();
               let minutess = today.getMinutes();
               // console.log("hourss ",hourss);
               // console.log("minutess ",minutess);
               formattedToday = formattedToday +" "+hourss+"h"+minutess;
               // // console.log("formattedToday ",formattedToday);
               // // return false;
               // invoice=JSON.parse(JSON.stringify(invoice));
               order_array[0]["frenchdate"]=formattedToday;
               //console.log(order_array);return false;
               const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
               //console.log(filepath)
               let less_val = order_array.length - 1;
               const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
               let subject = "Succès de la commande";
               var mainOptions = {
                   from:process.env.MAILER_FROM_EMAIL_ID ,
                   to: result_user.email,
                   subject:subject ,
                   html: html
               };
               // console.log("html data ======================>", mainOptions);
               console.log("hereeeeeeee 746");

               //smtpTransport.sendMail(mainOptions, function (err, info) {
               transport.sendMail(mainOptions, function(err, info) {
                   if (err) {
                       console.log(err);
                       //return true
                   } else {
                       console.log('Message sent: ' + info);
                       //return true
                   }
               });
               // return res.send({
               //    status:true,
               //    message:"Succès",
               //    data:order_array
               // });
            }
         }catch(e){
            console.log(e);
         }

        // console.log("multiple order 87");
        // console.log("allorders ", allorders);
        // return false;

        //console.log("allorders ", allorders);
        //return false;


        for(let i=0;i<allorders.length;i++)
        {
           let order=allorders[i];
            //    const order=await orderModel.findById(id);
            const user = await User.findById(order.userId);
            if(!user || !order){
                return res.send({
                status:false,
                message:"user or order does not exists"
                })
            }

            // let provider_id = "";
            // let seller_totoal_profit = 0;
            // let chekc_condition2 = [];
            // let o_ID = order._id;
            // //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
            // chekc_condition2 = order.products.map((val)=>{
            //     provider_id = val.provider_id;
            //     //console.log("val ",val);
            //     //  return val;
            //     seller_totoal_profit = seller_totoal_profit + val.sellerprofit;
            //     val.is_paid = true;
            //     return val;
            // });
            // // console.log("chekc_condition2 ",chekc_condition2);
            // // console.log("seller_totoal_profit ",seller_totoal_profit);
            // // console.log("provider_id ",provider_id);
            // // console.log("o_ID ",o_ID);
            // const sellerdoc=await seller.findById(provider_id);
            // sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+seller_totoal_profit;
            // sellerdoc.save();
            // await OrderModel.updateOne({_id:o_ID},{products:chekc_condition2});

            for(let m=0; m<order.products.length; m++)
            {
                //console.log("order ",order.products[0] );
                // console.log("provider_id ",order.products[0].provider_id );
                // console.log("part_id ",order.products[0].part_id );
                // console.log("admin_record ",admin_record);
                let part_id = order.products[0].part_id;
                let provider_id = order.products[0].provider_id;

                let message = "Nouvelle commande passée par un utilisateur dont l'ID de commande est - "+part_id;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Nouvelle commande",
                    message:message,
                    status: 'unread',
                    notification_type:"new_order"
                }
                await Notifications.create(data_save);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Nouvelle commande",
                        message:message,
                        status: 'unread',
                        notification_type:"new_order"
                    }
                    await Notifications.create(data_save_1);

                }
            }

            const productids=order.products?.map((elem)=>mongoose.Types.ObjectId(elem.product._id));
            const newProductsSet=new Set(productids);
            const newProductArray=Array.from(newProductsSet.values());
            //console.log("newProductsSet",newProductArray)
            const products=await Product.aggregate([{$match:{_id:{$in:newProductArray}}}]);
            //console.log("products",products)
            const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
            const pathtofileforuser = path.resolve("views/notifications/updateuseraboutorderconfirmation.ejs");
            let pushmessagetouser=``;
            
            if (order)
            {
                // console.log("order ",order );
                // console.log("========================>>>>>>>>>>>>>>>>>>>");
                // console.log("order products",order.products);
                if(order.products.length>0)
                { 
                    // console.log("order ",order.products[0] );
                    // console.log("order ",order.products[0].variation_id );
                    // console.log("order ",order.products[0].variation_value );
                    if(order.products[0].variation_id != "" && order.products[0].variation_id != null && order.products[0].variation_id != undefined)
                    {
                        let vari_rec = await VariationsPrice.findOne({_id:order.products[0].variation_id,variation_option_2:order.products[0].variation_value},{variation_option_2:1,stock:1,price:1});
                        //console.log("vari_rec ",vari_rec);
                        if(vari_rec)
                        {
                            let stock_array = [];
                            stock_array = vari_rec.stock;
                            if(vari_rec.variation_option_2 && vari_rec.price && vari_rec.stock)
                            {
                                if(vari_rec.variation_option_2.length > 0 && vari_rec.price.length > 0 && vari_rec.stock.length > 0)
                                {
                                    let indexxx = vari_rec.variation_option_2.indexOf(order.products[0].variation_value);
                                    
                                    if(vari_rec.price.length >= indexxx && vari_rec.stock.length >= indexxx)
                                    {
                                        
                                        priceee = vari_rec.price[indexxx];
                                        quantity = vari_rec.stock[indexxx];
                                        if(vari_rec.stock[indexxx] >= order.products[0].quantity)
                                        {
                                            let update_quantity = vari_rec.stock[indexxx] - order.products[0].quantity;
                                            if(stock_array.length >= indexxx)
                                            {
                                                stock_array[indexxx] = update_quantity;
                                                //console.log("stock_array ",stock_array);
                                                await VariationsPrice.updateOne({_id:order.products[0].variation_id},{ stock:stock_array });
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        const pro_rec = await Product.findOne({ _id: order.products[0].product._id });
                        //console.log("pro_rec 421 line no payment - controller",pro_rec);
                        if (pro_rec)
                        {
                            if(pro_rec.stock >=order.products[0].quantity )
                            {
                                pro_rec.stock = pro_rec.stock - order.products[0].quantity;
                                pro_rec.save()
                            }
                            
                        }
                    }
                }
                

                //return false;
                const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.userId });
                let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.teacher_id });
                
                order.payment_status = true;
                order.transaction_id = transaction_id;
                order.date_of_transaction = new Date();
                order.payment_method = "Cinetpay";
                // order.payment_amount = amount;
                // order.promo_code = promo;
                // order.discounted_amount=finalamount;
                order.status=0;
                order.save().then(async (result) => {
                    //console.log("method",method)
                    if(method=="cart"){
                        await User.findByIdAndUpdate(order.userId,{
                            cart:[]
                        });
                    }
                    let messageToUser;
                    if(order.products.length>1){
                        messageToUser=`Paiement réussi pour ${result.products[0].title} et plus dans le panier`;
                    }else{
                        messageToUser=`Paiement réussi pour ${result.products[0].title} dans le panier`;
                    }
                    await savetransactionclient(order, "");
                    
                    const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                    // console.log("user.email",user.email)
                    // if (notipermissionPermissionforUser) {
                    //     if (notipermissionPermissionforUser.push_reservation) {
                    //         //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    //     } else {
                    //         console.log("push notification is disabled for user")
                    //     } if (notipermissionPermissionforUser.email_reservation) {
                    //         sendmail(pathtofileforuser,
                    //             { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //             "Succès du paiement",
                    //             user.email);
                    //     } else {
                    //         console.log("email notification is disabled for user")
                    //     }
                    // } else {
                    //     //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    //     sendmail(pathtofileforuser,
                    //         { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //         "Succès du paiement",
                    //         user.email);
                    // }
                    // for(let i=0;i<products.length;i++){
                    //     let product=products[i];
                    //     let pushmessagetoprovider=``;
                    //     let messageToProvider=``;
                    //     let provider=await sellerModel.findById(product.provider_id);
                    //     if (notipermissionPermissionforProvider) {
                    //         if (notipermissionPermissionforProvider.push_reservation) {
                    //            // sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                    //         } else {
                    //             console.log("push notification is disabled for user")
                    //         } if (notipermissionPermissionforProvider.email_reservation) {
                    //             sendmail(pathtofileforuser,
                    //                 { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                    //                 "Un nouveau produit a été acheté",
                    //                 provider.email);
                    //         } else {
                    //             console.log("email notification is disabled for user")
                    //         }
                    //     } else {
                    //         //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                    //         sendmail(pathtofileforuser,
                    //             { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                    //             "Un nouveau produit a été acheté",
                    //             provider.email);
                    //     }
                    // }
                    // coupon code stuff 
                    const coupen = await CoupensModel.findOne({ code: promo });
                    if (coupen) {
                        coupen.used = coupen.used + 1;
                        coupen.save()
                    }
                    

                    

                })

                
            } else {
                return res.send({
                    status: false,
                    message: "user not found"
                })
            }
        }
        var redirect_url_web = customConstant.base_url_user_site+"order_confirm_combo/"+id;
        console.log("redirect_url_web ",redirect_url_web );
        //return false;
        res.redirect(303, redirect_url_web);
        //return false;
        // return res.send({
        //     status: true,
        //     message: "payemtn success"
        // })
    },
    //postPaymentforcombo
    postPaymentforcombo: async(req, res) => {
        // const id = req.body.id;
        // const finalamount = req.body.finalamount;
        // const promo = req.body.promo;
        // const amount = req.body.amount;
        // const method = req.body.method;
        const transaction_id= "transaction_id";
        const promo = "";
        const finalamount = req.params.amount;
        const amount = req.params.amount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        const id = req.params.id;
        const method = req.params.method;


        let admin_record = await AdminModel.findOne({});
        const allorders=await orderModel.find({combo_id:id});



        try{
            //console.log("req.body.id ", req.body.id);
            var base_url_server = customConstant.base_url;
            var imagUrl = base_url_server+"public/uploads/logo.png";
           // let id = req.body.id;
            let order_array = [];
            let seller_array = [];
            let order_rec_combo = await OrderModel.findOne({combo_id:id},{combo_id:1});

            if(order_rec_combo)
            {
               if(!order_rec_combo.combo_id)
               {
                  //here single record
                  //console.log("no combo id  order_rec_combo.combo_id ", order_rec_combo.combo_id);
                  let all_order_combo = await OrderModel.find({_id:id} );
                  //order_array.push(all_order_combo);
                  order_array = all_order_combo;
               }else{
                  //here multiple record
                  //console.log("order_rec_combo.combo_id ", order_rec_combo.combo_id);
                  let all_order_combo = await OrderModel.find({combo_id:order_rec_combo.combo_id} );
                  //order_array.push(all_order_combo);
                  order_array = all_order_combo;
               }
               for(let x=0; x<order_array.length; x++)
               {
                  console.log(order_array[x]._id);
                  console.log(order_array[x].combo_id);
                  let provider_id = order_array[x].products[0].provider_id;
                  let result_seller = await sellerModel.findOne({_id:provider_id},{fullname:1,shopname:1,email:1, countrycode:1, phone:1,pickupaddress:1} );
                  seller_array.push(result_seller);
               }
               // first_name: 'Edward ',
               // last_name: 'Auditore',
               // email: 'edward@mailinator.com',
               // phone: '0897631256',
               let userId = order_array[0].userId;
               let result_user = await User.findOne({_id:userId},{first_name:1,last_name:1,email:1, country_code:1, phone:1 } );
               //console.log("result_seller ", result_seller);
               // const frenchdate=frenchDayDate(order_array[0].date_of_transaction);
               const today = new Date(order_array[0].date_of_transaction);
               const yyyy = today.getFullYear();
               let mm = today.getMonth() + 1; // Months start at 0!
               let dd = today.getDate();
               if (dd < 10) dd = '0' + dd;
               if (mm < 10) mm = '0' + mm;
               let formattedToday = dd + '.' + mm + '.' + yyyy;
               let hourss = today.getHours();
               let minutess = today.getMinutes();
               // console.log("hourss ",hourss);
               // console.log("minutess ",minutess);
               formattedToday = formattedToday +" "+hourss+"h"+minutess;
               // // console.log("formattedToday ",formattedToday);
               // // return false;
               // invoice=JSON.parse(JSON.stringify(invoice));
               order_array[0]["frenchdate"]=formattedToday;
               //console.log(order_array);return false;
               const filepath=path.join(__dirname, "../../views/admin-panel/paymentinvoice_3.ejs");
               //console.log(filepath)
               let less_val = order_array.length - 1;
               const html = await ejs.renderFile(filepath,{order_array:order_array,result_user:result_user,seller_array:seller_array,imagUrl:imagUrl,less_val:less_val });
               let subject = "Succès de la commande";
               var mainOptions = {
                   from:process.env.MAILER_FROM_EMAIL_ID ,
                   to: result_user.email,
                   subject:subject ,
                   html: html
               };
               // console.log("html data ======================>", mainOptions);
               console.log("hereeeeeeee 746");

               //smtpTransport.sendMail(mainOptions, function (err, info) {
               transport.sendMail(mainOptions, function(err, info) {
                   if (err) {
                       console.log(err);
                       //return true
                   } else {
                       console.log('Message sent: ' + info);
                       //return true
                   }
               });
               // return res.send({
               //    status:true,
               //    message:"Succès",
               //    data:order_array
               // });
            }
         }catch(e){
            console.log(e);
         }

        // console.log("multiple order 87");
        // console.log("allorders ", allorders);
        // return false;

        //console.log("allorders ", allorders);
        //return false;


        for(let i=0;i<allorders.length;i++)
        {
           let order=allorders[i];
            //    const order=await orderModel.findById(id);
            const user = await User.findById(order.userId);
            if(!user || !order){
                return res.send({
                status:false,
                message:"user or order does not exists"
                })
            }

            // let provider_id = "";
            // let seller_totoal_profit = 0;
            // let chekc_condition2 = [];
            // let o_ID = order._id;
            // //console.log("user_order_id_arr 340 first --->>>> "+user_order_id_arr[mm].item_id);
            // chekc_condition2 = order.products.map((val)=>{
            //     provider_id = val.provider_id;
            //     //console.log("val ",val);
            //     //  return val;
            //     seller_totoal_profit = seller_totoal_profit + val.sellerprofit;
            //     val.is_paid = true;
            //     return val;
            // });
            // // console.log("chekc_condition2 ",chekc_condition2);
            // // console.log("seller_totoal_profit ",seller_totoal_profit);
            // // console.log("provider_id ",provider_id);
            // // console.log("o_ID ",o_ID);
            // const sellerdoc=await seller.findById(provider_id);
            // sellerdoc.totalpendingamount=sellerdoc.totalpendingamount+seller_totoal_profit;
            // sellerdoc.save();
            // await OrderModel.updateOne({_id:o_ID},{products:chekc_condition2});

            for(let m=0; m<order.products.length; m++)
            {
                //console.log("order ",order.products[0] );
                // console.log("provider_id ",order.products[0].provider_id );
                // console.log("part_id ",order.products[0].part_id );
                // console.log("admin_record ",admin_record);
                let part_id = order.products[0].part_id;
                let provider_id = order.products[0].provider_id;

                let message = "Nouvelle commande passée par un utilisateur dont l'ID de commande est - "+part_id;
                let data_save = {
                    to_id:provider_id,
                    user_type:"seller",
                    title:"Nouvelle commande",
                    message:message,
                    status: 'unread',
                    notification_type:"new_order"
                }
                await Notifications.create(data_save);
                if(admin_record)
                {
                    let admin_id = admin_record._id.toString();
                    let data_save_1 = {
                        to_id:admin_id,
                        user_type:"admin",
                        title:"Nouvelle commande",
                        message:message,
                        status: 'unread',
                        notification_type:"new_order"
                    }
                    await Notifications.create(data_save_1);

                }
            }

            const productids=order.products?.map((elem)=>mongoose.Types.ObjectId(elem.product._id));
            const newProductsSet=new Set(productids);
            const newProductArray=Array.from(newProductsSet.values());
            //console.log("newProductsSet",newProductArray)
            const products=await Product.aggregate([{$match:{_id:{$in:newProductArray}}}]);
            //console.log("products",products)
            const pathtofileforprovider = path.resolve("views/notifications/updateprovideraboutorderconfirmation.ejs");
            const pathtofileforuser = path.resolve("views/notifications/updateuseraboutorderconfirmation.ejs");
            let pushmessagetouser=``;
            
            if (order)
            {
                // console.log("order ",order );
                // console.log("========================>>>>>>>>>>>>>>>>>>>");
                // console.log("order products",order.products);
                if(order.products.length>0)
                { 
                    // console.log("order ",order.products[0] );
                    // console.log("order ",order.products[0].variation_id );
                    // console.log("order ",order.products[0].variation_value );
                    if(order.products[0].variation_id != "" && order.products[0].variation_id != null && order.products[0].variation_id != undefined)
                    {
                        let vari_rec = await VariationsPrice.findOne({_id:order.products[0].variation_id,variation_option_2:order.products[0].variation_value},{variation_option_2:1,stock:1,price:1});
                        //console.log("vari_rec ",vari_rec);
                        if(vari_rec)
                        {
                            let stock_array = [];
                            stock_array = vari_rec.stock;
                            if(vari_rec.variation_option_2 && vari_rec.price && vari_rec.stock)
                            {
                                if(vari_rec.variation_option_2.length > 0 && vari_rec.price.length > 0 && vari_rec.stock.length > 0)
                                {
                                    let indexxx = vari_rec.variation_option_2.indexOf(order.products[0].variation_value);
                                    
                                    if(vari_rec.price.length >= indexxx && vari_rec.stock.length >= indexxx)
                                    {
                                        
                                        priceee = vari_rec.price[indexxx];
                                        quantity = vari_rec.stock[indexxx];
                                        if(vari_rec.stock[indexxx] >= order.products[0].quantity)
                                        {
                                            let update_quantity = vari_rec.stock[indexxx] - order.products[0].quantity;
                                            if(stock_array.length >= indexxx)
                                            {
                                                stock_array[indexxx] = update_quantity;
                                                //console.log("stock_array ",stock_array);
                                                await VariationsPrice.updateOne({_id:order.products[0].variation_id},{ stock:stock_array });
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        const pro_rec = await Product.findOne({ _id: order.products[0].product._id });
                        //console.log("pro_rec 421 line no payment - controller",pro_rec);
                        if (pro_rec)
                        {
                            if(pro_rec.stock >=order.products[0].quantity )
                            {
                                pro_rec.stock = pro_rec.stock - order.products[0].quantity;
                                pro_rec.save()
                            }
                            
                        }
                    }
                }
                

                //return false;
                const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.userId });
                let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.teacher_id });
                
                order.payment_status = true;
                order.transaction_id = transaction_id;
                order.date_of_transaction = new Date();
                order.payment_method = "Cinetpay";
                // order.payment_amount = amount;
                // order.promo_code = promo;
                // order.discounted_amount=finalamount;
                order.status=0;
                order.save().then(async (result) => {
                    //console.log("method",method)
                    if(method=="cart"){
                        await User.findByIdAndUpdate(order.userId,{
                            cart:[]
                        });
                    }
                    let messageToUser;
                    if(order.products.length>1){
                        messageToUser=`Paiement réussi pour ${result.products[0].title} et plus dans le panier`;
                    }else{
                        messageToUser=`Paiement réussi pour ${result.products[0].title} dans le panier`;
                    }
                    await savetransactionclient(order, "");
                    
                    const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                    // console.log("user.email",user.email)
                    // if (notipermissionPermissionforUser) {
                    //     if (notipermissionPermissionforUser.push_reservation) {
                    //         //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    //     } else {
                    //         console.log("push notification is disabled for user")
                    //     } if (notipermissionPermissionforUser.email_reservation) {
                    //         sendmail(pathtofileforuser,
                    //             { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //             "Succès du paiement",
                    //             user.email);
                    //     } else {
                    //         console.log("email notification is disabled for user")
                    //     }
                    // } else {
                    //     //sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    //     sendmail(pathtofileforuser,
                    //         { name: user.first_name+" "+user.last_name, message:messageToUser},
                    //         "Succès du paiement",
                    //         user.email);
                    // }
                    // for(let i=0;i<products.length;i++){
                    //     let product=products[i];
                    //     let pushmessagetoprovider=``;
                    //     let messageToProvider=``;
                    //     let provider=await sellerModel.findById(product.provider_id);
                    //     if (notipermissionPermissionforProvider) {
                    //         if (notipermissionPermissionforProvider.push_reservation) {
                    //            // sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                    //         } else {
                    //             console.log("push notification is disabled for user")
                    //         } if (notipermissionPermissionforProvider.email_reservation) {
                    //             sendmail(pathtofileforuser,
                    //                 { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                    //                 "Un nouveau produit a été acheté",
                    //                 provider.email);
                    //         } else {
                    //             console.log("email notification is disabled for user")
                    //         }
                    //     } else {
                    //         //sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                    //         sendmail(pathtofileforuser,
                    //             { name: provider.first_name+" "+provider.last_name, message:messageToProvider},
                    //             "Un nouveau produit a été acheté",
                    //             provider.email);
                    //     }
                    // }
                    // coupon code stuff 
                    const coupen = await CoupensModel.findOne({ code: promo });
                    if (coupen) {
                        coupen.used = coupen.used + 1;
                        coupen.save()
                    }
                    

                    

                })

                
            } else {
                return res.send({
                    status: false,
                    message: "user not found"
                })
            }
        }
        var redirect_url_web = customConstant.base_url_user_site+"order_confirm_combo/"+id;
        console.log("redirect_url_web ",redirect_url_web );
        //return false;
        res.redirect(303, redirect_url_web);
        //return false;
        // return res.send({
        //     status: true,
        //     message: "payemtn success"
        // })
    },
    getpaymentevent: async(req, res) => 
    {
        //console.log("here 1826");return false;
        try{
            const amount = req.params.amount;
            const key = cinetpaykey;
            const user_id = req.params.user_id;
            const order_id = req.params.id;
            const method = req.params.method;
            let user_rec = await User.findOne({_id:user_id});
            let new_user_rec = {
                _id: user_rec._id,
                first_name: user_rec.first_name,
                last_name: user_rec.last_name,
                email: user_rec.email,
                phone: user_rec.phone
            };

            res.render('admin-panel/cinetpayforevent', {
                key: key,
                amount: amount,
                currency: "eur",
                user_id: user_id,
                order_id:order_id,
                method:method,
                new_user_rec:new_user_rec,
                url: "/api/common/postPaymentforevent",
                base_url: process.env.BASE_URL
            })
        }catch(e){
            console.log("e ",e);
        }
        
    },



    cinetPayEventSuccess: async(req, res) => {
        const amount = req.params.amount;
        const key = cinetpaykey;
        const user_id = req.params.user_id;
        let id = req.params.id;
        let  order_id = req.params.id;
        const method = req.params.method;
        const transaction_id= '';

        // const id = req.body.id;
        // const finalamount = req.body.finalamount;
        // const promo = req.body.promo;
        // const amount = req.body.amount;
        // const method = req.body.method;
        // const transaction_id=req.body.transaction_id;
        //console.log("id",req.body)
        
        const orders=await bookingmodel.find({combo_id:id});
        // console.log("orders ", orders);
        // return false;

        //console.log("user_id "+orders[0].user_id);
        
        //  console.log("order",order,"user",user,"organizer",organizer)
        //  if(!user||!provider){
        //     return res.send({
        //         status:false,
        //         message:"user  or organizer does not exists"
        //     })
        // }
       
        //    const order=await orderModel.findById(id);
      
     
      
        const pathtofileforprovider = path.resolve("views/notifications/updateproviderabouteventconfirmation.ejs");
        const pathtofileforuser = path.resolve("views/notifications/updateuserabouteventconfirmation.ejs");
        let pushmessagetouser=`L'événement {nom} a été réservé avec succès le {date} et {heure}`;
        let pushmessagetoprovider=`L'événement {nom} est réservé par {client} le {date} et {heure}`
        for(let i=0;i<orders?.length;i++)
        {
            let order=orders[i]
            if (order) 
        {
            // console.log("order event_id "+order);
            // console.log("order event_id "+order.event_id);
            let aa = order.event_id;
            //console.log("aa "+aa);
            let eve_rec = await Event.findById( aa ,{totoal_ticket:1});
            if(eve_rec)
            {
                // console.log("ifff 660");
                // console.log("eve_rec.totoal_ticket",eve_rec);
                // console.log("eve_rec.totoal_ticket",eve_rec.totoal_ticket);
                if(eve_rec.totoal_ticket >= order.purchased_ticket)
                {
                    //console.log("ifff 662");
                    let jjj = eve_rec.totoal_ticket - order.purchased_ticket;
                    //console.log("jjjjjjjjj ",jjj);
                    eve_rec.totoal_ticket = jjj;
                    eve_rec.save();
                }
            }
            //console.log("order "+order);
        // return false;
            //event_id: '64ba747560f340a8e22f2c2d'
            const user = await User.findById(order.user_id);
            
            const provider = await OrganizerModel.findById(order.org_id);
            const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.user_id });
            let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.org_id });
            
            order.payment_status = true;
            order.payment_amount=amount;
            order.transaction_id = transaction_id;
            order.date_of_transaction = new Date();
            order.payment_method = "Cinetpay";
            // order.payment_amount = amount;
            // order.promo_code = promo;
            // order.discounted_amount=finalamount;
            order.status=0;
            order.save().then(async (result) => {

                const user_record = await User.findById(orders[0].user_id);
                //console.log("user_record "+JSON.stringify(user_record));
                user_record.eventcart=[]
                user_record.save();

                await savetransactionclient(order, "");
            
                
                
                
                const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                // console.log("user.email",user.email)
                if (notipermissionPermissionforUser) {
                    if (notipermissionPermissionforUser.push_reservation) {
                        sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    } else {
                        console.log("push notification is disabled for user")
                    } if (notipermissionPermissionforUser.email_reservation) {
                        sendmail(pathtofileforuser,
                            { name: user.first_name+" "+user.last_name, message:pushmessagetouser},
                            "Succès du paiement",
                            user.email);
                    } else {
                        console.log("email notification is disabled for user")
                    }
                } else {
                    sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    sendmail(pathtofileforuser,
                        { name: user.first_name+" "+user.last_name, message:pushmessagetouser},
                        "Succès du paiement",
                        user.email);
                }
                // if (notipermissionPermissionforProvider) {
                //     if (notipermissionPermissionforProvider.push_reservation) {
                //         sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //     } else {
                //         console.log("push notification is disabled for user")
                //     } if (notipermissionPermissionforProvider.email_reservation) {
                //         sendmail(pathtofileforuser,
                //             { name: provider.first_name+" "+provider.last_name, message:pushmessagetoprovider},
                //             "Un nouveau produit a été acheté",
                //             provider.email);
                //     } else {
                //         console.log("email notification is disabled for user")
                //     }
                // } else {
                //     sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //     sendmail(pathtofileforuser,
                //         { name: provider.first_name+" "+provider.last_name, message:pushmessagetoprovider},
                //         "Un nouveau produit a été acheté",
                //         provider.email);
                // }
                // coupon code stuff 
                // const coupen = await CoupensModel.findOne({ code: promo });
                // if (coupen) {
                //     coupen.used = coupen.used + 1;
                //     coupen.save()
                // }
            
            })
            }
        }
        let urlll =  customConstant.base_url_web_site;
        urlll = urlll+"event_confirm";
        res.redirect(303, urlll);

        // return res.send({
        //     status: true,
        //     message: "payemtn success"
        // })

    },


    postPaymentforevent: async(req, res) => {
        // console.log(req.body);
        // return false;
        const id = req.body.id;
        const finalamount = req.body.finalamount;
        const promo = req.body.promo;
        const amount = req.body.amount;
        const method = req.body.method;
        const transaction_id=req.body.transaction_id;
        //console.log("id",req.body)
        
        const orders=await bookingmodel.find({combo_id:id});
        
        //console.log("user_id "+orders[0].user_id);
        
        //  console.log("order",order,"user",user,"organizer",organizer)
        //  if(!user||!provider){
        //     return res.send({
        //         status:false,
        //         message:"user  or organizer does not exists"
        //     })
        // }
       
        //    const order=await orderModel.findById(id);
      
     
      
        const pathtofileforprovider = path.resolve("views/notifications/updateproviderabouteventconfirmation.ejs");
        const pathtofileforuser = path.resolve("views/notifications/updateuserabouteventconfirmation.ejs");
        let pushmessagetouser=`L'événement {nom} a été réservé avec succès le {date} et {heure}`;
        let pushmessagetoprovider=`L'événement {nom} est réservé par {client} le {date} et {heure}`
        for(let i=0;i<orders?.length;i++)
        {
            let order=orders[i]
            if (order) 
        {
            // console.log("order event_id "+order);
            // console.log("order event_id "+order.event_id);
            let aa = order.event_id;
            //console.log("aa "+aa);
            let eve_rec = await Event.findById( aa ,{totoal_ticket:1});
            if(eve_rec)
            {
                // console.log("ifff 660");
                // console.log("eve_rec.totoal_ticket",eve_rec);
                // console.log("eve_rec.totoal_ticket",eve_rec.totoal_ticket);
                if(eve_rec.totoal_ticket >= order.purchased_ticket)
                {
                    //console.log("ifff 662");
                    let jjj = eve_rec.totoal_ticket - order.purchased_ticket;
                    //console.log("jjjjjjjjj ",jjj);
                    eve_rec.totoal_ticket = jjj;
                    eve_rec.save();
                }
            }
            //console.log("order "+order);
        // return false;
            //event_id: '64ba747560f340a8e22f2c2d'
            const user = await User.findById(order.user_id);
            
            const provider = await OrganizerModel.findById(order.org_id);
            const notipermissionPermissionforUser = await NotificationPermissionModel.findOne({ id: order.user_id });
            let notipermissionPermissionforProvider = await NotificationPermissionModel.findOne({ id: order.org_id });
            
            order.payment_status = true;
            order.payment_amount=amount;
            order.transaction_id = transaction_id;
            order.date_of_transaction = new Date();
            order.payment_method = "Cinetpay";
            // order.payment_amount = amount;
            // order.promo_code = promo;
            // order.discounted_amount=finalamount;
            order.status=0;
            order.save().then(async (result) => {

                const user_record = await User.findById(orders[0].user_id);
                //console.log("user_record "+JSON.stringify(user_record));
                user_record.eventcart=[]
                user_record.save();

                await savetransactionclient(order, "");
            
                
                
                
                const message = process.env.MESSAGE_PAYMENT_SUCCESS;
                // console.log("user.email",user.email)
                if (notipermissionPermissionforUser) {
                    if (notipermissionPermissionforUser.push_reservation) {
                        sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    } else {
                        console.log("push notification is disabled for user")
                    } if (notipermissionPermissionforUser.email_reservation) {
                        sendmail(pathtofileforuser,
                            { name: user.first_name+" "+user.last_name, message:pushmessagetouser},
                            "Succès du paiement",
                            user.email);
                    } else {
                        console.log("email notification is disabled for user")
                    }
                } else {
                    sendpushnotificationtouser(pushmessagetouser, user, user._id);
                    sendmail(pathtofileforuser,
                        { name: user.first_name+" "+user.last_name, message:pushmessagetouser},
                        "Succès du paiement",
                        user.email);
                }
                // if (notipermissionPermissionforProvider) {
                //     if (notipermissionPermissionforProvider.push_reservation) {
                //         sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //     } else {
                //         console.log("push notification is disabled for user")
                //     } if (notipermissionPermissionforProvider.email_reservation) {
                //         sendmail(pathtofileforuser,
                //             { name: provider.first_name+" "+provider.last_name, message:pushmessagetoprovider},
                //             "Un nouveau produit a été acheté",
                //             provider.email);
                //     } else {
                //         console.log("email notification is disabled for user")
                //     }
                // } else {
                //     sendpushnotificationtouser(pushmessagetoprovider, provider, provider._id);
                //     sendmail(pathtofileforuser,
                //         { name: provider.first_name+" "+provider.last_name, message:pushmessagetoprovider},
                //         "Un nouveau produit a été acheté",
                //         provider.email);
                // }
                // coupon code stuff 
                // const coupen = await CoupensModel.findOne({ code: promo });
                // if (coupen) {
                //     coupen.used = coupen.used + 1;
                //     coupen.save()
                // }
            
            })
            }
        }
        return res.send({
            status: true,
            message: "payemtn success"
        })
    },

    
}

// module.exports.cinetpaysuccess = async (req, res) => {
//     try{
//         const id = req.params.id;
//         const finalamount = req.params.finalamount;
//         const promo = req.params.promo;
//         const amount = req.params.amount;
//         const service = await Serviceprovided.findById(id);
//         const pathtofile = path.resolve("views/notifications/newbookingupdate.ejs");
//         const pathtofileuser = path.resolve("views/notifications/paymentsuccess.ejs");
//         if (service) {
//             const notipermissions = await notificationpermissionmodel.findOne({ id: service.student_id });
//             const notipermissiont = await notificationpermissionmodel.findOne({ id: service.teacher_id });
//             console.log("notipermissions,notipermissiont", notipermissions, notipermissiont)
//             service.payment_status = true;
//             service.mode_of_payment = "Cinetpay";
//             service.payment_amount = amount;
//             service.finalamount = finalamount;
//             service.promo = promo;
//             service.save().then(async (result) => {
//                 deletetimeslot(service.teacher_id, service.time_availed, service.date_availed, service.mode_of_service)
//                 await lawunit.find({ _id: service.teacher_id }).exec(async (err, lawyer) => {
//                     // console.log("lawyer in payent controller ",lawyer);
//                     const message = process.env.MESSAGE_TO_UPDATE_TEACHER_FOR_BOOKED_SERVICE + " " + service.client_name;
//                     if (notipermissiont) {
//                         if (notipermissiont.push_reservation) {
//                             sendpushnotificationtouser(message, lawyer[0], service._id)
//                         } else {
//                             console.log("push notification is disabled for teacher")
//                         } if (notipermissiont.email_reservation) {
//                             sendmail(pathtofile,
//                                 { name: result.teacher_name, message: "Une offre est faite par " + service.student_name + " pour le " + service.course_name },
//                                 "Une offre est faite par " + service.student_name + " pour le " + service.course_name,
//                                 lawyer[0].email);
//                         } else {
//                             console.log("email notification is disabled for teacher")
//                         }
//                     } else {
//                         sendpushnotificationtouser(message, lawyer[0], service._id);
//                         sendmail(pathtofile,
//                             { name: result.teacher_name, message: "Une offre est faite par " + service.student_name + " pour le " + service.course_name },
//                             "Une offre est faite par " + service.student_name + " pour le " + service.course_name,
//                             lawyer[0].email);
//                     }


//                 })
//                 await savetransactionclient(service, "");
//                 const user = await User.findById(service.student_id);
//                 const message = process.env.MESSAGE_PAYMENT_SUCCESS;
//                 // console.log("user.email",user.email)
//                 if (notipermissions) {
//                     if (notipermissions.push_reservation) {
//                         sendpushnotificationtouser(message, user, service._id);
//                     } else {
//                         console.log("push notification is disabled for user")
//                     } if (notipermissions.email_reservation) {
//                         sendmail(pathtofileuser,
//                             { name: result.teacher_name, message: "Succès du paiement pour" + service.course_name + " à la date" },
//                             "Succès du paiement pour" + service.course_name + " à la date",
//                             user.email);
//                     } else {
//                         console.log("email notification is disabled for user")
//                     }
//                 } else {
//                     sendpushnotificationtouser(message, user, service._id);
//                     sendmail(pathtofileuser,
//                         { name: result.teacher_name, message: "Succès du paiement pour" + service.course_name + " à la date" },
//                         "Succès du paiement pour" + service.course_name + " à la date",
//                         user.email);
//                 }

//                 const coupen = await CoupensModel.findOne({ code: promo });
//                 if (coupen) {
//                     coupen.used = coupen.used + 1;
//                     coupen.save()
//                 }
//                 return res.send({
//                     status: true,
//                     message: "payemtn success"
//                 })
//             })
//         } else {
//             return res.send({
//                 status: false,
//                 message: "user not found"
//             })
//         }
//     }catch(e){
//         return res.send({
//             status: false,
//             message: e.message
//         })
//     }

// }

