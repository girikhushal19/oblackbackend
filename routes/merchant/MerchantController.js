//const Seller = require("../../models/seller/Seller");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const fs = require('fs');
const UsersModel = require("../../models/user/User");
const customConstant = require('../../helpers/customConstant');
const SettingModel = require("../../models/admin/SettingModel");
const PushNotificationMerchantModel = require("../../models/admin/PushNotificationMerchantModel");
const ShipmentModel = require("../../models/admin/ShipmentModel");
const ClientsModel = require("../../models/admin/ClientsModel");
const mongoose = require("mongoose");
const {getdateinadvance,getDays,getDate} =require("../../modules/dates.js")
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");
//const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});



module.exports = {

  merchantRegistration:async function(req,res,next)
  {
    try{
      //console.log("hereee");
      //console.log(req.body);
      //return false;
      var encryptedPassword = "";
      var {firstName,lastName,companyName,email,mobileNumber,address,latitude,longitude,password,credit_limit} = req.body;
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse finale doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        encryptedPassword = await bcrypt.hash(password, 10);
        credit_limit = parseFloat(credit_limit);
        //console.log("xx"+xx);
        //console.log("encryptedPassword khushal "+encryptedPassword);
        var token =  jwt.sign(
          { user_id:  Math.random().toString(36).slice(2), email },
          "Oblack-User",
          //{
          //  expiresIn: "2h",
          //}
        );
        UsersModel.count({ email },(errs,emailCount)=>
        {
          if(errs)
          {
            //console.log(errs);
            var return_response = {"error":true,success: false,errorMessage:errs};
              res.status(200)
              .send(return_response);
          }else{
            //console.log(emailCount);
            if(emailCount === 0)
            {
              UsersModel.count({ phone:mobileNumber },(userNameerrs,userNameCount)=>
              {
                if(userNameerrs)
                {
                  console.log(userNameerrs);
                  var return_response = {"error":true,success: false,errorMessage:userNameerrs};
                    res.status(200)
                    .send(return_response);
                }else{
                  if(userNameCount === 0)
                  {
                    UsersModel.create({
                      user_type:'merchant',
                      phone:mobileNumber,
                      first_name:firstName,
                      last_name:lastName,
                      companyName:companyName,
                      email: email.toLowerCase(), // sanitize: convert email to lowercase
                      password: encryptedPassword,
                      token: token,
                      userAddress:address,
                      userLocation: {
                        type: "Point",
                        coordinates: [latitude,longitude]
                      },
                      credit_limit:credit_limit,
                    },function(err,result){
                      if(err)
                      {
                        //console.log(err);
                        var return_response = {"error":true,success: false,errorMessage:err};
                          res.status(200)
                          .send(return_response);
                      }else{
                        var return_response = {"error":false,success: true,errorMessage:"Votre enregistrement a réussi, vous pouvez vous connecter avec nous"};
                          res.status(200)
                          .send(return_response);
                      }
                    });
                  }else{
                    //email exist
                    res.status(200)
                          .send({
                              error: true,
                              success: false,
                              errorMessage: "Le numéro de mobile existe déjà"
                          });
                  }
                }
              });
            }else{
              //email exist
              res.status(200)
                .send({
                  error: true,
                  success: false,
                  errorMessage: "Email déjà existant"
              });
            }
          }
        }); 
      }
    }catch(error)
    {
      console.log(error);
    }
  },
  merchantLogin:async function(req,res,next)
  {
    try{
      //console.log(req.body);return false;
      // Get user input
      const { email, password } = req.body;
      var date_time = new Date();
      // Validate user input
      // Validate if user exist in our database
      var user = "";
      user = await UsersModel.findOne({ email , user_type: 'merchant',is_deleted: false});
      //console.log("user record"+user);
      if (user && (await bcrypt.compare(password, user.password)))
      {
        // Create token
        const token = jwt.sign(
          { user_id: user._id, email },
          "Oblack-Driver",
          /*process.env.TOKEN_KEY,
          {
            expiresIn: "2h",
          }*/
        );
        // save user token

        if(user.is_active != true)
        {
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Votre compte est en cours de révision",
          });
        }else{
          user.token = token;
          UsersModel.updateOne({ "email":email,user_type: 'merchant',is_deleted: false}, 
          {token:token,
            loggedin_time:date_time}, function (uerr, docs) {
          if (uerr)
          {
              console.log(uerr);
              res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: uerr,
                    //userRecord:user
                });
          }
          else{
              res.status(200)
              .send({
                  error: false,
                  success: true,
                  errorMessage: "Connexion réussie",
                  userRecord:user
              });
            }
          });
        }
      }else{
        //res.status(400).send("Invalid Credentials");
        res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: "Détails de connexion non valides"
          });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  merchantForgotPassword:async function(req,res,next)
  {
    try{
      var base_url = customConstant.base_url;
      var imagUrl = base_url+"public/uploads/logo.png";
      //console.log("process.env.MAILER_FROM_EMAIL_ID "+process.env.MAILER_FROM_EMAIL_ID);
      
      //console.log(req.body);
      var randomNumber = Array.from(Array(20), () => Math.floor(Math.random() * 36).toString(36)).join('');
      var base_url_web_site = customConstant.base_url_merchant_site;
      var resetPWUrl = base_url_web_site+"/resetPassword/"+randomNumber;
      var dateobj = new Date();
      var isoDate = dateobj.toISOString();
      //console.log("isoDate "+isoDate);
      var {email} = req.body;
      UsersModel.findOne({ email , user_type: 'merchant',is_deleted: false}).exec((err,result)=>{
        if(err)
        {
          console.log(err);
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: err
          });
        }else{
          //console.log(result);isoDate
          if(result)
          {
            if(result.is_active == true)
            {
              var html = "";
              html += "<div class='mail-content' style='position: relative;display: block;font-size: 14px;line-height: 25px;font-family: Poppins, sans-serif;'>";
              html += "<div class='content'></div>";
              html += "<p>	Bienvenue ,</p>";
              html += "<p>"+result.first_name+' '+result.last_name+"</p>";
              html += "<p>Vous pouvez réinitialiser le mot de passe à partir d'ici ci-dessous est le lien disponible, vous pouvez cliquer et réinitialiser votre mot de passe, ce lien expirera dans 24 heures.</p>";
              html += "<p><a href='"+resetPWUrl+"'>Cliquez ici</a></p>";
              html += "</div>";
              html += "<div class='mail-footer'>";
              html += "<img class='mw-100' src='"+imagUrl+"' height='120' width='120'>";
              html += "</div>";
              html += "<i>Oblack</i>";
              //console.log(randomNumber);
              UsersModel.updateOne({ "email":email, user_type: 'merchant',is_deleted: false }, 
              {forgotPasswordLink:randomNumber,forgotPasswordTime:isoDate,forgotPasswordUsed:0}, function (uerr, docs) {
              if (uerr)
              {
                  //console.log(uerr);
                  res.status(200)
                    .send({
                        error: true,
                        success: false,
                        errorMessage: uerr,
                        //userRecord:user
                    });
              }else{
                  const message = {
                    from: process.env.MAILER_FROM_EMAIL_ID, // Sender address
                    to: email,         // recipients
                    subject: "Mot de passe oublié", // Subject line
                    html: html
                  };
                  transport.sendMail(message, function(err, info) {
                    if (err)
                    {
                      console.log(err);
                      var return_response = {"error":true,success: false,errorMessage:"Le courrier électronique ne fonctionne pas, veuillez contacter le support technique"};
                      res.status(200)
                      .send(return_response);

                    }else{
                      //console.log('mail has sent.');
                      //console.log(info);
                      var return_response = {"error":false,success: true,errorMessage:"Nous avons envoyé un lien sur votre adresse e-mail enregistrée, vous pouvez réinitialiser le mot de passe à partir de là."};
                      res.status(200)
                      .send(return_response);
                    }
                  });
                }
              });
            }else{
              res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Votre compte n'est pas actif, vous pouvez contacter l'administrateur."
              });
            }
          }else{
            res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Donnée non valide."
              });
          }
          
        }
      });
    }catch(error)
    {
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  resetPasswordSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);
      var {password,reset_id} = req.body;
      var encryptedPassword = await bcrypt.hash(password, 10);
      var dateobj = new Date();
      var isoDate = dateobj.toISOString();
      if(reset_id == "" || reset_id == null || reset_id == 'null' || password == "" || password == null)
      {
        res.status(200)
        .send({
            error: true,
            success: false,
            errorMessage: "Certaines valeurs requises sont manquantes, vous ne pouvez pas continuer "
        });
      }else{
          UsersModel.findOne({ forgotPasswordLink:reset_id,forgotPasswordUsed:0}).exec((err,result)=>{
          if(err)
          {
            console.log(err);
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
          }else{
            if(result)
            {
              console.log(result);
              var endTime = result.forgotPasswordTime;
              var currentTime = new Date();
              var diffTime =(currentTime.getTime() - endTime.getTime());
   
              // calculate the number of days between hours dates javascript
              var hoursDiff = diffTime / (1000 * 3600); 
              //console.log("hoursDiff "+hoursDiff);
              hoursDiff = parseFloat(hoursDiff);
              if(hoursDiff > 24)
              {
                res.status(200)
                .send({
                    error: true,
                    success: false,
                    errorMessage: "Ce lien a expiré, vous pouvez l'utiliser dans les 24 heures suivant la demande de réinitialisation du mot de passe."
                });
              }else{
                //console.log("here 352");
                UsersModel.updateOne({ "_id":result._id }, 
                  {
                    password:encryptedPassword,
                    forgotPasswordUsed:1
                  }, function (uerr, docs) {
                  if (uerr)
                  {
                      //console.log(uerr);
                      res.status(200)
                        .send({
                            error: true,
                            success: false,
                            errorMessage: uerr,
                            //userRecord:user
                        });
                  }else{
                    res.status(200)
                    .send({
                        error: false,
                        success: true,
                        errorMessage: "Votre nouveau mot de passe a été défini avec succès, vous pouvez vous connecter avec ce détail."
                    });
                  }
                });
              }
            }else{
              res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Ce lien est expiré ou déjà utilisé."
              });
            }
          }
        });
      }
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  getMerchantProfile:async function(req,res,next)
  {
    try{
      var {id} = req.body;
      UsersModel.findOne({ _id:id}).exec((err,result)=>{
        if(err)
        {
          console.log(err);
          res.status(200)
          .send({
              error: true,
              success: false,
              errorMessage: err
          });
        }else{
          res.status(200)
          .send({
              error: false,
              success: true,
              errorMessage: "Succès",
              record:result
          });
        }
      });
    }catch(error){
      console.log(error);
      res.status(200)
      .send({
          error: true,
          success: false,
          errorMessage: error
      });
    }
  },
  editMerchantProfileSubmitApi:async function(req,res,next)
  {
    try{
      //console.log("hereee");
      //console.log(req.body);
      //console.log(req.file);
      console.log(req.files);
      //return false;
      var photo = "";
      
      //console.log("photo 453  "+photo);
      //return false;
      var {first_name,last_name,companyName,email,mobileNumber,address,latitude,longitude,id} = req.body;
      if(latitude == "" || latitude == null || longitude == "" || longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"l'adresse finale doit être une adresse google"};
        res.status(200)
        .send(return_response);
      }else{
        latitude = parseFloat(latitude);
        longitude = parseFloat(longitude);
        UsersModel.findOne({ _id:id},{photo:1}).exec((err,result)=>{
          if(err)
          {
            console.log(err);
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
          }else{
            if(result)
            {
              if(req.files.file)
              {
                if(req.files.file.length > 0)
                {
                  photo = req.files.file[0].filename;

                  var oldFileName = result.photo;
                  var uploadDir = './public/uploads/merchant/';
                  let fileNameWithPath = uploadDir + oldFileName;
                  //console.log(fileNameWithPath);
                  if (fs.existsSync(fileNameWithPath))
                  {
                    fs.unlink(uploadDir + oldFileName, (err) => 
                    {
                      console.log("unlink file error "+err);
                    });
                  }

                }else{
                  photo = result.photo;
                }
              }else{
                photo = result.photo;
              }
              console.log("photo 481  "+photo);
              UsersModel.count({ "_id":{$ne:id},email:email,is_deleted: false },(errs,emailCount)=>
              {
                if(errs)
                {
                  //console.log(errs);
                  var return_response = {"error":true,success: false,errorMessage:errs};
                    res.status(200)
                    .send(return_response);
                }else{
                  //console.log(emailCount);
                  if(emailCount === 0)
                  {
                    UsersModel.count({ "_id":{$ne:id},phone:mobileNumber,is_deleted: false },(userNameerrs,userNameCount)=>
                    {
                      if(userNameerrs)
                      {
                        console.log(userNameerrs);
                        var return_response = {"error":true,success: false,errorMessage:userNameerrs};
                          res.status(200)
                          .send(return_response);
                      }else{
                        if(userNameCount === 0)
                        {
                          UsersModel.updateOne({ _id:id },{
                            phone:mobileNumber,
                            first_name:first_name,
                            last_name:last_name,
                            companyName:companyName,
                            email: email.toLowerCase(), // sanitize: convert email to lowercase
                            photo: photo,
                            userAddress:address,
                            userLocation: {
                              type: "Point",
                              coordinates: [latitude,longitude]
                            },
                          },function(err,result){
                            if(err)
                            {
                              console.log(err);
                              var return_response = {"error":true,success: false,errorMessage:err};
                                res.status(200)
                                .send(return_response);
                            }else{
                              var return_response = {"error":false,success: true,errorMessage:"Votre enregistrement a réussi, vous pouvez vous connecter avec nous"};
                                res.status(200)
                                .send(return_response);
                            }
                          });
                        }else{
                          //email exist
                          res.status(200)
                                .send({
                                    error: true,
                                    success: false,
                                    errorMessage: "Le numéro de mobile existe déjà"
                                });
                        }
                      }
                    });
                  }else{
                    //email exist
                    res.status(200)
                      .send({
                        error: true,
                        success: false,
                        errorMessage: "Email déjà existant"
                    });
                  }
                }
              });
            }else{
              res.status(200)
              .send({
                  error: true,
                  success: false,
                  errorMessage: "Pas d'enregistrement"
              });
            }
            
          }
        }); 
      }
    }catch(error)
    {
      console.log(error);
    }
  },
  getMerchantLimit:async function(req,res,next)
  {
    SettingModel.findOne({ attribute_key:"max_merchant_limit" }, {}).exec((err, result)=>
    {
      if(err)
      {
        console.log(err);
      }else{
        if(result)
        {
          var return_response = {"error":false,success: true,errorMessage:"success","result":result};
            res.status(200)
            .send(return_response);
        }else{
          var return_response = {"error":true,success: true,errorMessage:"success","result":result};
            res.status(200)
            .send(return_response);
        }        
      }
    });
  },

  allMerchantPushNotification:async function(req,res,next)
	{
		var { user_id } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    
      String_qr['user_id'] =  user_id;
	  	//console.log(String_qr);
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);

	    	
	      PushNotificationMerchantModel.find( String_qr, {title:1,description:1}).sort({created_at:-1}).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allMerchantPushNotificationCount:async function(req,res,next)
	{
		var { user_id } = req.body;
  	/*console.log(fullName);
  	console.log(lastName);
  	console.log(email);*/

	  	var String_qr = {}; 
	    
      String_qr['user_id'] =  user_id;
	  	//console.log(String_qr);
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    try{
	    	//mongoose.set('debug', true);
	      PushNotificationMerchantModel.countDocuments(String_qr).exec((err, totalcount)=>
        {
          //console.log("countResult "+totalcount);
          if(err)
          {
            console.log(err);
          }else{
            total = totalcount;
            totalPageNumber = total / perPageRecord;
            //console.log("totalPageNumber"+totalPageNumber);
            totalPageNumber = Math.ceil(totalPageNumber);
            //console.log("totalPageNumber"+totalPageNumber);
            //console.log("totalcount new fun"+totalcount);
            //console.log("all record count "+totalcount);
            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
            res.status(200).send(return_response);
          }
        });
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},

  getHomePageChartData : async (req, res) =>
  {
    var user_id = req.body.user_id;
    if(!user_id)
    {
        return res.send({
          status:false,
          data:{ }
      });
    }
    // console.log("user_id merchant side ", user_id);
    // return false;
    const totalClient = await ClientsModel.count({user_id:user_id});
    let date = new Date(), y = date.getFullYear(), m = date.getMonth();
    let currDate = new Date(y, m, 1);
    let daydateto30 = new Date(y, m + 1, 0);
    // let currDate=getdateinadvance(1);
    // let daydateto30=getdateinadvance(30);
    //  console.log("currDate",currDate,"daydateto30",daydateto30)
    let numberofdays=getDays(currDate.getFullYear(),currDate.getMonth()+1);
    let numberofdaysarray=[];
    let totalsalesarray=[];
    // console.log("numberofdaysarray",numberofdaysarray)
    const monthrange={$and:[
      {user_id:mongoose.Types.ObjectId(user_id)},
        {created_at:{
        $gt:currDate
        }},
        {created_at:{
            $lte:daydateto30
        }}
    ]};
    //console.log("monthrange",monthrange,"currDate",currDate,"daydateto30",daydateto30);
    
    const salesthismonth = await ShipmentModel.find(monthrange,{created_at:1} );
    
    //console.log("salesthismonth ",salesthismonth );
    //status: { type: Number, default: 1 }, // 1 in progress 2 shipped 3 delivered 4 cancled
    const totalShipment = await ShipmentModel.count({user_id:user_id});
    const proccessingorders = await ShipmentModel.count({user_id:user_id,status:1});
    const shippedorders = await ShipmentModel.count({user_id:user_id,status:2});
    const deliveredorders = await ShipmentModel.count({user_id:user_id,status:3});
    const cancledorders = await ShipmentModel.count({user_id:user_id,status:4});

    // console.log("ordersthismonth",ordersthismonth)
    for(let i=1;i<=numberofdays;i++)
    {
      let datetocompare=getDate(currDate.getFullYear(),currDate.getMonth()+1,i);
      numberofdaysarray.push(i);
      let totalsalesonthisdate=salesthismonth.filter((e)=>{
           //console.log("e.created_at", e.created_at.toISOString().split("T")[0]);
          // console.log(" datetocompare", datetocompare.toISOString().split("T")[0]);
          return e.created_at.toISOString().split("T")[0]==datetocompare.toISOString().split("T")[0]
      });
      //console.log("totalsalesonthisdate ",totalsalesonthisdate);
      if(totalsalesonthisdate.length)
      {
          let totalsalesonthisdateqty=totalsalesonthisdate.length;
          totalsalesarray.push(totalsalesonthisdateqty)
      }else{
          totalsalesarray.push(0)
      }
    }

    const orderspie=[
      totalShipment,
      proccessingorders,
      shippedorders,
      deliveredorders,
      cancledorders
    ]
    return res.send({
        status:true,
        totalShipment:totalShipment,
        totalClient:totalClient,
        data:{
            totalsalesarray,
            numberofdaysarray,
             orderspie
        }
    });
  },
};