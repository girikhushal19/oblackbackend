const express =require('express');
const DriverController = require('./DriverController');
const DriverRoutePlanController = require('./DriverRoutePlanController');
const router = express.Router();
const multer=require("multer");
const path=require("path");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads/driver');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadDriverProfile = multer({ storage: storage });

const storage3 = multer.diskStorage({
  destination: function(req, file, cb) {
      cb(null, './public/uploads/deliveryStop/');
  },
  filename: function(req, file, cb) {
    //console.log(req.body);
    //console.log(req.file);
    //console.log(file.originalname);
      var dynamicFileName = Math.floor(Math.random() * Date.now());
    cb(null, dynamicFileName + '-' + file.originalname);
  }
});
var uploadDeliveredStop = multer({ storage: storage3 });



router.post('/driverLogin',DriverController.driverLogin);
router.post('/driverLogout',DriverController.driverLogout);

router.post('/forgotPasswordDriver',DriverController.forgotPasswordDriver);
router.post('/getDriverProfile',DriverController.getDriverProfile);
router.post('/editDriverProfile',uploadDriverProfile.array('file',1),DriverController.editDriverProfile);
router.get('/getDriverSupport',DriverController.getDriverSupport);
router.post('/checkFcmToken',DriverController.checkFcmToken);

router.post('/getAllPushNotification',DriverController.getAllPushNotification);
router.post('/getAllPushNotificationCount',DriverController.getAllPushNotificationCount);
router.post('/getSinglePushNotification',DriverController.getSinglePushNotification);
router.post('/updateReadStatus',DriverController.updateReadStatus);

router.post('/getDriverCurrentRoutePlan',DriverRoutePlanController.getDriverCurrentRoutePlan);
router.post('/getDriverPastRoutePlan',DriverRoutePlanController.getDriverPastRoutePlan);

router.post('/getDriverAllRouteStop',DriverRoutePlanController.getDriverAllRouteStop);
router.post('/getDriverSingleRouteStop',DriverRoutePlanController.getDriverSingleRouteStop);
router.post('/getDriverSingleRouteWithStop',DriverRoutePlanController.getDriverSingleRouteWithStop);

router.post('/deliveredStop',uploadDeliveredStop.fields([{
  name: 'deliverySignature', maxCount: 1
}, {
  name: 'deliveryPhoto', maxCount: 1
}]),DriverRoutePlanController.deliveredStop);

router.post('/skipStop',DriverRoutePlanController.skipStop);
router.post('/getDriverAllRoutePlan',DriverRoutePlanController.getDriverAllRoutePlan);
router.post('/getDeliveredStop',DriverRoutePlanController.getDeliveredStop);
router.post('/getSkippedStop',DriverRoutePlanController.getSkippedStop);
router.post('/startRoute',DriverRoutePlanController.startRoute);


router.post('/driverChangePasswordSubmit',DriverController.driverChangePasswordSubmit);
module.exports=router