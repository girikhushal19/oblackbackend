const SellerRatingsModel=require("../../models/actions/Ratingnreviews");
const mongoose=require("mongoose")
module.exports={
    addOrupdaterating:async(req,res)=>{
     const {
        user_id,
        seller_id,
        user_name,
        seller_name,
        rating,
        review
   
     }=req.body;
     console.log("req.body",req.body)
    //   let mediaarray=[]
    //     if(req.files.photo.length){
    //         req.files.photo.map((file)=>{
    //             newfile=process.env.PHOTOS_PATH_PERFIX+"/"+file.filename;
    //             mediaarray.push(newfile);
    //         })
           
    //     }
     const isreviewexists=await SellerRatingsModel.findOne({user_id:user_id,seller_id:seller_id});
     if(isreviewexists){
        isreviewexists.rating=rating;
        isreviewexists.review=review;
        isreviewexists.save((result)=>{
            return res.send({
                status:true,
                message:"update successfully"
            })
        })
     }else{
      await SellerRatingsModel.create({
        user_id:user_id,
        seller_id:seller_id,
        user_name:user_name,
        seller_name:seller_name,
        rating:rating,
        review:review
      }).then((result)=>{
        return res.send({
            status:true,
            message:"added successfully"
        })
      })
     }
    },
    deleterating:async(req,res)=>{
     const id=req.params.id;
     await SellerRatingsModel.findByIdAndDelete(id).then((result)=>{
        return res.send({
            status:true,
            message:"deleted successfully"
        })
     })
    },
    getallratingsbystoreid:async(req,res)=>{
        const id=req.params.id;
        console.log("id",id)
    const storeratings=await SellerRatingsModel.aggregate([
        {$match:{seller_id:id}},
        {$addFields:{
           
            userIDOBJ:{"$toObjectId":"$user_id"},
            vendorIDOBJ:{"$toObjectId":"$seller_id"},
        }},
        {
            $lookup:{
                from:"users",
                localField:"userIDOBJ",
                foreignField:"_id",
                as:"user"
            }
        },
        {
            $lookup:{
                from:"sellers",
                localField:"vendorIDOBJ",
                foreignField:"_id",
                as:"vendor"
            }
        }
    ]);
    return res.send({
        status:true,
        data:storeratings,
        message:"fetched successfully"
    })
    },
    getallstoresratings:async(req,res)=>{
        try{
            const pageno=req.params.pageno
            const limit=10;
            
            const skip=limit*pageno;
                if(pageno==null||pageno=="null"){
                const count=(await SellerRatingsModel.aggregate([
                    {$match:{_id:{$ne:null}}},
                    // {$addFields:{
                    
                    //     userIDOBJ:{"$toObjectId":"$user_id"},
                    //     vendorIDOBJ:{"$toObjectId":"$seller_id"},
                    // }},
                    // {
                    //     $lookup:{
                    //         from:"users",
                    //         localField:"userIDOBJ",
                    //         foreignField:"_id",
                    //         as:"user"
                    //     }
                    // },
                    // {
                    //     $lookup:{
                    //         from:"sellers",
                    //         localField:"vendorIDOBJ",
                    //         foreignField:"_id",
                    //         as:"vendor"
                    //     }
                    // }
                ]))?.length
        
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            const storeratings=await SellerRatingsModel.aggregate([
                {$match:{_id:{$ne:null}}},
                // {$addFields:{
                
                //     userIDOBJ:{"$toObjectId":"$user_id"},
                //     vendorIDOBJ:{"$toObjectId":"$seller_id"},
                // }},
                {
                    $lookup:{
                        from:"users",
                        let:{"usr_id":{ "$toObjectId":"$user_id" }},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$usr_id"]
                                    }
                                }
                            }
                        ],
                        as:"user"
                    }
                },
                {
                    $addFields:{
                        first_name:{$arrayElemAt:["$user.first_name",0]},
                        last_name: {$arrayElemAt:["$user.last_name",0] }
                    }
                }, 

                {
                    $lookup:{
                        from:"sellers",
                        let:{"slr_id":{"$toObjectId":"$provider_id"} },
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$_id","$$slr_id"]
                                    }
                                }
                            }
                        ],
                        as:"vendor"
                    }
                },
                {
                    $addFields:{
                        fullname: {$arrayElemAt:["$vendor.fullname",0]} ,
                        shopname:{$arrayElemAt:[ "$vendor.shopname",0] }
                    }
                },
                {
                    $project:{
                        "user":0,
                        "vendor":0
                    }
                },
                {$skip:skip},
                {$limit:limit}
            ]);
            return res.send({
                status:true,
                data:storeratings,
                message:"fetched successfully"
            })
        }catch(error){
            return res.send({
                status:false,
                data:[],
                message:error.message
            })
        }
        
    }
}