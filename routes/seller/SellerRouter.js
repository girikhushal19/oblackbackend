const express =require('express');
const SellerController = require('./SellerController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const SELLER_QUERY_PATH=process.env.SELLER_QUERY_PATH;


const storage_2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(SELLER_QUERY_PATH))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadTo_2 = multer({ storage: storage_2 });

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });

router.post('/submitSellerQuery',uploadTo_2.fields([
  { 
    name: 'photo', 
    maxCount: 1
  }
]
),SellerController.submitSellerQuery);

router.post('/createseller',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    },
    { 
      name: 'front', 
      maxCount: 10
    },
    { 
      name: 'back', 
      maxCount: 10
    },
    { 
      name: 'shopphoto', 
      maxCount: 10
    },
    { 
      name: 'registration_card', 
      maxCount: 1
    }
    
  ]
  ),SellerController.createseller);
router.post('/login',SellerController.login)
//router.post('/forgotpassword',SellerController.forgotpassword);
router.get("/logout/:id", SellerController.logout);
router.post("/updateBankDetail",SellerController.updateBankDetail);
router.post("/getSingleBankDetail",SellerController.getSingleBankDetail);

router.get("/sellerincome/:id",SellerController.sellerincome);
router.post("/sellerincomePostMethod",SellerController.sellerincomePostMethod);

router.get("/sellerincome_dashboard_page/:id",SellerController.sellerincome_dashboard_page);

router.post("/sellerbalance",SellerController.sellerbalance);
// router.post("/updatebank",SellerController.updatebankmarkbankasdefault);
router.get("/getallbanks/:id",SellerController.getallbanks);

router.post("/getbankbybankid", SellerController.getbankbybankid);
router.get("/getuserprofile/:id", SellerController.getuserprofile);
router.post("/deletebank", SellerController.deletebank);
router.post("/deletemultipleseller", SellerController.deletemultipleuser);
router.post("/getvendors", SellerController.getvendors);
router.get("/deletevendor/:id", SellerController.deletevendors);
router.get("/markvendorasactive/:id", SellerController.markVendorasActive);
router.get("/markvendorasInactive/:id", SellerController.markVendorasInActive);

router.post("/markvendorasactive", SellerController.markVendorasActive);
router.post("/markvendorasInactive", SellerController.markVendorasInActive);

router.get("/markvendorasapproved/:id", SellerController.markAsApproved);
router.get("/markvendorasunapproved/:id", SellerController.markAsUnApproved);
router.get("/reset_password",SellerController.render_reset_password_template);
router.post("/reset_password",SellerController.reset_password);
router.post("/register",SellerController.register);
router.post("/verifycode", SellerController.verifycode);
router.post("/resend_verificatio_email_code", SellerController.resend_verificatio_email_code);

router.get("/getallsellerforfilter",SellerController.getallsellerforfilter);

router.post("/makeapaymentrequest", SellerController.makeapaymentrequest);
router.post("/getallpaymentrequest", SellerController.getallpaymentrequest);
router.get("/getpaymentrequestbyvendor/:id", SellerController.getpaymentrequestbyvendor);

router.post("/getCountPaymentRequestVendor", SellerController.getCountPaymentRequestVendor);
router.post("/getPaymentRequestVendor", SellerController.getPaymentRequestVendor);

router.get("/markpaymentrequestasresolved/:id", SellerController.markpaymentrequestasresolved);
router.get("/markpaymentrequestascanclled/:id", SellerController.markpaymentrequestascanclled);
//router.get("/deletepaymentrequest/:id", SellerController.deletepaymentrequest);


router.get("/getallcouponsbyvendorid/:id", SellerController.getallcouponsbyvendorid);
router.get("/getSingleCouponsById/:id", SellerController.getSingleCouponsById);

router.get("/getallcouponsforuser", SellerController.getallcouponsforuser);
router.post("/saveorupdatecoupens",SellerController.saveorupdatecoupens);
router.get("/deletecoupen/:id",SellerController.deletecoupen);
router.get("/deactivatecoupen/:id",SellerController.deactivatecoupen);
router.get("/activatecoupen/:id", SellerController.activatecoupen);
router.post("/activatecouponforproduct", SellerController.activatecouponforproduct);
router.post("/markbankasdefault", SellerController.markbankasdefault);
router.post("/makepaymentrequest", SellerController.makepaymentrequest);
router.post("/submitSellerContactForm", SellerController.submitSellerContactForm);
router.post("/sellerForgotPassword", SellerController.sellerForgotPassword);
router.post("/submitForgotPassword", SellerController.submitForgotPassword);
router.get("/get_terms_condition", SellerController.get_terms_condition);
router.get("/get_seller_product/:id", SellerController.get_seller_product);
router.post("/get_seller_product_POST", SellerController.get_seller_product_POST);

router.get("/get_seller_product_2/:id", SellerController.get_seller_product_2);
router.get("/get_seller_coupon", SellerController.get_seller_coupon);
router.post("/demandpayment", SellerController.demandpayment);
router.post("/addNote",SellerController.addNote);

module.exports=router







                