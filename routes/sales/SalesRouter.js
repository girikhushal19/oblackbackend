const express =require('express');
const SalesController = require('./SalesController');
const router = express.Router();
const multer=require("multer");
const path=require("path");
const user_path=process.env.USER_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(user_path))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });
router.post('/createsales',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),SalesController.createSales);
router.get('/deletesales/:id',SalesController.deleteSales)
module.exports=router