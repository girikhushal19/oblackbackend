const express =require('express');
const EventController = require('./EventController');
const Favrioutcontroller=require("./Favriout.controller");
const router = express.Router();
const multer=require("multer");
const path=require("path");
const PRODUCT_PATH=process.env.PRODUCT_PATH;
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.resolve(PRODUCT_PATH))
    },
    filename: function (req, file, cb) {
      cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
    }
  });
const uploadTo = multer({ storage: storage });


const storage2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve(PRODUCT_PATH))
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname)) //Appending extension
  }
});
const uploadTo2 = multer({ storage: storage2 });


router.post('/createevent',uploadTo.fields([
    { 
      name: 'photo', 
      maxCount: 10
    }
  ]
  ),EventController.createevent);
router.get('/makeEventasactive/:id',EventController.makeEventasactive)
router.get('/makeEventasInactive/:id',EventController.makeEventasInactive)
router.get('/getallevents',EventController.getallevents);
// router.get('/geteventbyid/:id',EventController.geteventbyid);
router.get('/deleteevent/:id',EventController.deleteevent);
router.post('/createrating',uploadTo.fields([
  { 
    name: 'photo', 
    maxCount: 10
  }
]
),EventController.saveratingandreview);
router.post('/createeditordeletecatagory',uploadTo2.fields([
  { 
    name: 'image', 
    maxCount: 1
  }
]
),EventController.createeditordeletecatagory)
router.post('/createeditbanner',uploadTo2.fields([
  { 
    name: 'image', 
    maxCount: 1
  }
]
),EventController.createeditbanner)
router.post('/showbanner',EventController.showbanner)


router.get('/getratingandreviewbyeventid/:event_id',EventController.getratingandreviewbyeventid)
router.get('/getratingandreviewbyuserid/:user_id',EventController.getratingandreviewbyuserid)
router.get('/deletereviewbyreviewid/:review_id',EventController.deleteratingandreview)
router.post('/createeditordeleteorganizer',EventController.createeditordeleteorganizer)

router.post('/bookaevent',EventController.bookaevent)
router.get('/getallecatagory',EventController.getallecatagory)
router.get('/getallorganizer',EventController.getallorganizer)
router.post('/showcatagories',EventController.showcatagories)
router.post('/showorganizer',EventController.showorganizer);
router.get('/showorganizerCount',EventController.showorganizerCount);
router.get('/getOrgPayHistoryCount/:id',EventController.getOrgPayHistoryCount);
router.post('/getOrgPayHistory',EventController.getOrgPayHistory);


router.post('/showevent',EventController.showevent);
router.post('/showeventCount',EventController.showeventCount);
router.post('/showeventOngoing',EventController.showeventOngoing);
router.get('/showeventOngoingCount',EventController.showeventOngoingCount);
router.post('/showeventExpired',EventController.showeventExpired);
router.get('/showeventExpiredCount',EventController.showeventExpiredCount);

router.post('/validateticket',EventController.validateticket)
router.post('/getallevents',EventController.getallevents)
router.post('/getallevents_without_pagination',EventController.getallevents_without_pagination)

router.get('/getcatagorybyid/:id',EventController.getcatagorybyid)
router.get('/getbannerbyid/:id',EventController.getbannerbyid);
router.get('/getRandombannerbyid',EventController.getRandombannerbyid);


router.get('/getorganizerbyid/:id',EventController.getorganizerbyid)
router.post('/geteventbyid',EventController.geteventbyid);
router.get('/getSingleEvent/:id',EventController.getSingleEvent);

router.get('/getallpendingandpaidamount',EventController.getallpendingandpaidamount)
router.post('/makepaymentrequest',EventController.makepaymentrequest)
router.get('/markpaymentrequestaspaid/:id',EventController.markpaymentrequestaspaid)
router.get('/getallpaymenttransactionbyorgid/:id',EventController.getallpaymenttransactionbyorgid)
router.get("/downloadinvoice/:id",EventController.downloadinvoice);
router.get("/downloadtickets/:id",EventController.downloadtickets);

router.get('/getongoingeventsbyuserid/:id',EventController.getongoingeventsbyuserid)
router.get("/getcompletedeventsbyuserid/:id",EventController.getcompletedeventsbyuserid);

router.post('/getallbookingbyeventid',EventController.getallbookingbyeventid);
router.get('/getallbookingbyeventidCount/:id',EventController.getallbookingbyeventidCount);

router.get("/getsinglebookingbyid/:id",EventController.getsinglebookingbyid);
//favriout controller
router.post("/savefavriout", Favrioutcontroller.savefavriout);
router.get("/getallfavriouts", Favrioutcontroller.getallfavriouts);
router.get("/getfavrioutbyclientID/:client_id", Favrioutcontroller.getfavrioutbyclientID);
router.post("/deletefavorite", Favrioutcontroller.deletefavorite);

//event cart
router.post('/addEventtocart',EventController.addEventtocart);
router.post('/deleteEventfromcart',EventController.deleteEventfromcart);
router.post('/clearcart',EventController.clearcart);
router.post('/getcartbyuserid',EventController.getcartbyuserid);
router.post('/incEventtocart',EventController.incEventtocart);
router.post('/incEventtocartWeb',EventController.incEventtocartWeb);

router.post('/decEventtocart',EventController.decEventtocart);
router.post('/eventcartcheckout',EventController.eventcartcheckout);
router.post('/mark_user_fav_event_category',EventController.mark_user_fav_event_category);
router.get('/get_mark_user_fav_event_category/:user_id',EventController.get_mark_user_fav_event_category);

router.get("/createOrderInvoice/:id",EventController.createOrderInvoice);
router.get("/createOrderInvoiceQrCode/:id",EventController.createOrderInvoiceQrCode);


module.exports=router