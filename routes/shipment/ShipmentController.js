const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const async = require("async");
const base_url = process.env.BASE_URL;
const path = require("path");
const fs = require('fs');
const ShipmentModel = require("../../models/admin/ShipmentModel");
const customConstant = require('../../helpers/customConstant');
const UsersModel = require("../../models/user/User");
const InvoicesModel = require("../../models/admin/InvoicesModel");

const mongoose = require("mongoose");
//var bcrypt = require('bcryptjs');
//const jwt = require("jsonwebtoken");
//const nodemailer = require('nodemailer');
const transport = nodemailer.createTransport({
    name: "Oblack",
    host: process.env.MAILER_HOST,
    port: process.env.MAILER_PORT,
    auth: {
      user: process.env.MAILER_EMAIL_ID,
      pass: process.env.MAILER_PASSWORD,
    }
});



module.exports = {
  addShipmentSubmit:async function(req,res,next)
  {
    try{
      //console.log(req.body);return false;
      //43 value will come
      var {client_id,full_name,email,contact_person_name,mobile_number,contact_person,streat_po_box,office_code,building_number,lift,floor,intercom,locality,sender_contact_number,address,latitude,longitude,user_id,receiver_longitude,receiver_latitude,receiver_address,receiver_additional_information,receiver_intercom,receiver_apartment,receiver_floor,receiver_entrance_code,receiver_building_number,receiver_office_code,receiver_streat_po_box,receiver_locality,receiver_email,receiver_phone,receiver_contact,receiver_name,shipment_date,shipment_type,shipping_type,total_weight,quantity,width,height,shipment_note,extra_service,total_shipping_price,shipping_price,return_price,length} = req.body;
      var tracking_number = Math.floor(10000000 + Math.random() * 90000000);
      if(latitude == "" || latitude == null || longitude == "" || longitude == null||receiver_latitude == "" || receiver_latitude == null || receiver_longitude == "" || receiver_longitude == null)
      {
        var return_response = {"error":true,success: false,errorMessage:"L'adresse de l'expéditeur et du destinataire doit être une adresse google."};
        res.status(200)
        .send(return_response);
      }else{
        UsersModel.findOne({ _id:user_id}).exec((err,resultUser)=>{
          if(err)
          {
            console.log(err);
            res.status(200)
            .send({
                error: true,
                success: false,
                errorMessage: err
            });
          }else{
            if(resultUser)
            {
              // console.log(resultUser);
              // console.log(resultUser.credit_limit);
              // console.log("total_shipping_price "+total_shipping_price);
              // return false;
              if(resultUser.credit_limit >= total_shipping_price)
              {
                var remaining_credit_limit = parseFloat(resultUser.credit_limit) - parseFloat(total_shipping_price)
                if(resultUser.userLocation)
                {
                  ShipmentModel.count({},(errs,shipmentCount)=>{
                    if(errs)
                    {
                      res.status(200)
                      .send({
                          error: true,
                          success: false,
                          errorMessage: errs
                      });
                    }else{
                      console.log("shipmentCount --> 67 line no. --->"+shipmentCount);
                      if(shipmentCount == 0)
                      {
                        var order_id = "M - 1001";
                      }else{
                        let incVal = 1001+shipmentCount;
                        var order_id = "M - "+incVal;
                      }
                      //console.log("resultUser 55 line "+resultUser);return false;
                      ShipmentModel.create({
                        order_id:order_id,
                        tracking_number:tracking_number,
                        user_id:user_id,
                        full_name:full_name,
                        email: email.toLowerCase(), // sanitize: convert email to lowercase
                        contact_person_name:contact_person_name,
                        mobile_number:mobile_number,
                        contact_person: contact_person,
                        streat_po_box: streat_po_box,
                        office_code: office_code,
                        building_number: building_number,
                        lift: lift,
                        floor:floor,
                        intercom:intercom,
                        locality:locality,
                        sender_contact_number:sender_contact_number,
                        sender_address:address,
                        sender_location: {
                          type: "Point",
                          coordinates: [latitude,longitude]
                        },
                        receiver_address:receiver_address,
                        receiver_location: {
                          type: "Point",
                          coordinates: [receiver_latitude,receiver_longitude]
                        },
                        receiver_additional_information:receiver_additional_information,
                        receiver_intercom:receiver_intercom,
                        receiver_apartment:receiver_apartment,
                        receiver_floor:receiver_floor,
                        receiver_entrance_code:receiver_entrance_code,
                        receiver_building_number:receiver_building_number,
                        receiver_office_code:receiver_office_code,
                        receiver_streat_po_box:receiver_streat_po_box,
                        receiver_locality:receiver_locality,
                        receiver_email:receiver_email,
                        receiver_phone:receiver_phone,
                        receiver_contact:receiver_contact,
                        receiver_name:receiver_name,
                        shipment_date:shipment_date,
                        pickup_address:resultUser.userAddress,
                        pickup_location:resultUser.userLocation,
                        client_id:client_id,
                        shipment_type:shipment_type,
                        shipping_type:shipping_type,
                        total_weight:total_weight,
                        quantity:quantity,
                        width:width,
                        height:height,
                        shipment_note:shipment_note,
                        extra_service:extra_service,
                        total_shipping_price:total_shipping_price,
                        return_price:return_price,
                        shipping_price:shipping_price,
                        length:length,
                      },function(err,result){
                        if(err)
                        {
                          //console.log(err);
                          var return_response = {"error":true,success: false,errorMessage:err};
                            res.status(200)
                            .send(return_response);
                        }else{
                          UsersModel.updateOne({ _id:user_id }, 
                          {credit_limit:remaining_credit_limit}, function (uerr, docs) {
                          if (uerr)
                          {
                            console.log(uerr);
                            res.status(200)
                            .send({
                              error: true,
                              success: false,
                              errorMessage: uerr,
                              //userRecord:user
                            });
                          }else{
                              var return_response = {"error":false,success: true,errorMessage:"Informations sur le client ajoutées avec succès"};
                              res.status(200)
                              .send(return_response);
                            }
                          });
                        }
                      });
                    }
                  });
                  
                }else{
                  var return_response = {"error":true,success: false,errorMessage:"L'adresse de votre profil est incorrecte. Veuillez d'abord mettre à jour l'adresse de votre profil."};
                  res.status(200)
                  .send(return_response);
                }
              }else{
                var return_response = {"error":true,success: false,errorMessage:"Votre limite est dépassée, contactez l'administrateur."};
                res.status(200)
                .send(return_response);
              }
            }else{
              var return_response = {"error":true,success: false,errorMessage:"Détails de l'utilisateur non valides, vous pouvez vous reconnecter et continuer."};
              res.status(200)
              .send(return_response);
            }
          }
        });

        
      }
    }catch(error)
    {
      console.log(error);
    }
  },
  allShipment:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        //console.log(String_qr);	    	
	      ShipmentModel.find( String_qr, {}).skip(fromindex).limit(perPageRecord).sort({created_at:-1}).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allShipmentCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        var { user_id } = req.body;
        var String_qr = {}; 
        String_qr['user_id'] = user_id;
        ShipmentModel.countDocuments(String_qr).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  getShipmentDetail:async function(req,res,next)
	{
    try{
      //mongoose.set('debug', true);
      var { order_id } = req.body;
      var String_qr = {}; 
      String_qr['_id'] = order_id;
      //console.log(String_qr);	    	
      ShipmentModel.find( String_qr, {}).exec((err, result)=>
      {
        if(err)
        {
          console.log(err);
        }else{
          //console.log("result ");
          //console.log(typeof result);
          //console.log("result"+result);
          if(result)
          {
            var return_response = {"error":false,errorMessage:"Succès","record":result};
            res.status(200).send(return_response);
          }else{
            var return_response = {"error":true,errorMessage:"Pas d'enregistrement","record":result};
            res.status(200).send(return_response);
          }
        }
      });
    }catch(err){
      console.log(err);
      var return_response = {"error":true,errorMessage:err };
      res.status(200).send(return_response);
    }
	},

  allMerchantInvoice:async function(req,res,next)
	{
		
	    var xx = 0;
	    var total = 0;
	    var perPageRecord = 10;
	    var fromindex = 0;
	    if(req.body.numofpage == 0)
	    {
	      fromindex = 0;
	    }else{
	      fromindex = perPageRecord * req.body.numofpage
	    }
	    try{
	    	//mongoose.set('debug', true);
        //console.log(String_qr);
				const queryJoin = [
					{
						path :"user_id",
						select:['first_name','last_name','phone','email']
					}
				];
	      InvoicesModel.find( {user_id:req.body.user_id}, {}).populate(queryJoin).skip(fromindex).limit(perPageRecord).lean(true).exec((err, result)=>
	      {
	        if(err)
	        {
	          console.log(err);
	        }else{
	          //console.log("result ");
	          //console.log(typeof result);
	          //console.log("result"+result);
	          if(result.length > 0)
	          {
	             var return_response = {"error":false,errorMessage:"Succès","record":result};
	                  res.status(200).send(return_response);
	          }else{
	            var return_response = {"error":false,errorMessage:"Pas d'enregistrement","record":result};
	            res.status(200).send(return_response);
	          }

	        }
	      });

	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
  allMerchantInvoiceCount:async function(req,res,next)
	{
	    let total = 0;
	    var totalPageNumber = 1;
	    var perPageRecord = 10;
	    //console.log("here");
	    //console.log(req.body);

	    try{
        
        InvoicesModel.countDocuments({user_id:req.body.user_id}).exec((err, totalcount)=>
	        {
	          //console.log("countResult "+totalcount);
	          if(err)
	          {
	            console.log(err);
	          }else{
	            total = totalcount;
	            totalPageNumber = total / perPageRecord;
	            //console.log("totalPageNumber"+totalPageNumber);
	            totalPageNumber = Math.ceil(totalPageNumber);
	            //console.log("totalPageNumber"+totalPageNumber);
	            //console.log("totalcount new fun"+totalcount);
	            //console.log("all record count "+totalcount);
	            var return_response = {"error":false,errorMessage:"Succès","totalPageNumber":totalPageNumber};
	            res.status(200).send(return_response);
	          }
	        });
	       
	    }catch(err){
	      console.log(err);
        var return_response = {"error":true,errorMessage:err };
        res.status(200).send(return_response);
	    }
	},
};