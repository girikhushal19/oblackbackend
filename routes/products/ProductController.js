const { Product } = require("../../models/products/Product");
const  ProductModel  = require("../../models/products/Product");
const ProductstempimagesModel = require("../../models/products/ProductstempimagesModel");
const MatCatagory=require("../../models/products/MatCatagory");
const Variations =require("../../models/products/Variations");
const VariationsPrice =require("../../models/products/VariationPrice");
const pageViewsModel=require("../../models/products/pageViews");
const mongoose=require("mongoose");
const catagorymodel = require("../../models/products/Catagory");
const ProductfavoriteModel = require("../../models/products/ProductfavoriteModel");
const Subscriptions=require("../../models/subscriptions/Subscription");
const ShippingModel = require("../../models/shipping/Shipping");
const GlobalSettings=require("../../models/admin/GlobalSettings");
const SellerModel=require("../../models/seller/Seller");
const fs = require("fs");
module.exports = {
    tempImageUpload: async (req, res) => 
    {
        try{
            //ProductstempimagesModel
            let images = [];
            var random_product_id = req.body.random_product_id;
            if(random_product_id)
            {
                var productRec = await ProductstempimagesModel.findOne({random_product_id});
                if(productRec)
                {
                    console.log(productRec);
                    images = productRec.images;
                    if (req.files?.photo)
                    {
                        req.files.photo?.map((image) =>{
                            //console.log("photo",image)
                            let filename = process.env.PHOTOS_PATH_PERFIX+"/" + image.filename;
                            images.push(filename);
                        })
                    }
                    await ProductstempimagesModel.updateOne({random_product_id:random_product_id},{images:images}).then((val)=>{
                        ProductstempimagesModel.findOne({random_product_id}).exec((err,allData)=>{
                            if(err)
                            {
                                return res.send({
                                    status: false,
                                    message: err
                                })
                            }else{
                                return res.send({
                                    status: true,
                                    message: "Succès",
                                    data:allData
                                })
                            }
                        });
                    }).catch((error)=>{
                        return res.send({
                            status: false,
                            message: error.message
                        })
                    });
                }else{
                    return res.send({
                        status: false,
                        message: "Identifiant invalide",
                        data:allData
                    })
                }
            }else{
                let images = [];
                var random_product_id = Math.floor(10000000 + Math.random() * 90000000);
                if (req.files?.photo) {
                    req.files.photo?.map((image) => {
                        //console.log("photo",image)
                        let filename = process.env.PHOTOS_PATH_PERFIX+"/" + image.filename;
                        images.push(filename);
                    })
                }
                await ProductstempimagesModel.create({
                    random_product_id:random_product_id,
                    images:images
                }).then((result)=>{
                    ProductstempimagesModel.findOne({random_product_id}).exec((err,allData)=>{
                        if(err)
                        {
                            return res.send({
                                status: false,
                                message: err
                            })
                        }else{
                            return res.send({
                                status: true,
                                message: "Succès",
                                data:allData
                            })
                        }
                    });
                }).catch((error)=>{
                    return res.send({
                        status: false,
                        message: error.message
                    })
                });
            }
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    createproduct: async (req, res) => {
        
        // catagory: 63f5e12685565c422873b105
        // sub_cat_1: mango
          
        // condition: new  
          
        // spec_attributes: [object Object],[object Object]
        var {
            sku,
            title,
            stock,
            description,
            price,
            weight,
            id,
            parent_sku,
            catagory,
            provider_id,
            random_product_id,
            dangerous_goods,
            condition
        } = req.body;
        var available_product_upload_limit = 0;
        
        let shipping_condition_check = false;
        

        //console.log(req.body);
        //return false;

        var products_record = "";
        var enable_variation = req.body.enable_variation;
        var seller_will_bear_shipping = req.body.seller_will_bear_shipping;
        if(seller_will_bear_shipping == 'true')
        {
            seller_will_bear_shipping = true;
        }else{
            seller_will_bear_shipping = false;
        }
        if(enable_variation == "false")
        {
            enable_variation = false;
        }else{
            enable_variation = true;
        }
        
        if(id != "")
        {
            const subscription = await Subscriptions.findOne({provider_id:provider_id,payment_status:true}).sort({"created_at":-1});
            //console.log("subscription ",subscription);
            if(!subscription)
            {
                return res.send({
                    status: false,
                    message: "Veuillez souscrire à un forfait, puis vous pourrez télécharger le produit.",
                })
            }else{
                available_product_upload_limit = subscription.Numberofitems;
                //console.log("subscription.payment_amount ",subscription.payment_amount);
                if(subscription.payment_amount > 0)
                {
                    const subscription_check = await Subscriptions.findOne({provider_id:provider_id,end_date:{$gte:new Date()},payment_status:true}).sort({"created_at":-1});
                    if(!subscription_check)
                    {
                        return res.send({
                            status: false,
                            message: "Veuillez souscrire à un forfait, puis vous pourrez télécharger le produit.",
                        })
                    }else{
                        available_product_upload_limit = subscription_check.Numberofitems;
                    }
                }
            }
            //console.log("available_product_upload_limit ",available_product_upload_limit);
            
            const d = new Date();
            let month_date = d.getMonth();
            month_date = month_date + 1;
            //console.log("month_date ",month_date);
            var product_total_count = await Product.aggregate([
                {$project: {title: 1, month: {$month: '$created_at'}}},
                {$match: {month: month_date}}
              ]);
            //console.log("product_total_count ",product_total_count.length);
            //return false;
            if(available_product_upload_limit <= product_total_count.length)
            {
                return res.send({
                    status: false,
                    message: "Votre limite est dépassée pour le téléchargement du produit de ce mois.",
                })
            }
            //id
            products_record = await Product.findById(id,{images:1,video:1});
            //console.log(products_record);
        }
        //return false;

        //console.log(seller_will_bear_shipping ," seller_will_bear_shipping");return false;
        var category_id = req.body.category_id;
        if(category_id)
        {
            category_id = JSON.parse(category_id);
            //console.log(category_id);

            let valuesArray = Object.values(category_id);
            
           
            // console.log(valuesArray);
            category_id = valuesArray;
            //console.log("category_id");
            
            //console.log(category_id);
            if(category_id.length > 0)
            {
                let aa = category_id.length -1;
                
                //console.log(category_id[aa]);
                let cat_record = await catagorymodel.findOne({_id:category_id[aa]},{title:1});
                //console.log("cat_record ",cat_record);
                //return false;
                if(cat_record)
                {
                    catagory = cat_record.title;
                }
            }
            //return false;
        }else{
            category_id = [];
        }
        let images = [];
        let video;
        var newCat_id = [];
         
        
        if(random_product_id)
        {
            var productRec = await ProductstempimagesModel.findOne({random_product_id});
            if(productRec)
            {
                //console.log(productRec);
                images = productRec.images;
            }
            
        }else{
            if (req.files?.photo)
            {
                if(products_record)
                {
                    if(products_record.images.length > 0)
                    {
                        products_record.images.map((val)=>{
                            images.push(val);
                        });
                    }
                }
                
                //images.push(products_record.images);
                req.files.photo?.map((image) => {
                   // console.log("photo",image)
                    let filename = process.env.PHOTOS_PATH_PERFIX+"/" + image.filename;
                    images.push(filename);
                })
            }
        }
        
        if (req.files?.video) {
            //console.log("in req.files.video")
            req.files.video?.map((e) => {
              //  console.log("in req.files.video video",e)
                let filename =  process.env.PHOTOS_PATH_PERFIX+"/" + e.filename;
                video=filename;
            });

            if(products_record)
            {
                if(products_record.video)
                {
                    var uploadDir = './views/admin-panel/public';
                    let fileNameWithPath = uploadDir + products_record.video;
                    //console.log(fileNameWithPath);
                    
                    if (fs.existsSync(fileNameWithPath))
                    {
                        fs.unlink(fileNameWithPath, (err) => 
                        {
                            //console.log("unlink file error "+err);
                        });
                    }
                }
            }

        }
        // console.log("images ",images);
        // return false;
        // console.log("req.files,video",video)
        // const attributes=req.body.attributes?JSON.parse(req.body.attributes):[]
        let attributes;
        let dispatch_info;
        let spec_attributes;
        let size;
        let other_details;
        
        try{
            attributes=JSON.parse(req.body.attributes)
        }catch{
            attributes=req.body.attributes
        }
        try{
            dispatch_info=JSON.parse(req.body.dispatch_info)
        }catch{
            dispatch_info=req.body.dispatch_info
        }

        try{
            spec_attributes=JSON.parse(req.body.spec_attributes)
        }catch{
            spec_attributes=req.body.spec_attributes
        }
        //console.log("spec_attributes ",spec_attributes);return false;
        try{
            size=JSON.parse(req.body.size)
        }catch{
            size=req.body.size
        }
        try{
            other_details=JSON.parse(req.body.other_details)
        }catch{
            other_details=req.body.other_details
        }
        // const sales_info_variation_prices=JSON.parse(req.body.sales_info_variation_prices)
        // const sales_info=req.body.sales_info?JSON.parse(req.body.sales_info):[]
        // const dispatch_info=req.body.dispatch_info?JSON.parse(req.body.dispatch_info):[]
        // const dispatch_info=req.body.dispatch_info;
        // const size=req.body.size?JSON.parse(req.body.size):[]
        // const size=req.body.size;
        // const other_details=req.body.other_details?JSON.parse(req.body.other_details):[]
        // const other_details=req.body.other_details;
        // const variations=sales_info;
        // const variationsIDS=[];
        if(seller_will_bear_shipping == true)
        {

            const gloabalsettings=await GlobalSettings.findOne({});
            const provider= await SellerModel.findById(provider_id);
            let plusperproduct = 0;
            let productcommission = 0;
            if(provider)
            {
                if(provider?.sub_id)
                {
                    //sub_id = mongoose.Types.ObjectId(provider.sub_id)
                    let sub_id = provider.sub_id
                    const subscription = await Subscriptions.findOne({_id:sub_id,payment_status:true}).sort({"created_at":-1});
                    //console.log("subscription ",subscription);
                    
                    let product_qty = 1;
                    if(subscription)
                    {
                        plusperproduct = subscription.plusperproduct * product_qty;
                        productcommission = ((subscription.extracommission*price)/100)*product_qty;
                    }else{
                        plusperproduct = gloabalsettings?.plusperproduct*product_qty||0;
                        productcommission = ((gloabalsettings?.commission*price)/100)*product_qty||0;
                    }
                }
                // console.log("plusperproduct  ", plusperproduct);
                // console.log("productcommission  ", productcommission);
            }
            let pro_totla_comision_price = plusperproduct + productcommission;
            let product_final_price_after_commission = price - pro_totla_comision_price;

            //return false;

            let dispatch_info_kk;
            try{
                dispatch_info_kk = JSON.parse(req.body.dispatch_info)
            }catch{
                dispatch_info_kk = req.body.dispatch_info
            }
            const product_weight = dispatch_info_kk.weight; 
            // console.log("dispatch_info_kk ",dispatch_info_kk);
            // console.log("dispatch_info_kk.weight ",dispatch_info_kk.weight);
            const product_totalweight = dispatch_info_kk.weight;
            const product_totalweight_in_gm = product_totalweight * 1000;
            //console.log("product_totalweight_in_gm ",product_totalweight_in_gm);
            
            var shipping_record = await ShippingModel.find({},{max_weight:1,cost_base:1,cose_per_unit:1,number_of_days:1,cost_additional_weight:1});
            //console.log("shipping_record ", shipping_record);
            for(let x=0; x<shipping_record.length; x++)
            {
                const max_weight_in_gm = shipping_record[x].max_weight * 1000;
                const cost_additional_weight = shipping_record[x].cost_additional_weight * 1000;
                const cose_per_unit = shipping_record[x].cose_per_unit;
                let shipping_price = shipping_record[x].cost_base;

               // console.log("max_weight_in_gm ",max_weight_in_gm);

                //console.log("394 no. line  shipping_price ",shipping_price);

                if(product_totalweight_in_gm > max_weight_in_gm)
                {
                    let remaining_weight = product_totalweight_in_gm - max_weight_in_gm;
                    let one_unit_in_gm =remaining_weight / cost_additional_weight;

                    //console.log("remaining_weight ",remaining_weight);
                    //console.log("cost_additional_weight ",cost_additional_weight);

                    //console.log("one_unit_in_gm ",one_unit_in_gm);
                    
                    let total_additional_price = one_unit_in_gm * cose_per_unit;

                    

                    //console.log("total_additional_price ",total_additional_price);
                    
                    shipping_price = shipping_price + total_additional_price;
                    if(shipping_price >= product_final_price_after_commission)
                    {
                        shipping_condition_check = true;
                    }
                }
                
                console.log("411 no. line  product_final_price_after_commission ", product_final_price_after_commission);
                console.log("411 no. line  shipping_price ",shipping_price);
                console.log("====================================> for loop end ");
            }

        }
        if(shipping_condition_check == true)
        {
            return res.send({
                status:false,
                message:"Vous ne pouvez pas payer les frais d'expédition pour ce produit. Veuillez désactiver la bascule."
            })
        }
        // 
        // console.log("seller_will_bear_shipping ", seller_will_bear_shipping);
        // return false;
        if (id)
        {
            if(condition)
            {
                if(condition == 'new')
                {
                    condition = true
                }else{
                    condition = false;
                }
            }
            
            const datatoupdate={
                ...(condition && { condition: condition }),
                ...(dangerous_goods && { dangerous_goods: dangerous_goods }),
                ...(sku && { sku: sku }),
                ...(stock && { stock: stock }),
                ...(provider_id && { provider_id: provider_id }),
                ...(parent_sku&&{parent_sku:parent_sku}),
                ...(title && { title: title }),
                ...(description && { description: description }),
                ...(price && { price: price }),
                ...(catagory && { catagory: catagory }),
                ...(category_id && { category_id: category_id }),
                ...(images.length && { images: images }),
                ...(video && { video: video }),
                ...(enable_variation?.toString() && { enable_variation: enable_variation }),
                ...(seller_will_bear_shipping?.toString() && { seller_will_bear_shipping: seller_will_bear_shipping}),
                ...(spec_attributes && { spec_attributes: spec_attributes }),
                //...(attributes && { spec_attributes: attributes }),
                
                // ...(sales_info?.length && { sales_info: variations }),
                // ...(sales_info_variation_prices.length && { sales_info_variation_prices: sales_info_variation_prices }),
                ...(dispatch_info && { dispatch_info: dispatch_info }),
                ...(other_details && { other_details: other_details }),
            };
           
            // console.log("req.body",size,weight,datatoupdate);
            // return false;
            await Product.findByIdAndUpdate(id, datatoupdate).then((result) => {
                //console.log("result",result)
                return res.send({
                    status: true,
                    data:result,
                    message: "Produit mis à jour avec succès"
                })
            })

        } else {
          // console.log("hereeee");
            if(condition == 'new')
            {
                condition = true
            }else{
                condition = false;
            }
           
            const product = new Product();
            product.sku = sku;
            product.stock = stock;
            product.title = title;
            product.provider_id=provider_id;
            product.description = description;
            product.price = price;
            product.images = images;
            // product.sales_info = variations;
            product.seller_will_bear_shipping = seller_will_bear_shipping;
            product.enable_variation = enable_variation;
            // product.sales_info_variation_prices=sales_info_variation_prices;
            product.video = video;
            product.parent_sku=parent_sku;
            product.spec_attributes = spec_attributes;
            product.catagory = catagory;
            product.dispatch_info = dispatch_info;
            product.other_details = other_details;
            product.category_id = category_id;
            product.dangerous_goods = dangerous_goods;
            product.condition = condition;
            
            product.save()
                .then((result) => {
                    //console.log(result._id)
                    ProductstempimagesModel.deleteOne({random_product_id:random_product_id}).exec((err,val)=>{
                        return res.send({
                            status: true,
                            message: "Produit enregistré avec succès",
                            product_id:result._id
                        })
                    });
                    
                    
                })
        }
    },
    createproductTest:async(req,res)=>{
        // console.log(req.file);
        // console.log(req.files);
        // console.log(req.body);


        // title: hfghghg
        // description: gfhgfh gfhtry tytry bnvbn kjjki ghfgh
        // catagory: 63f5e12685565c422873b105
        // sub_cat_1: mango
        // dangerous_goods: true
        // dispatch_info: [object Object]
        // seller_will_bear_shipping: false
        // condition: new
        // sku: KHKJHJGFDS
        // enable_variation: simple
        // price: 500
        // stock: 100
        // photo: (binary)
        // video: (binary)
        // spec_attributes: [object Object],[object Object]


    },
    createAttribute: async (req, res) =>
    {
        try{
            var id = req.body.id;
            var main_attr = req.body.main_attr;
             main_attr = JSON.parse(main_attr);
        //console.log(req.body);
        //console.log( JSON.stringify(req.body.main_attr));
        // console.log( JSON.stringify(req.body.main_attr[0].option_value_1));
        // console.log( JSON.stringify(req.body.main_attr[1].option_value_2));
         var variation_1 = ""; var option_value_1 = "";
         var variation_2 = ""; var option_value_2 = "";
         
        var sub_attr_1 = req.body.sub_attr_1;  var sub_attr_2 = req.body.sub_attr_2;
        var sub_attr_3 = req.body.sub_attr_3;  var sub_attr_4 = req.body.sub_attr_4;
        var sub_attr_5 = req.body.sub_attr_5;  var sub_attr_6 = req.body.sub_attr_6;
        var sub_attr_7 = req.body.sub_attr_7;  var sub_attr_8 = req.body.sub_attr_8;
        var sub_attr_9 = req.body.sub_attr_9;  var sub_attr_10 = req.body.sub_attr_10;
        
        var arr_attribute = [];
        if(main_attr)
        {
            if(main_attr.length > 1)
            {
                variation_1 = main_attr[0].variation_1;
                option_value_1 = main_attr[0].option_value_1;

                variation_2 = main_attr[1].variation_2;
                option_value_2 = main_attr[1].option_value_2;

                // console.log(variation_1);
                // console.log(option_value_1);
                // console.log(variation_2);
                // console.log(option_value_2);
            }else if(main_attr.length > 0)
            {
                var variation_1 = main_attr[0].variation_1;
                var option_value_1 = main_attr[0].option_value_1;
            }

            let m = 1;
            if(variation_1 != "" && option_value_1 != "" && variation_2 != "" && option_value_2 != "")
            {
                //console.log("iffff 176");
                for(let x=0; x < option_value_1.length; x++)
                {
                    let n = 1;
                    for(let y=0; y< option_value_2.length; y++)
                    {
                        // console.log(variation_1);
                        // console.log(option_value_1[x].option);
                        // console.log(variation_2);
                        // console.log(option_value_2[y].option);
                        if(m == 1)
                        {
                            //console.log("sub_attr_1");
                            if(sub_attr_1)
                            {
                                // console.log(sub_attr_1[y]);
                                // console.log(sub_attr_1[y].price);
                                // console.log(sub_attr_1[y].stock);
                                // console.log(sub_attr_1[y].sku);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_1)
                                    {
                                        if(req.files.sub_attr_image_1.length > 0)
                                        {
                                            images = req.files.sub_attr_image_1.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }
                                //console.log("images ",images);
                                sub_attr_1 = JSON.parse(req.body.sub_attr_1);
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_1[y].sku,
                                    price:sub_attr_1[y].price,
                                    stock:sub_attr_1[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 2)
                        {
                            //console.log("sub_attr_2");
                            if(sub_attr_2)
                            {
                                // console.log(sub_attr_2[y]);
                                // console.log(sub_attr_2[y].price);
                                // console.log(sub_attr_2[y].stock);
                                // console.log(sub_attr_2[y].sku);

                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_2)
                                    {
                                        if(req.files.sub_attr_image_2.length > 0)
                                        {
                                            images = req.files.sub_attr_image_2.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }
                                //console.log("images ",images);

                                sub_attr_2 = JSON.parse(req.body.sub_attr_2);
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_2[y].sku,price:sub_attr_2[y].price,stock:sub_attr_2[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 3)
                        {
                            //console.log("sub_attr_3");
                            if(sub_attr_3)
                            {
                                // console.log(sub_attr_3[y]);
                                // console.log(sub_attr_3[y].price);
                                // console.log(sub_attr_3[y].stock);
                                // console.log(sub_attr_3[y].sku);
                                sub_attr_3 = JSON.parse(req.body.sub_attr_3);

                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_3)
                                    {
                                        if(req.files.sub_attr_image_3.length > 0)
                                        {
                                            images = req.files.sub_attr_image_3.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }

                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_3[y].sku,price:sub_attr_3[y].price,stock:sub_attr_3[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 4)
                        {
                            //console.log("sub_attr_4");
                            if(sub_attr_4)
                            {
                                // console.log(sub_attr_4[y]);
                                // console.log(sub_attr_4[y].price);
                                // console.log(sub_attr_4[y].stock);
                                // console.log(sub_attr_4[y].sku);
                                sub_attr_4 = JSON.parse(req.body.sub_attr_4);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_4)
                                    {
                                        if(req.files.sub_attr_image_4.length > 0)
                                        {
                                            images = req.files.sub_attr_image_4.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }

                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_4[y].sku,price:sub_attr_4[y].price,stock:sub_attr_4[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 5)
                        {
                            //console.log("sub_attr_5");
                            if(sub_attr_5)
                            {
                                // console.log(sub_attr_5[y]);
                                // console.log(sub_attr_5[y].price);
                                // console.log(sub_attr_5[y].stock);
                                // console.log(sub_attr_5[y].sku);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_5)
                                    {
                                        if(req.files.sub_attr_image_5.length > 0)
                                        {
                                            images = req.files.sub_attr_image_5.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }

                                sub_attr_5 = JSON.parse(req.body.sub_attr_5);
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_5[y].sku,price:sub_attr_5[y].price,stock:sub_attr_5[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 6)
                        {
                            //console.log("sub_attr_6");
                            if(sub_attr_6)
                            {
                                // console.log(sub_attr_6[y]);
                                // console.log(sub_attr_6[y].price);
                                // console.log(sub_attr_6[y].stock);
                                // console.log(sub_attr_6[y].sku);
                                sub_attr_6 = JSON.parse(req.body.sub_attr_6);

                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_6)
                                    {
                                        if(req.files.sub_attr_image_6.length > 0)
                                        {
                                            images = req.files.sub_attr_image_6.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }

                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_6[y].sku,price:sub_attr_6[y].price,stock:sub_attr_6[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 7)
                        {
                            //console.log("sub_attr_7");
                            if(sub_attr_7)
                            {
                                // console.log(sub_attr_7[y]);
                                // console.log(sub_attr_7[y].price);
                                // console.log(sub_attr_7[y].stock);
                                // console.log(sub_attr_7[y].sku);
                                sub_attr_7 = JSON.parse(req.body.sub_attr_7);

                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_7)
                                    {
                                        if(req.files.sub_attr_image_7.length > 0)
                                        {
                                            images = req.files.sub_attr_image_7.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }

                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_7[y].sku,price:sub_attr_7[y].price,stock:sub_attr_7[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 8)
                        {
                            //console.log("sub_attr_8");
                            if(sub_attr_8)
                            {
                                // console.log(sub_attr_8[y]);
                                // console.log(sub_attr_8[y].price);
                                // console.log(sub_attr_8[y].stock);
                                // console.log(sub_attr_8[y].sku);
                                sub_attr_8 = JSON.parse(req.body.sub_attr_8);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_8)
                                    {
                                        if(req.files.sub_attr_image_8.length > 0)
                                        {
                                            images = req.files.sub_attr_image_8.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_8[y].sku,price:sub_attr_8[y].price,stock:sub_attr_8[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 9)
                        {
                            //console.log("sub_attr_9");
                            if(sub_attr_9)
                            {
                                // console.log(sub_attr_9[y]);
                                // console.log(sub_attr_9[y].price);
                                // console.log(sub_attr_9[y].stock);
                                // console.log(sub_attr_9[y].sku);
                                sub_attr_9 = JSON.parse(req.body.sub_attr_9);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_9)
                                    {
                                        if(req.files.sub_attr_image_9.length > 0)
                                        {
                                            images = req.files.sub_attr_image_9.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_9[y].sku,price:sub_attr_9[y].price,stock:sub_attr_9[y].stock,images:images,
                                    product_id:id};
                                arr_attribute.push(jj);
                            }
                        }
                        if(m == 10)
                        {
                            //console.log("sub_attr_10");
                            if(sub_attr_10)
                            {
                                // console.log(sub_attr_10[y]);
                                // console.log(sub_attr_10[y].price);
                                // console.log(sub_attr_10[y].stock);
                                // console.log(sub_attr_10[y].sku);
                                sub_attr_10 = JSON.parse(req.body.sub_attr_10);
                                let  images = [];
                                if(req.files)
                                {
                                    if(req.files.sub_attr_image_10)
                                    {
                                        if(req.files.sub_attr_image_10.length > 0)
                                        {
                                            images = req.files.sub_attr_image_10.map((vall_i)=>{
                                                //console.log(vall_i);
                                                return vall_i.filename;
                                            })
                                        }
                                        
                                    }
                                }
                                let jj = {
                                    variation_1:variation_1,
                                    variation_option_1:option_value_1[x].option,
                                    variation_2:variation_2,
                                    variation_option_2:option_value_2[y].option,
                                    sku:sub_attr_10[y].sku,price:sub_attr_10[y].price,stock:sub_attr_10[y].stock,
                                    images:images,
                                    product_id:id
                                };
                                arr_attribute.push(jj);
                            }
                        }
                        n++;
                    }
                    m++;
                }
            }
        }
        //console.log("arr_attribute ",arr_attribute);

        if(arr_attribute.length > 0)
        {
            await Product.findByIdAndUpdate(id,{
                "sales_info_variation_prices":arr_attribute
            }).then((result)=>{
                return res.send({
                    status:true,
                    message:"Attribut ajouté succès complet"
                })
            }).catch((error)=>{
                return res.send({
                    status: false,
                    message: error.message
                })
            })
        }else{
            return res.send({
                status:false,
                message:"Rien à ajouter"
            })
        }
         //return false;
        }catch(error){
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    editVariation:async(req,res)=>{
        try{
            //console.log(req.body);
            const {product_id,id,price,stock,sku} = req.body;

            //console.log("product_id ",product_id);
            var product_record = await Product.findOne({_id:product_id},{sales_info_variation_prices:1});
            //console.log("product_record ",product_record);
            if(product_record)
            {
                if(product_record.sales_info_variation_prices.length > 0)
                {
                    const old_arr = product_record.sales_info_variation_prices;
                    //console.log("product_record ",old_arr);
                    var newArray = old_arr.filter((val)=>{
                        //console.log(val);
                        if(val._id.toString() == id)
                        {
                            //console.log("iffffff");
                            //console.log(val);
                            val["price"] = price;
                            val["stock"] = stock;
                            val["sku"] = sku;
                        }
                        return val;
                    });
                    //console.log(newArray);
                    await Product.updateOne({_id:product_id},
                        {sales_info_variation_prices:newArray}).then((result)=>{
                            return res.send({
                                status: true,
                                message: "Le dossier de modification a été mis à jour avec succès"
                            })
                        }).catch((error)=>{
                            return res.send({
                                status: false,
                                message: error.message
                            })
                        })
                }else{
                    return res.send({
                        status: false,
                        message: "Identifiant de produit invalide"
                    });
                }
            }else{
                return res.send({
                    status: false,
                    message: "Identifiant de produit invalide"
                });
            }
        }catch(error){
            return res.send({
                status: false,
                message: error.message
            })
        }
    },
    deleteproduct: async (req, res) => {
        try{
            const id = req.params.id;
            await Product.updateOne({_id:id},{is_deleted:true})
            .then((result) => {
                return res.send({
                    status: true,
                    message: "Supprimé avec succès"
                })
            }).catch((e)=>{
                return res.send({
                    status: false,
                    message: e.message
                })
            });
        }catch(e)
        {
            return res.send({
                status: false,
                message: e.message
            })
        }
        
    },
    restoreproduct: async (req, res) => {
        try{
            const id = req.params.id;
            await Product.updateOne({_id:id},{is_deleted:false})
            .then((result) => {
                return res.send({
                    status: true,
                    message: "Restauration du produit entièrement réussie"
                })
            }).catch((e)=>{
                return res.send({
                    status: false,
                    message: e.message
                })
            });
        }catch(e)
        {
            return res.send({
                status: false,
                message: e.message
            })
        }
        
    },
    getallproductsforuser: async (req, res) => {
        try{
            // var randomNumber = Array.from(Array(4), () => Math.floor(Math.random() * 36).toString(36)).join('');
            // console.log("randomNumber ",randomNumber);
            // return false;
            let singleid=req.body.singleid;
            const limit =10;
            var pagno=req.body.pageno;
            pagno = pagno ? pagno : 0;
            let skip=pagno*10;
            //console.log("req.body",req.body)
            if(singleid){
                //mongoose.set("debug",true);
                singleid=mongoose.Types.ObjectId(singleid);
                //return false;
                
                
                var user_id = req.body.user_id;
                var products = await Product.aggregate([
                    {$match:{"_id":singleid}},
                    {
                        $lookup:{
                            from:"catagories",
                            localField:"catagory",
                            foreignField:"title",
                            as:"mcatprev"
                        }
                    },
                    {
                    $addFields:{
                        "mcat":{$arrayElemAt:["$mcatprev",0]}
                    }
                    },
                    {$lookup:{ 
                        from:"catagories",
                        let:{"mastercatagory":"$mcat._id"},
                        pipeline:[
                            {$match:{$expr:{$eq:["$mastercatagory","$$mastercatagory"]}}},
                        ],
                        as:"subcatagories"
                    }},
                    {
                        $lookup:{
                            from:"ratingnreviews",
                            let:{"prod_id":{"$toString":"$_id"}},
                            //let:{productidstring:{"$toString":"$_id"}},
                            pipeline:[
                                // {$match:{
                                //     $expr:{
                                //         $eq:["$product_id","$$productidstring"]
                                        
                                //     }
                                // }}
                                {
                                    $match:{
                                        $expr:{
                                            $eq:["$product_id","$$prod_id"]
                                        }
                                    }
                                },
                                //{$match:{$expr:{"product_id":"$$prod_id"}}},
                                {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                                {
                                    $skip:0
                                },
                                {
                                    $limit:15
                                },
                                {
                                    $sort:{"updatedAt":-1}
                                },
                                {
                                    $lookup:{
                                        from:"users",
                                        localField:"userIDOBJ",
                                        foreignField:"_id",
                                        as:"userwhorated"
                                    }
                                },
                                {
                                    $project:{
                                        "userwhorated.password":0,
                                        "userwhorated.address":0,
                                        "userwhorated.cart":0,
                                        "userwhorated.eventcart":0,
                                        "userwhorated.reset_password_token":0,
                                        "userwhorated.reset_password_expires":0,
                                        "userwhorated.phone":0,
                                        "userwhorated.email":0,
                                        "userwhorated.companyName":0,
                                        "userwhorated.userAddress":0,
                                        "userwhorated.forgotPasswordLink":0,
                                        "userwhorated.forgotPasswordTime":0,
                                        "userwhorated.forgotPasswordUsed":0,
                                        "userwhorated.is_deleted":0,
                                        "userwhorated.is_active":0,
                                        "userwhorated.isverfied":0,
                                        "userwhorated.credit_limit":0,
                                        "userwhorated.extProvider":0,
                                        "userwhorated.createdAt":0,
                                        "userwhorated.updatedAt":0,
                                        "userwhorated.verification_code":0,
                                        "userwhorated.fcm_token":0,
                                        "userwhorated.token":0,
                                        "userwhorated.fav_event_cat":0,
                                        "userwhorated.otp_date_time":0,
                                        "userwhorated.otp_used_or_not":0,
                                        "userwhorated.country_code":0,
                                    }
                                }
                            ],
                            as:"reviews"
                        },
                        
                    },
                    {
                        $project:{
                            "reviews.media":0,
                            "reviews.replies":0,
                            "reviews.product_id":0,
                            "reviews.user_id":0,
                            "reviews.provider_id":0,
                            "reviews.order_id":0,
                            "reviews.__v":0,

                        }
                    },
                    {
                        $lookup:{
                            from:"ratingnreviews",
                            let:{"prod_id":{"$toString":"$_id"}},
                            //let:{productidstring:{"$toString":"$_id"}},
                            pipeline:[
                                {
                                    $match:{
                                        $expr:{
                                            $eq:["$product_id","$$prod_id"]
                                        }
                                    }
                                },
                                {
                                $group:{
                                    "_id":"$rating",
                                    count:{$sum:1
                                    }
                                }
                                }
                            ],
                            as:"reviews_group"
                        },
                        
                    },
                    { $addFields: {
                                        
                        avgRating:{
                            $avg: "$reviews.rating"
                        }
                    }},
                    {
                        $addFields:{
                            "selr_id":{"$toObjectId":"$provider_id"}
                        }
                    },
                    {
                        $lookup:{
                            from:"sellers",
                            localField:"selr_id",
                            foreignField:"_id",
                            as:"sellr_rec"
                        }
                    },
                    {$unwind:"$sellr_rec"},
                    {
                        $addFields:{
                            "shop_name":"$sellr_rec.shopname"
                        }
                    },
                    {
                        $project:{
                            "sellr_rec":0
                        }
                    },
                    {
                        $lookup:{
                            from:"variationprices",
                            let:{productidstring:{"$toString":"$_id"}},
                            pipeline:[
                                {$match:{
                                    $expr:{
                                        $eq:["$product_id","$$productidstring"]
                                        // $and:[
                                        //     {
                                        //         $eq:["$provider_id","$$provider_id"]
                                                
                                        //     },{
                                        //         $lte:[new Date(),"$end_date"]
                                        //     }
                                        // ]
                                    }
                                }}
                            ],
                            as:"variations"
                        }
                    },
                    {
                        $addFields:{
                            pricerangeMin:{
                                $cond:{
                                    if:{$gt:[{$size:"$variations"},0]},
                                    then:{
                                        $min:"$variations.price"
                                    
                                    
                                    },else:"$price"
                            }},
                            pricerangeMax:{
                                $cond:{
                                    if:{$gt:[{$size:"$variations"},0]},
                                    then:{
                                        $max:"$variations.price"
                                    
                                    
                                    },else:"$price"
                            }},
                            // isusermarkedthisproductasabusive:{
                            //     $filter:{
    
                            //     }
                            // }
                        }
                        },
                    {
                        $lookup:{
                            from:"productfavorites",
                            let:{"pro_id":{"$toString":"$_id"}},
                            pipeline:[
                                {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$user_id","$user_id"]},
                                            {$eq:["$product_id","$$pro_id"]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                        }
                    },{
                        $addFields:{
                            "isFav":{$size:"$favProduct"}
                        }
                    } 
                ]);
                
                    
                //mongoose.set("debug",true);
                var vari = await VariationsPrice.aggregate([
                    {
                        $match:{
                            "product_id":req.body.singleid
                        }
                    },
                    { 
                        $group : 
                        { 
                            "_id" : "$variation_option_1", 
                            "first_variation": { $push: "$variation_1" } ,
                            "ad_id":{"$first":"$_id"},
                            "images":{"$first":"$images"},
                            "price":{"$first":"$price"},
                            "stock":{"$first":"$stock"},
                        } 
                    }
                ]);

                var vari_2 = await VariationsPrice.aggregate([
                    {
                        $match:{
                            "product_id":req.body.singleid
                        }
                    },
                    { 
                        $group : 
                        { 
                            "_id" : "$variation_option_2", 
                            "second_variation": { $push: "$variation_2" },
                            "ad_id":{"$first":"$_id"},
                            "price":{"$first":"$price"},
                            "stock":{"$first":"$stock"},
                        } 
                    }
                ]);
                //console.log("vari ",vari);

                //if(user_id)
                //ProductfavoriteModel
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: products,
                    vari:vari,
                    vari_2:vari_2
                })
            }else{
            //mongoose.set("debug",true);
            const opquery={is_deleted:false};
            const ipquery={};
            const rpquery={};
            const catagory=req.body.catagory;
            // const attributes=req.body.attributes;
            // const sales_info=req.body.sales_info;
            const sort=req.body.sort||1;
            const pricerangeLow= parseFloat(req.body.pricerangeLow);
            const pricerangeHigh=parseFloat(req.body.pricerangeHigh);
            const color=req.body.color;
            const countryofmfg=req.body.countryofmfg;
            const condition=req.body.condition;
            const availibility=req.body.availibility;
            const rating=parseInt(req.body.rating);
            if(condition){
                opquery['condition']=condition
            }
            if(rating){
                rpquery['rating']= parseInt(rating);
            }
            if(pricerangeLow && pricerangeHigh){
              opquery['$and']=[
                {
                    price:{$gte:pricerangeLow}
                },
                {
                    price:{$lte:pricerangeHigh}
                }
            ]
              
            }
            // if(color){
            //     opquery["sales_info"]= { $elemMatch: { variation_name: 'color',variation_options:{$elemMatch:{option_value:color}}} }
            // }
            if(catagory){
                // opquery["catagory"]=new RegExp(catagory)
                opquery["catagory"]=catagory
            }
    
           
            const regcat=new RegExp(catagory)
            //console.log("regcat",regcat,"catagory",catagory,"opquery",opquery,"pageno",pagno);
            
            var user_id = req.body.user_id;
            var products = await Product.aggregate([
                {$match:opquery},
                {
                    $lookup:{
                        from:"catagories",
                        localField:"catagory",
                        foreignField:"title",
                        as:"mcat"
                    }
                },
                {
                    $addFields:{
                        "mcatone":{$arrayElemAt: ["$mcat", 0]}
                    }
                    },
                {$lookup:{
                    from:"catagories",
                    let:{"mastercatagory":"$mcatone._id"},
                    pipeline:[
                        {$match:{$expr:{$eq:["$mastercatagory","$$mastercatagory"]}}},
                        
                        
                    ],
                    as:"subcatagories"
                }},
                {
                    $lookup:{
                        from:"ratingnreviews",
                        let:{"prod_id":{"$toString":"$_id"}},
                        //let:{productidstring:{"$toString":"$_id"}},
                        pipeline:[
                            // {$match:{
                            //     $expr:{
                            //         $eq:["$product_id","$$productidstring"]
                                    
                            //     }
                            // }}
                            {
                                $match: rpquery 
                            },
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$product_id","$$prod_id"]
                                    }
                                }
                            }
                            //{$match:{$expr:{"product_id":"$$product_id"}}},
                            // {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                            // {
                            //     $lookup:{
                            //         from:"users",
                            //         localField:"userIDOBJ",
                            //         foreignField:"_id",
                            //         as:"userwhorated"
                            //     }
                            // }
                        ],
                        as:"reviews"
                    },
                    
                        
                },
                
                { $addFields: {
                                    
                    avgRating:{
                        $avg: "$reviews.rating"
                    },
                    provideridObj:{
                        "$toObjectId":"$provider_id"
                    }
                }},
                {
                    $project:{
                        "reviews.media":0,
                        "reviews.replies":0,
                        "reviews.review":0,
                        "reviews.createdAt":0,
                        "reviews.updatedAt":0,
                        "reviews.__v":0

                    }
                },
                // {
                //     $lookup:{
                //         from:"sellers",
                //         localField:"provideridObj",
                //         foreignField:"_id",
                //         as:"seller"
                //     }
                // },
                {
                    $lookup:{
                        from:"subscriptions",
                        let:{provider_id:"$provider_id"},
                        pipeline:[
                            {$match:{
                                $expr:{
                                    $and:[
                                        {
                                            $eq:["$provider_id","$$provider_id"]
                                            
                                        },{
                                            $lte:[new Date(),"$end_date"]
                                        }
                                    ]
                                }
                            }}
                        ],
                        as:"subs"
                    }
                },
                {
                    $lookup:{
                        from:"variationprices",
                        let:{productidstring:{"$toString":"$_id"}},
                        pipeline:[
                            {$match:{
                                $expr:{
                                    $eq:["$product_id","$$productidstring"]
                                    // $and:[
                                    //     {
                                    //         $eq:["$provider_id","$$provider_id"]
                                            
                                    //     },{
                                    //         $lte:[new Date(),"$end_date"]
                                    //     }
                                    // ]
                                }
                            }}
                        ],
                        as:"variations"
                    }
                },
                {
                    $addFields:{
                        pricerangeMin:{
                            $cond:{
                                if:{$gt:[{$size:"$variations"},0]},
                                then:{
                                    $min:"$variations.price"
                                
                                
                                },else:"$price"
                        }},
                        pricerangeMax:{
                            $cond:{
                                if:{$gt:[{$size:"$variations"},0]},
                                then:{
                                    $max:"$variations.price"
                                
                                
                                },else:"$price"
                        }},
                        
                    }
                    },
                    {
                    $lookup:{
                        from:"productfavorites",
                        let:{"pro_id":{"$toString":"$_id"}},
                        pipeline:[
                            {
                                $match:{
                                    $expr:{
                                        $and:[
                                            {$eq:["$user_id",user_id]},
                                            {$eq:["$product_id","$$pro_id"]}
                                        ]
                                    }
                                }
                            }
                        ],
                        as:"favProduct"
                    }
                    },
                {$sort:{"price":sort}},
                {$addFields:{"isFav":{$size:"$favProduct"}}},
                {$skip:skip},
                {$limit:limit},
                {$project:{
                    "variations":0,
                    "favProduct":0
                }}
    
            ]);
            
                
            


            // console.log("products",products)
            // const products1 = products?.filter((pro)=>pro?.subs?.length!=0)
            const commoncatagory = products[0]?.mcatone
            const commonsubcatagory=products[0]?.subcatagories
            let newproducts=products
            let products1 = newproducts;
            //console.log(products1)
            var final_product = [];
            if(rating > 0)
            {
                final_product = products1.filter((val)=>{
                    console.log("here 652");
                    console.log(val.reviews);
                    if(val.reviews.length > 0)
                    {
                        if(val.reviews[0].rating == rating)
                        {
                            console.log("here match 659");
                            return val;
                        }
                    }
                })
            }else{
                final_product=products1;
            }
            //console.log("final_product "+final_product);
                return res.send({
                    status: true,
                    message: "fetched successfully",
                    data: {products:final_product,
                        commoncatagory:commoncatagory,
                        commonsubcatagory:commonsubcatagory
                    },
                
                })
            }
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message,
                data: [],
               
            })
        }
    },
    getallproductsforseller: async (req, res) => {
        console.log("req.body", req.body);
        const seller_id=req.body.seller_id;
        const opquery={
            provider_id:seller_id,
            is_deleted:false
        };
        const violation=req.body.violation;
        const is_stock=req.body.is_stock;
        const instock= req.body.instock;
        const removed= req.body.removed;
        const product_name=req.body.product_name;
        const catagory=req.body.catagory;
        if(product_name){
            opquery["title"]=product_name
        }
        if(catagory){
            opquery["catagory"]=catagory
        }
        if(violation){
            opquery["reported.violations"]={$gte:1}
        }
        if(instock){
            opquery["reported.violations"]={$gte:1}
        }
        if(removed){
            opquery["is_active"]=true;
        }
        if(is_stock=='true'){
            opquery["stock"]={$gte:1};
        }
        if(is_stock=='false'){
            opquery["stock"]=0;
        }
        //console.log("opquery",opquery)
        //mongoose.set("debug",true);
        const products = await Product.aggregate([
            {$match:opquery},
            // {$lookup:{
            //     from:"matcatagories",
            //     let:{"catagory":"$catagory"},
            //     pipeline:[
            //         {$match:{$expr:{"name":"$$catagory"}}},
            //     ],
            //     as:"subcatagories"
            // }},
           {
           $addFields:{
            productidstr:{"$toString":"$_id"},
            
           }
        },
          { $lookup:{
            from:"variationprices",
            localField:"productidstr",
            foreignField:"product_id",
            as:"variations"
             }
           },
           {
                $lookup:{
                    from:"productfavorites",
                    let:{"prod_id":{"$toString":"$_id"}},
                    pipeline:[
                        {
                            $match:{
                                $expr:{
                                    $eq:["$product_id","$$prod_id"]
                                }
                            }
                        }
                    ],
                    as:"productFav"
                }
           },
           {
                $addFields:{
                    "productFavCount":{$size:"$productFav"}
                }
           },
           {
            $project:{
                "productFav":0
            }
           }
        ]);
        
        return res.send({
            status: true,
            message: "fetched successfully",
            data: products
        })
    },
    getallproductsforadmin: async (req, res) => {
        try{
            //console.log("req.body ",req.body);
            const stockstatus=req.body.stockstatus;
            var is_active = req.body.is_active;
            const product_name=req.body.product_name;
            const catagory=req.body.catagory;
            const opquery={is_deleted:false};
            
            const inventoryquery={};
            // const violation=req.body.violation;
            // const instock= req.body.instock;
            const pageno= req.body.pageno;
        
            let vendor_id = req.body.vendor_id;
            //let ff = {};
            if(product_name)
            {
                opquery["title"] =  {'$regex' : '^'+product_name, '$options' : 'i'};
            }
            if(vendor_id)
            {
                opquery["provider_id"] = vendor_id;
            }
            if(catagory){
                opquery["catagory"]=catagory
            }
            if(is_active )
            {
                if(is_active == "true")
                {
                    opquery["is_active"]=true;
                }else{
                    opquery["is_active"]=false;
                }
                
            }
            
            if(stockstatus){
                if(stockstatus=="0"){
                    inventoryquery["$and"]= [
                        {$expr:{$eq:["$product_id","$$product_id"]}},
                        {$expr:{$eq:["$quantity",0]}}
                    ]
                }else if(stockstatus=="1"){
                    inventoryquery["$and"]= [
                        {$expr:{$eq:["$product_id","$$product_id"]}},
                        {$expr:{$ne:["$quantity",0]}}
                    ]
                    // inventoryquery["$expr"]= {$ne:["$quantity",0]}
                    // inventoryquery["$and"]=[
                    //     {"$product_id":"$$product_id"},
                    //     {"quantity":{$ne:0}}
                    // ]
                    // inventoryquery["product_id"]="$product_id"
                }
            }
            
            const limit=10;
            const skip=limit*pageno;
            //console.log("opquery ", opquery);
            //mongoose.set("debug",true);
            if(pageno==null||pageno=="null"){
                const count=(await Product.aggregate([
                    {$match:opquery},
                
                    {$lookup:{
                        from:"sellers",
                        let:{"provider_id":{"$toObjectId":"$provider_id"}},
                        pipeline:[
                            {$match:{
                                $expr:{$eq:["$_id","$$provider_id"]}
                            }},
                        ],
                        as:"seller"
                    }},
                    {$unwind:"$seller"},
                    {$addFields:{
                        // provider_name:{
                        //     $cond:{
                        //         if:{$gt:[{$size:"$seller"},0]},
                        //         then:"$seller.fullname",
                        //         else:""
                        //     }
                        // }
                        provider_name:"$seller.fullname"
                    }},
                    {
                        $project:{
                            "seller":0
                        }
                    }
                
                    
        
                ]))?.length
        
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            
            //console.log("ff ", ff);
            //mongoose.set("debug",true);
            const products = await Product.aggregate([
                {$match:opquery},
            
                {$lookup:{
                    from:"sellers",
                    let:{"provider_id":{"$toObjectId":"$provider_id"}},
                    pipeline:[
                        {$match:{
                            $expr:{$eq:["$_id","$$provider_id"]}
                        }}, 
                    ],
                    as:"seller"
                }},
                {$unwind:"$seller"},
                {$addFields:{
                    // provider_name:{
                    //     $cond:{
                    //         if:{$gt:[{$size:"$seller"},0]},
                    //         then:"$seller.fullname",
                    //         else:""
                    //     }
                    // }
                    provider_name:"$seller.fullname"
                }},
                
                {$skip:skip},
                {$limit:limit},
                {
                    $project:{
                        "seller":0
                    }
                }
            
                

            ]);
            //console.log("opquery","after")
            const pages=Math.ceil(products.length/10)
            return res.send({
                status: true,
                message: "Succès",
                data: products,
                pages:pages
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: [],
                pages:0
            })
        }
    },
    getallDeletedproductsforadmin: async (req, res) => {
        try{
            //console.log("req.body ",req.body);
            const stockstatus=req.body.stockstatus;
            var is_active = req.body.is_active;
            const product_name=req.body.product_name;
            const catagory=req.body.catagory;
            const opquery={is_deleted:true};
            
            const inventoryquery={};
            // const violation=req.body.violation;
            // const instock= req.body.instock;
            const pageno= req.body.pageno;
        
            let vendor_id = req.body.vendor_id;
            //let ff = {};
            if(product_name)
            {
                opquery["title"] =  {'$regex' : '^'+product_name, '$options' : 'i'};;
            }
            if(vendor_id)
            {
                opquery["provider_id"] = vendor_id;
            }
            if(catagory){
                opquery["catagory"]=catagory
            }
            if(is_active )
            {
                if(is_active == "true")
                {
                    opquery["is_active"]=true;
                }else{
                    opquery["is_active"]=false;
                }
                
            }
            
            if(stockstatus){
                if(stockstatus=="0"){
                    inventoryquery["$and"]= [
                        {$expr:{$eq:["$product_id","$$product_id"]}},
                        {$expr:{$eq:["$quantity",0]}}
                    ]
                }else if(stockstatus=="1"){
                    inventoryquery["$and"]= [
                        {$expr:{$eq:["$product_id","$$product_id"]}},
                        {$expr:{$ne:["$quantity",0]}}
                    ]
                    // inventoryquery["$expr"]= {$ne:["$quantity",0]}
                    // inventoryquery["$and"]=[
                    //     {"$product_id":"$$product_id"},
                    //     {"quantity":{$ne:0}}
                    // ]
                    // inventoryquery["product_id"]="$product_id"
                }
            }
            
            const limit=10;
            const skip=limit*pageno;
            //console.log("opquery ", opquery);
            //mongoose.set("debug",true);
            if(pageno==null||pageno=="null"){
                const count=(await Product.aggregate([
                    {$match:opquery},
                
                    {$lookup:{
                        from:"sellers",
                        let:{"provider_id":{"$toObjectId":"$provider_id"}},
                        pipeline:[
                            {$match:{
                                $expr:{$eq:["$_id","$$provider_id"]}
                            }},
                        ],
                        as:"seller"
                    }},
                    {$unwind:"$seller"},
                    {$addFields:{
                        // provider_name:{
                        //     $cond:{
                        //         if:{$gt:[{$size:"$seller"},0]},
                        //         then:"$seller.fullname",
                        //         else:""
                        //     }
                        // }
                        provider_name:"$seller.fullname"
                    }},
                    {
                        $project:{
                            "seller":0
                        }
                    }
                
                    
        
                ]))?.length
        
                const pages=Math.ceil(count/limit)
                return res.send({
                    status:true,
                    pages:pages
                })
            }
            
            //console.log("ff ", ff);
            //mongoose.set("debug",true);
            const products = await Product.aggregate([
                {$match:opquery},
            
                {$lookup:{
                    from:"sellers",
                    let:{"provider_id":{"$toObjectId":"$provider_id"}},
                    pipeline:[
                        {$match:{
                            $expr:{$eq:["$_id","$$provider_id"]}
                        }}, 
                    ],
                    as:"seller"
                }},
                {$unwind:"$seller"},
                {$addFields:{
                    // provider_name:{
                    //     $cond:{
                    //         if:{$gt:[{$size:"$seller"},0]},
                    //         then:"$seller.fullname",
                    //         else:""
                    //     }
                    // }
                    provider_name:"$seller.fullname"
                }},
                
                {$skip:skip},
                {$limit:limit},
                {
                    $project:{
                        "seller":0
                    }
                }
            
                

            ]);
            //console.log("opquery","after")
            const pages=Math.ceil(products.length/10)
            return res.send({
                status: true,
                message: "Succès",
                data: products,
                pages:pages
            })
        }catch(e){
            return res.send({
                status: true,
                message: e.message,
                data: [],
                pages:0
            })
        }
    },
    getproductbyid: async (req, res) => {
        const id = req.param.id;
        const products = await Product.aggregate([
            {$match:{_id:mongoose.Types.ObjectId(id)}},
            {
                $lookup:{
                    from:"catagories",
                    localField:"catagory",
                    foreignField:"title",
                    as:"mcat"
                }
            },
           {
            $unwind:"$mcat"
           },
            {$lookup:{
                from:"catagories",
                let:{"mastercatagory":"$mcat._id"},
                pipeline:[
                    {$match:{$expr:{$eq:["$mastercatagory","$$mastercatagory"]}}},
                   
                    {
                        $graphLookup: {
                            "from": "catagories",
                            "startWith": "$_id",
                            "connectFromField": "_id",
                            "connectToField": "mastercatagory",
                            "as": "subs",
                            "maxDepth": 20,
                            "depthField": "level",
        
                        },
        
                    },
        
                    {
                        "$unwind": {
                            "path": "$subs",
                            "preserveNullAndEmptyArrays": true
                        }
                    },
        
        
                    {
                        $sort: {
                            "subs.level": -1
                        }
                    },
                    {
                        $group: {
                            _id: "$_id",
        
                            parent_id: {
                                $first: "$mastercatagory"
                            },
                            title: {
                                $first: "$title"
                            },
                            subs: {
                                $push: "$subs"
                            }
                        }
                    },
                    {
                        $addFields: {
                            subs: {
                                $reduce: {
                                    input: "$subs",
                                    initialValue: {
                                        currentLevel: -1,
                                        currentLevelChildren: [],
                                        previousLevelChildren: []
                                    },
                                    in: {
                                        $let: {
                                            vars: {
                                                prev: {
                                                    $cond: [
                                                        {
                                                            $eq: [
                                                                "$$value.currentLevel",
                                                                "$$this.level"
                                                            ]
                                                        },
                                                        "$$value.previousLevelChildren",
                                                        "$$value.currentLevelChildren"
                                                    ]
                                                },
                                                current: {
                                                    $cond: [
                                                        {
                                                            $eq: [
                                                                "$$value.currentLevel",
                                                                "$$this.level"
                                                            ]
                                                        },
                                                        "$$value.currentLevelChildren",
                                                        []
                                                    ]
                                                }
                                            },
                                            in: {
                                                currentLevel: "$$this.level",
                                                previousLevelChildren: "$$prev",
                                                currentLevelChildren: {
                                                    $concatArrays: [
                                                        "$$current",
                                                        [
                                                            {
                                                                $mergeObjects: [
                                                                    "$$this",
                                                                    {
                                                                        subs: {
                                                                            $filter: {
                                                                                input: "$$prev",
                                                                                as: "e",
                                                                                cond: {
                                                                                    $and: [
                                                                                        {
                                                                                            $eq: [
                                                                                                "$$e.mastercatagory",
                                                                                                "$$this._id"
                                                                                            ]
                                                                                        },
                                                                                        {
                                                                                            $eq: [
                                                                                                "$$e.is_parent", true
                                                                                            ]
                                                                                        }
                                                                                    ]
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                ]
                                                            }
                                                        ]
                                                    ]
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    },
                    {
                        $addFields: {
                            subs: "$subs.currentLevelChildren"
                        }
                    },
                    {
                        $match: {
                            mastercatagory: null
                        }
                    }
                ],
                as:"subcatagories"
            }},
            {
                $lookup:{
                    from:"ratingnreviews",
                    let:{"product_id":{"$toObjectId":"$_id"}},
                    pipeline:[
                        {$match:{$expr:{"product_id":"$$product_id"}}},
                        {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                        {
                            $lookup:{
                                from:"users",
                                localField:"userIDOBJ",
                                foreignField:"_id",
                                as:"userwhorated"
                            }
                        }
                    ],
                    as:"reviews"
                },
                
            },
            { $addFields: {
                                
                avgRating:{
                  $round:[ {$avg: "$reviews.rating"},0]
                }
            }},
            

        ]);
      
        return res.send({
            status: true,
            message: "fetched successfully",
            data: products
        })
    },
    getproductbyidtoedit: async (req, res) => {
        try{
            const id = req.params.id;
            //mongoose.set("debug",true);
            const products = await Product.findById(id);
            
            if(!products)
            {
                return res.send({
                    status: false,
                    message: "Numéro d'identification invalide",
                    data: []
                })
            }else{
                return res.send({
                    status: true,
                    message: "Obtenir un succès record pleinement",
                    data: products
                })
            }
            
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message,
                data: []
            })
        }
        
    },
    addcatagoryonline: async (req, res) => {
        let id = mongoose.Types.ObjectId(req.body.id);
        const query = {};
        let cat;
        cat = await catagorymodel.aggregate([
            // {$mathc:is_parent:true},
            { $match: { mastercatagory: null } },
            {
                $graphLookup: {
                    "from": "catagories",
                    "startWith": "$_id",
                    "connectFromField": "_id",
                    "connectToField": "mastercatagory",
                    "as": "subs",
                    "maxDepth": 20,
                    "depthField": "level",

                },

            },

            {
                "$unwind": {
                    "path": "$subs",
                    "preserveNullAndEmptyArrays": true
                }
            },


            {
                $sort: {
                    "subs.level": -1
                }
            },
            {
                $group: {
                    _id: "$_id",

                    parent_id: {
                        $first: "$mastercatagory"
                    },
                    title: {
                        $first: "$title"
                    },
                    subs: {
                        $push: "$subs"
                    }
                }
            },
            {
                $addFields: {
                    subs: {
                        $reduce: {
                            input: "$subs",
                            initialValue: {
                                currentLevel: -1,
                                currentLevelChildren: [],
                                previousLevelChildren: []
                            },
                            in: {
                                $let: {
                                    vars: {
                                        prev: {
                                            $cond: [
                                                {
                                                    $eq: [
                                                        "$$value.currentLevel",
                                                        "$$this.level"
                                                    ]
                                                },
                                                "$$value.previousLevelChildren",
                                                "$$value.currentLevelChildren"
                                            ]
                                        },
                                        current: {
                                            $cond: [
                                                {
                                                    $eq: [
                                                        "$$value.currentLevel",
                                                        "$$this.level"
                                                    ]
                                                },
                                                "$$value.currentLevelChildren",
                                                []
                                            ]
                                        }
                                    },
                                    in: {
                                        currentLevel: "$$this.level",
                                        previousLevelChildren: "$$prev",
                                        currentLevelChildren: {
                                            $concatArrays: [
                                                "$$current",
                                                [
                                                    {
                                                        $mergeObjects: [
                                                            "$$this",
                                                            {
                                                                subs: {
                                                                    $filter: {
                                                                        input: "$$prev",
                                                                        as: "e",
                                                                        cond: {
                                                                            $and: [
                                                                                {
                                                                                    $eq: [
                                                                                        "$$e.mastercatagory",
                                                                                        "$$this._id"
                                                                                    ]
                                                                                },
                                                                                {
                                                                                    $eq: [
                                                                                        "$$e.is_parent", true
                                                                                    ]
                                                                                }
                                                                            ]
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    }
                                                ]
                                            ]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    subs: "$subs.currentLevelChildren"
                }
            },
            {
                $match: {
                    mastercatagory: null
                }
            }
        ]);



        return res.render("admin-panel/addcatagoryonline.ejs", { cat, base_url, root_url });
    },
    savecatagoryonline: async (req, res) => {
        try{
            // console.log("req.body", req.body)
            var catagory = req.body.title;
            let is_parent = req.body.is_parent;
            let masterid=req.body.mastercatagory;
            if(catagory=="true"||catagory==true){
                is_parent=true
            }else if(catagory=="false"||catagory==false){
                is_parent=false
            }
            const id=req.body.id;
            if(!masterid){
                masterid=null
            }
            let price=parseInt(req.body.price);
            let image;

            if (req.files?.image) {
                if (req.files?.image) {
                    // console.log("req.files.photo",req.files.photo)
                    image = "media/" + req.files.image[0].filename;
                }
            }

            const lccatagory = catagory;
            catagory = catagory;
        
            if (id) {
            const datatoupdate={
                ...(catagory&&{title:catagory}),
                ...(image&&{image:image}),
                ...(price&&{price:price}),
                ...(is_parent&&{is_parent:is_parent}),
            }
            await catagorymodel.findByIdAndUpdate(id,datatoupdate).
            then((result)=>{
                return res.send({
                    status: true,
                    message: "Catagorie mise à jour avec succès",
                    errmessage: "Catagorie mise à jour avec succès",
                    data: null
                })
            })
            } else {
                var chck = await catagorymodel.findOne({title:lccatagory});
                // console.log("chck");
                // console.log(chck);
                // return false;
                chck = "";
                if(chck)
                {
                    return res.send({
                        status: false,
                        message: "Ce nom de catégorie existe déjà",
                        errmessage: "Ce nom de catégorie existe déjà",
                        data: null
                    })
                }else{
                    const newCatagory = new catagorymodel();
                    newCatagory.title = lccatagory;

                    newCatagory.mastercatagory = masterid;
                    newCatagory.price = price;
                    newCatagory.is_active = true;
                    newCatagory.image = image;
                    newCatagory.is_parent = is_parent;
                    newCatagory.save().then(() => {
                        return res.send({
                            status: true,
                            message: "Catégorie ajoutée avec succès",
                            errmessage: "Catégorie ajoutée avec succès",
                            data: null
                        })
                    }).catch((error)=>{
                        return res.send({
                            status: false,
                            message: error.message,
                            errmessage: error.message,
                            data: null
                        })
                    })
                }
                
            }
        }catch(error){
            return res.send({
                status: false,
                message: error.message,
                errmessage: error.message,
                data: null
            })
    }

    },
    editcatagoryonline: async (req, res) => {
        const id = req.params.id;

        let cats;
        let cat;
        cat = await catagorymodel.findById(id);

        cats = await catagorymodel.aggregate([
            // {$mathc:is_parent:true},
            { $match: { mastercatagory: null } },
            {
                $graphLookup: {
                    "from": "catagories",
                    "startWith": "$_id",
                    "connectFromField": "_id",
                    "connectToField": "mastercatagory",
                    "as": "subs",
                    "maxDepth": 20,
                    "depthField": "level",

                },

            },

            {
                "$unwind": {
                    "path": "$subs",
                    "preserveNullAndEmptyArrays": true
                }
            },
            { "$match": { "subs.is_parent": true } },
            {
                $sort: {
                    "subs.level": -1
                }
            },
            {
                $group: {
                    _id: "$_id",

                    parent_id: {
                        $first: "$mastercatagory"
                    },
                    title: {
                        $first: "$title"
                    },
                    subs: {
                        $push: "$subs"
                    }
                }
            },
            {
                $addFields: {
                    subs: {
                        $reduce: {
                            input: "$subs",
                            initialValue: {
                                currentLevel: -1,
                                currentLevelChildren: [],
                                previousLevelChildren: []
                            },
                            in: {
                                $let: {
                                    vars: {
                                        prev: {
                                            $cond: [
                                                {
                                                    $eq: [
                                                        "$$value.currentLevel",
                                                        "$$this.level"
                                                    ]
                                                },
                                                "$$value.previousLevelChildren",
                                                "$$value.currentLevelChildren"
                                            ]
                                        },
                                        current: {
                                            $cond: [
                                                {
                                                    $eq: [
                                                        "$$value.currentLevel",
                                                        "$$this.level"
                                                    ]
                                                },
                                                "$$value.currentLevelChildren",
                                                []
                                            ]
                                        }
                                    },
                                    in: {
                                        currentLevel: "$$this.level",
                                        previousLevelChildren: "$$prev",
                                        currentLevelChildren: {
                                            $concatArrays: [
                                                "$$current",
                                                [
                                                    {
                                                        $mergeObjects: [
                                                            "$$this",
                                                            {
                                                                subs: {
                                                                    $filter: {
                                                                        input: "$$prev",
                                                                        as: "e",
                                                                        cond: {
                                                                            $and: [{
                                                                                $eq: [
                                                                                    "$$e.mastercatagory",
                                                                                    "$$this._id"
                                                                                ]
                                                                            },
                                                                            {
                                                                                $eq: [
                                                                                    "$$e.is_parent", true
                                                                                ]
                                                                            }
                                                                            ]
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        ]
                                                    }
                                                ]
                                            ]
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },
            {
                $addFields: {
                    subs: "$subs.currentLevelChildren"
                }
            },
            {
                $match: {
                    mastercatagory: null
                }
            }
        ]);


        return res.render("admin-panel/editcatagoryonline.ejs", { base_url, cats, cat, base_url, root_url });
    },
    updatecatagoryonline: async (req, res) => {
        const id = req.body.id;
        const catagory = req.body.catagory;
        const is_parent = req.body.is_parent;
        const mastercatagory = req.body.mastercatagory;
        let price;
        if (is_parent == "true") {
            price = null;
        } else {
            price = parseInt(req.body.price);
        }
        let masterid;
        let successesor;
        let catagoryID
        if (req.body.mastercatagory) {
            //  const parentcatagory=await catagorymodel.findById(id);
            const catagory = await catagorymodel.findOne({ title: req.body.mastercatagory });
            catagoryID = mongoose.Types.ObjectId(id);

            successesor = await catagorymodel.findOne({ mastercatagory: catagoryID });
            catagoryID = successesor?._id;
            masterid = catagory?._id;
        } else {
            masterid = null;
        }

        let image
        if (req.files?.image) {
            if (req.files?.image) {
                // console.log("req.files.photo",req.files.photo)
                image = "userprofile/" + req.files.image[0].filename;
            }
        }
        const lccatagory = catagory.toLowerCase();
        const catexists = await catagorymodel.findById(id);
        if (catexists) {
            const data = {
                ...(lccatagory && { title: lccatagory }),
                ...(image && { image: image }),
                ...(price && { price: price }),
                ...(is_parent && { is_parent: is_parent }),
                ...(mastercatagory && { mastercatagory: masterid }),
            }
            console.log("catagory====>", catagory, "<=============================================");
            console.log("catagory.mastercatagory====>", catagory.mastercatagory, "<=============================================");
            console.log("successesor====>", successesor, "<=============================================");
            console.log("id====>", id, "<=============================================");
            if (catagory.mastercatagory == null) {

                await catagorymodel.findByIdAndUpdate(catagoryID, { mastercatagory: null });

            } else {

            }

            await catagorymodel.findByIdAndUpdate(catexists._id, data).then((result) => {
                return res.send({
                    status: true,
                    message: "Catagorie mise à jour avec succès"
                })

            })
        } else {
            return res.send({
                status: false,
                message: "La catégorie n'existe pas",
                errmessage: "La catégorie n'existe pas",
                data: null
            })
        }

    },
    deletecatagoryonline: async (req, res) => {
        const id = mongoose.Types.ObjectId(req.params.id);
        console.log("id", id)

        const allcatagories = await catagorymodel.aggregate([
            { $match: { _id: id } },
            {
                $graphLookup: {
                    "from": "catagories",
                    "startWith": "$_id",
                    "connectFromField": "_id",
                    "connectToField": "mastercatagory",
                    "as": "subs",
                    "maxDepth": 20,
                    "depthField": "level",

                },

            }
        ]);
        const deleteids = allcatagories[0].subs.map(e => e._id);
        deleteids.push(allcatagories[0]._id);
        await catagorymodel.deleteMany({ _id: { $in: deleteids } }).then(() => {
            return res.send({
                status: true,
                message: "Catagorie supprimée avec succès"
            })
        })
        // return res.send({
        //   status:true,
        //   data:allcatagories
        // })
        // const id=req.params.id;
        // console.log("id",id)
        // await catagorymodel.findByIdAndDelete(id).then((result)=>{
        //   return res.send({
        //     status:true,
        //     message:"Catagorie supprimée avec succès"
        //   })
        // })
    },
    getOnlyFeatureCategory:async(req,res)=>{
        try{
            let result = await catagorymodel.find({is_featured:true,is_active:true});

            return res.send({
                status:true,
                message:"Success",
                data:result
            })

        }catch(e)
        {
            return res.send({
                status:false,
                message:e.message
            })
        }
    },
    getOnlyParentCategory:async(req,res)=>{
        try{
            await catagorymodel.aggregate([
                {
                    $match:{ mastercatagory:null }
                },{
                    $match:{ is_active:true }
                },
                {
                    $lookup:{
                        from:"catagories",
                        localField:"_id",
                        foreignField:"mastercatagory",
                        as:"subCategory"
                    }
                },
                { 
                    $addFields: {
                        subCatCount: {$size: "$subCategory"}
                    }  
                },
                {
                    $project:{
                        "subCategory":0
                    }
                } 

                
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                })
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    getOnlyChildCategory:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var id = mongoose.Types.ObjectId(req.params.id);
            var cat_record = await catagorymodel.findOne({_id:id});
            await catagorymodel.aggregate([
                {
                    $match:{mastercatagory:id}
                },{
                    $match:{is_active:true}
                },
                {
                    $lookup:{
                        from:"categories",
                        localField:"_id",
                        foreignField:"mastercatagory",
                        as:"subCategory"
                    }
                },
                {
                    $addFields:{
                        subCatCount:{$size:"$subCategory"}
                    }
                },
                {
                    $project:{
                        "subCategory":0
                    }
                }
            ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result,
                    cat_record:cat_record
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                })
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    getAllParentCategory:async(req,res)=>{
        try{
            var id = mongoose.Types.ObjectId(req.params.id);
            
            await catagorymodel.aggregate([
                {
                    $match:{_id:id}
                },{
                    $match:{is_active:true}
                },
                {
                  $lookup: {
                    from: 'categories', // <- schema from which data will be selected (same schema)
                    let: { id: '$mastercatagory' }, // <- a variable that will refer to the $parentId field in the current schema
                    pipeline: [
                      { $match: { $expr: { $eq: ['$mastercatagory', "$$id"] }, mastercatagory: { $ne: null } } }
                    ],
                    as: 'parent'
                  }
                }
              ]).then((result)=>{
                return res.send({
                    status:true,
                    message:"Succès",
                    data:result
                });
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message,
                    data:[]
                });
            });
        }catch(error){
            return res.send({
                status:false,
                message:error.message,
                data:[]
            });
        }
    },
    getallcatagories: async (req, res) => {
        const search = req.query.search;

        let id = mongoose.Types.ObjectId(req.query.id);
        const query = {};
        if (search) {
            query['$or'] = [
                { catagory: search },

            ]
        }


        let cat;
        console.log("in else ", "id", id)
        if (req.query.id) {
            cat = await catagorymodel.aggregate([
                { $match: { "_id": mongoose.Types.ObjectId(req.query.id) } },
                { $addFields: { sid: { $toString: "$_id" } } },
                { $lookup: { from: "catagories", localField: "_id", foreignField: "mastercatagory", as: "subcatagories" } },
            ])
        } else {

            //    cat=await catagorymodel.aggregate( [
            //     {$match: {"$or":[
            //       {"mastercatagory":{$eq:""}},
            //       {"mastercatagory":{ $exists: false}}
            //     ]}},
            //     {$addFields:{sid:{$toString:"$_id"}}},
            //     {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
            //  ] )

            cat = await catagorymodel.aggregate([
                // {$mathc:is_parent:true},
                { $match: { mastercatagory: null } },
                {
                    $graphLookup: {
                        "from": "catagories",
                        "startWith": "$_id",
                        "connectFromField": "_id",
                        "connectToField": "mastercatagory",
                        "as": "children",
                        "maxDepth": 10,
                        "depthField": "level",

                    },

                },
                {
                    $unwind: "$children"
                },
                {
                    $sort: {
                        "children.level": -1
                    }
                },
                {
                    $group: {
                        _id: "$_id",
                        mastercatagory: {
                            $first: "$mastercatagory"
                        },
                        title: {
                            $first: "$title",
                            
                        },
                        price:{  $first: "$price"},
                        image:{  $first: "$image"},
                        children: {
                            $push: "$children"
                        }
                    }
                },
                {
                    $addFields: {
                        children: {
                            $reduce: {
                                input: "$children",
                                initialValue: {
                                    currentLevel: -1,
                                    currentLevelChildren: [],
                                    previousLevelChildren: []
                                },
                                in: {
                                    $let: {
                                        vars: {
                                            prev: {
                                                $cond: [
                                                    {
                                                        $eq: [
                                                            "$$value.currentLevel",
                                                            "$$this.level"
                                                        ]
                                                    },
                                                    "$$value.previousLevelChildren",
                                                    "$$value.currentLevelChildren"
                                                ]
                                            },
                                            current: {
                                                $cond: [
                                                    {
                                                        $eq: [
                                                            "$$value.currentLevel",
                                                            "$$this.level"
                                                        ]
                                                    },
                                                    "$$value.currentLevelChildren",
                                                    []
                                                ]
                                            }
                                        },
                                        in: {
                                            currentLevel: "$$this.level",
                                            previousLevelChildren: "$$prev",
                                            currentLevelChildren: {
                                                $concatArrays: [
                                                    "$$current",
                                                    [
                                                        {
                                                            $mergeObjects: [
                                                                "$$this",
                                                                {
                                                                    children: {
                                                                        $filter: {
                                                                            input: "$$prev",
                                                                            as: "e",
                                                                            cond: {
                                                                                $eq: [
                                                                                    "$$e.mastercatagory",
                                                                                    "$$this._id"
                                                                                ]
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                ]
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                },
                {
                    $addFields: {
                        children: "$children.currentLevelChildren"
                    }
                },
                {
                    $match: {
                        mastercatagory: null
                    }
                }
            ])
        }
        // const cat=await catagorymodel.find(query);
        return res.send({
            status: true,
            data: cat
        })
        // return res.render("admin-panel/managecatagory.ejs", {cat, base_url, root_url });
    },
    getallcatagoriesFilter: async (req, res) => {
        
        cat = await catagorymodel.aggregate([
           {$match:{_id:{$ne:"xcxc"}}},
           {$match:{is_active:true}},
           {
            $project:{
                "_id":1,
                "title":1
            }
           }
        ])

        return res.send({
            status: true,
            data: cat
        })
        // return res.render("admin-panel/managecatagory.ejs", {cat, base_url, root_url });
    },
    getallcatagoriesflat: async (req, res) => {
        const search = req.query.search;

        let id = mongoose.Types.ObjectId(req.query.id);
        const query = {};
        if (search) {
            query['$or'] = [
                { catagory: search },

            ]
        }


        let cat;
        console.log("in else ", "id", id)
        if (req.query.id) {
            cat = await catagorymodel.aggregate([
                { $match: { "_id": mongoose.Types.ObjectId(req.query.id) } },
                { $match: { is_active:true } },
                { $addFields: { sid: { $toString: "$_id" } } },
                { $lookup: { from: "catagories", localField: "_id", foreignField: "mastercatagory", as: "subcatagories" } },
            ])
        } else {

            //    cat=await catagorymodel.aggregate( [
            //     {$match: {"$or":[
            //       {"mastercatagory":{$eq:""}},
            //       {"mastercatagory":{ $exists: false}}
            //     ]}},
            //     {$addFields:{sid:{$toString:"$_id"}}},
            //     {$lookup:{from:"catagories",localField:"_id",foreignField:"mastercatagory",as:"subcatagories"}},
            //  ] )

            cat = await catagorymodel.aggregate([
                {$match:{_id:{$ne:"xcxc"}}},
                // { $match: { mastercatagory: null } },
                // {
                //     $graphLookup: {
                //         "from": "catagories",
                //         "startWith": "$_id",
                //         "connectFromField": "_id",
                //         "connectToField": "mastercatagory",
                //         "as": "children",
                //         "maxDepth": 10,
                //         "depthField": "level",

                //     },

                // },
               
            ])
        }
        // const cat=await catagorymodel.find(query);
        return res.send({
            status: true,
            data: cat
        })
        // return res.render("admin-panel/managecatagory.ejs", {cat, base_url, root_url });
    },
    getAllCatagoriesCount: async (req, res) => 
    {
        try{
            let cat_count = await catagorymodel.count({});
            let limit = 10;
            const pages = Math.ceil(cat_count/limit);
            return res.send({
                status: true,
                message:"success",
                data:pages
            })
        }catch(error)
        {
            return res.send({
                status: false,
                message:error.message
            })
        }
    },
    getAllCatagoriesPagi: async (req, res) => 
    {
        try{
            
            const pageno=req.body.pageno;
            const limit=10;
            const skip=limit*pageno;
            let cat_count = await catagorymodel.find({}).skip(skip).limit(limit);
            return res.send({
                status: true,
                message:"success",
                data:cat_count
            })
        }catch(error)
        {
            return res.send({
                status: false,
                message:error.message
            })
        }
    },
    makeFeatureApi: async (req, res) => 
    {
        try{
            console.log(req.body);
            let is_featured = req.body.status;
            let id = req.body.id;

            let count_check = await catagorymodel.count({is_featured:true});

            if(count_check >= 4)
            {
                console.log("is_featured ",is_featured);
                if(is_featured == false)
                {
                    await catagorymodel.updateOne({_id:req.body.id},{is_featured:req.body.status}).then((result)=>{
                        return res.send({
                            status: true,
                            message:"Catégorie en vedette avec succès"
                        })
                    }).catch((error)=>{
                        return res.send({
                            status: false,
                            message:error.message
                        })
                    })
                
                }else{
                    return res.send({
                        status: false,
                        message:"Maximum quatre catégories, nous pouvons faire en sorte que chaque catégorie soit mise en évidence ou non. "
                    })
                }
                
            }else{
                await catagorymodel.updateOne({_id:req.body.id},{is_featured:req.body.status}).then((result)=>{
                    return res.send({
                        status: true,
                        message:"Catégorie en vedette avec succès"
                    })
                }).catch((error)=>{
                    return res.send({
                        status: false,
                        message:error.message
                    })
                })
                
            }
            
        }catch(error)
        {
            return res.send({
                status: false,
                message:error.message
            })
        }
    },
    getSingleCatEditTime:async(req,res)=>{
        try{
            //
            await catagorymodel.findById(req.params.id).then((result)=>{
                return res.send({
                    status:true,
                    message:"Success",
                    data:result
                })
            }).catch((error)=>{
                return res.send({
                    status:false,
                    message:error.message
                })
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
    },
    
    makeProductasactive:async(req,res)=>{
        const id=req.params.id;
        await Product.makeActive(id).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
    },
    makeProductasInactive:async(req,res)=>{
        const id=req.params.id;
        await Product.makeInActive(id).then((result)=>{
            return res.send({
                status:true,
                message:"updated"
            })
        })
    },
    ProductfilterwithAttributes:async(req,res)=>{
        const attribute=req.body.attribute;
        const products=await Product.filterwithAttributes(attribute);
        console.log("products",products)
        return res.send({
            status:true,
            data:products
        })
    },

    //material catagory arch
    savematcatagory:async(req,res)=>{
        let image;
        if(req.files.image.length){
            image=process.env.PHOTOS_PATH_PERFIX+"/"+req.files.image[0].filename;
        }
        console.log("req.files.image",req.files.image)
        await MatCatagory.addcatagory({
            name:req.body.name,
            path:req.body.path,
            image:image
        }).then(()=>{
            return res.send({
                status:true,
                message:"catagory saved successfully"
            })
        })
    },
    updatematcatagory:async(req,res)=>{
         let image;
        if(req.files.image.length){
            image=process.env.PHOTOS_PATH_PERFIX+"/"+req.files.image[0].filename;
        }
        await MatCatagory.updatecatagory({
            name:req.body.name,
            path:req.body.path,
            image:image,
            id:req.body.id
        }).then(()=>{
            return res.send({
                status:true,
                message:"catagory updated successfully"
            })
        })
    },
    deletematcatagory:async(req,res)=>{
        await MatCatagory.deletecatagory({
            id:req.params.id
        }).then(()=>{
            return res.send({
                status:true,
                message:"catagory updated successfully"
            })
        })
    },
    getallmatcatagories:async(req,res)=>{
        await MatCatagory
        .getallcatagories()
        .then((result)=>{
            return res.send({
                status:true,
                message:"fetched successfully",
                data:result
            })
        })
    },
    getchildcatagories:async(req,res)=>{
        let master="";
        master=req.body.master;
        await MatCatagory
        .getchildcatagories(master)
        .then((result)=>{
            return res.send({
                status:true,
                message:"fetched successfully",
                data:result
            })
        })
    },
    getparentcatagoriesonly:async(req,res)=>{
        await MatCatagory
        .getparentcatagoriesonly()
        .then((result)=>{
            return res.send({
                status:true,
                message:"fetched successfully",
                data:result
            })
        })
    },

    //material catagory arch ends here
    markProductasViolated:async(req,res)=>{
        const product_id=req.body.product_id;
        const user_id =req.body.user_id;
        const reason=req.body.reason;
        const comment=req.body.comment;
        console.log("req.body",req.body);
       
        const product= await Product.findById(product_id);
        let newviolationnumber=product.reported.violations+1;
        const data={
            "reported.violations":newviolationnumber,
             $push:{"reported.users":{user_id,reason:reason,comment:comment}}
        }
        console.log("data",data)
        await Product.findByIdAndUpdate(product_id,data)
        .then((result)=>{
            return res.send({
                status:true,
                message:"violation marked successfully"
            })
        }).catch((err)=>{
            return res.send({
                status:false,
                message:err.message
            })
        })
       
        
    },

    get2level_category:async(req,res)=>{
        try{

            const cat_record = await catagorymodel.aggregate([
                {
                    $match:{
                        "mastercatagory":null
                    }
                },
                {
                    $graphLookup: {
                        "from": "catagories",
                        "startWith": "$_id",
                        "connectFromField": "_id",
                        "connectToField": "mastercatagory",
                        "as": "children",
                        "maxDepth": 0,
                        "depthField": "level",

                    },

                }
                
            ]);
            return res.send({
                status:true,
                data:cat_record,
                message:"Success"
            });
        }catch(error)
        {
            return res.send({
                status:false,
                message:error.message
            })
        }
        
    },

//slaes info or variation methods
    addVariations: async (req, res) => {
        let {
            variation,
            variation_option,
            product_id,
            type
    
        } = req.body;
        let images = [];
    
        if (req.files?.images) {
            req.files.images?.map((image) => {
                let filename = process.env.PHOTOS_PATH_PERFIX + "/" + image.filename;
                images.push(filename);
            })
        }
    
        const product = await Product.findById(product_id);
        const isvariationthere = product?.sales_info.filter(e => e.variation == variation)[0];
        let newoption;
        if (isvariationthere) {
            const query = { _id: product_id };
            const toupdate = {
                option: variation_option,
                images: images
            }
            const updateDocument1 = {
                ...(variation_option && { $push: { "sales_info.$[item].options": toupdate } })
            };
            const options1 = {
                arrayFilters: [
                    {
                        "item._id": mongoose.Types.ObjectId(isvariationthere._id)
    
                    }
                ],
            };
    
            await Product.updateOne(query, updateDocument1, options1)
                .then((result) => {
                    return res.send({
                        status: true
                    })
                })
        } else {
            const toupdate = {
                variation:variation,
                type:type,
                options:{
                   
                    option: variation_option,
                    images: images
                }
               
            }
            const query = { _id: product_id };
            const updateDocument1 = {
                ...(variation_option && { $push: { "sales_info": toupdate } })
            };
    
            await Product.updateOne(query, updateDocument1)
                .then((result) => {
                    return res.send({
                        status: true
                    })
                })
    
        }
    },
    getVariations:async(req,res)=>{
        const product_id=req.params.id;
        console.log("id",product_id)
        await Product.findOne({_id:product_id})
                .then((result) => {
                    console.log("result",result)
                    return res.send({
                        status: true,
                        data:result?.sales_info
                    })
                })
    },
    updateVariations:async(req,res)=>{
        let {
            variation,
            variation_option,
            product_id,
            type
    
        } = req.body;
        let images = [];
    
        if (req.files?.images) {
            req.files.images?.map((image) => {
                let filename = process.env.PHOTOS_PATH_PERFIX + "/" + image.filename;
                images.push(filename);
            })
        }
    
        const product = await Product.findById(product_id);
        const isvariationthere = product?.sales_info.filter(e => e.variation == variation)[0];
        let newoption;
        if (isvariationthere) {
            const query = { _id: product_id };
            const toupdate = {
                option: variation_option,
                images: images
            }
            const updateDocument1 = {
                ...(variation_option && { $push: { "sales_info.$[item].options": toupdate } })
            };
            const options1 = {
                arrayFilters: [
                    {
                        "item._id": mongoose.Types.ObjectId(isvariationthere._id)
    
                    }
                ],
            };
    
            await Product.updateOne(query, updateDocument1, options1)
                .then((result) => {
                    return res.send({
                        status: true
                    })
                })
        } else {
            const toupdate = {
                variation:variation,
                type:type,
                options:{
                   
                    option: variation_option,
                    images: images
                }
               
            }
            const query = { _id: product_id };
            const updateDocument1 = {
                ...(variation_option && { $push: { "sales_info": toupdate } })
            };
    
            await Product.updateOne(query, updateDocument1)
                .then((result) => {
                    return res.send({
                        status: true
                    })
                })
    
        }
    },
    deleteVariations:async(req,res)=>{
        let {
            name,
            product_id
    
        } = req.body;
        const product = await Product.findById(product_id);
        const newsalesinfo=product?.sales_info?.filter((e)=>e.variation!=name)
        product.sales_info=newsalesinfo
        product.save().then((result)=>{
            return res.send({
                status:true
            })
        })
       
    },
    deleteVariationoption:async(req,res)=>{
        let {
            variation,
            option,
            product_id
    
        } = req.body;
        const product = await Product.findById(product_id);
        const newsalesinfo=product?.sales_info?.filter((e)=>e.variation==variation)[0];
        console.log("newsalesinfo",newsalesinfo)
        const newsalesoption=newsalesinfo?.options?.filter(e=>e.option!=option);
        console.log("newsalesoption",newsalesoption)

        const query = { _id: product_id };
        const updateDocument1 = {
            "sales_info.$[item].options": newsalesoption
        };
       
        const options1 = {
            arrayFilters: [
                {
                    "item.variation":variation

                }
            ],
        };

        await Product.updateOne(query, updateDocument1, options1)
            .then((result) => {
                return res.send({
                    status: true
                })
            })
       
    },





//slaes infor combination
    addVariationsPrice: async (req, res) => {
        let {
            variations,
            variation_options,
            product_id,
            stock,
            sku,
            price
        }=req.body;
        const id=req.body.id;
        console.log("req.body",req.body)
        const product=await Product.findById(product_id);
        const sales_info=product?.sales_info;
        let images;
        console.log("sales_info",sales_info)
        const imagestosavetovariationprice=sales_info?.map((e)=>{
            let options=e.options;
            options?.map((f)=>{
                if(f.option==variation_options[0]){
                    images=f.images
                }
            })
        })
        try{
            variations=JSON.parse(variations)
        }catch(e){
            variations=variations
        }
        try{
            variation_options=JSON.parse(variation_options)
        }catch(e){
            variation_options=variation_options
        }
        const isthissetthere=await VariationsPrice.findOne({
            variations:variations,
            variation_options:variation_options,
            product_id:product_id
        })
       if(isthissetthere){
        await VariationsPrice.findByIdAndUpdate(isthissetthere._id,{
            ...(price&&{price:price}),
           
          
           
            ...(stock&&{stock:stock}),
            ...(sku&&{sku:sku}),
            ...(images&&{images:images})
        }).then((result)=>{
            return res.send({
                status:true,
                message:"updated there"
            })
        })
      
       }else{
        await VariationsPrice.create({
            ...(price&&{price:price}),
            ...(variation_options?.length&&{variation_options:variation_options}),
            ...(product_id&&{product_id:product_id}),
            ...(variations?.length&&{variations:variations}),
            ...(stock&&{stock:stock}),
            ...(sku&&{sku:sku}),
            ...(images&&{images:images})
    }).then((result)=>{
        // await Product.findByIdAndUpdate(product_id,{
        //     $push:{"sales_info":{
        //         ...(price&&{price:price}),
        //         ...(variation_options?.length&&{variation_options:variation_options}),
        //         ...(product_id&&{product_id:product_id}),
        //         ...(variations?.length&&{variations:variations}),
        //         ...(stock&&{stock:stock}),
        //         ...(sku&&{sku:sku})
        // }}
        // })
        return res.send({
            status:true
        })
    })
       }
       
    },
    getVariationsPrice:async(req,res)=>{
        const id=req.params.id;
        await VariationsPrice.find({product_id:id}).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    updateVariationsPrice:async(req,res)=>{
        const variation=req.body;
        const id=req.body.id;
        await VariationsPrice.findByIdAndUpdate(id,{
                ...(variation.price&&{price:variation.price}),
                ...(variation.variation_option&&{stock:variation.variation_option}),
                ...(variation.product_id&&{product_id:variation.product_id}),
                ...(variation.variations&&{variations:variation.variations}),
                ...(variation.variation_options&&{variation_options:variation.variation_options})
        }).then((result)=>{
            return res.send({
                status:true
            })
        })
        
    },
    deleteVariationsPrice:async(req,res)=>{
        const product_id=req.params.id;
        await VariationsPrice.findByIdAndDelete(product_id).then((result)=>{
            return res.send({
                status:true
            })
        })
    },
    getPriceofVariations:async(req,res)=>{
        const variations=req.body.variations;
        const product_id=req.body.product_id;
        const variationoptions=req.body.variation_options;
        console.log("req.body",req.body,"variations",variations,"variationoptions",variationoptions)
        await VariationsPrice.aggregate([
            {
                $match:{
                   product_id:product_id,
                   $and:[
                    {
                        variations:{$all:variations}
                    },
                    {
                        variation_options:{$all:variationoptions}
                    },
                    ]
                }
            }
        ])
        .then((result)=>{
                return res.send({
                    status:true,
                    data:result
                })
            })
    },

    getallproductjustforfilter:async(req,res)=>{
        const allproducts=await Product.aggregate([
            {$match:{is_deleted:false}},
            {$project:{
                _id:1,
                title:1,
                catagory:1
            }}
        ])
        return res.send({
            status:true,
            data:allproducts
        })
    },

//page view methods
    addPageView:async(req,res)=>{
        const provider_id=req.body.provider_id;
        const pageviews=await pageViewsModel.findOne({provider_id:provider_id});
        if(pageviews){
            pageviews.count=pageviews.count+1;
            pageviews.save()
            .then((result)=>{
                return res.send({
                    status:true
                })
            })
        }else{
            const newpageview=await pageViewsModel.create({
                provider_id:provider_id,
                count:1
            }).then((result)=>{
                return res.send({
                    status:true
                })
            })
        }
    },
    getPageViews:async(req,res)=>{
        const provider_id=req.params.id;
        await pageViewsModel.findOne({provider_id:provider_id}).then((result)=>{
            return res.send({
                status:true,
                data:result
            })
        })
    },
    updatestockofproduct:async(req,res)=>{
        // const {
        //     sku,
        //     qty,
        //     product_id
        // }=req.body;
        // const product=await Product.findOne()
    },
     getrecommendedproducts: async (req, res) => {
        try{
            const {
                catagory,
                id
            }=req.body;
            if(!id || !catagory)
            {
                return res.send({
                    status: false,
                    message: "L'identifiant et la catégorie sont tous deux requis",
                    data: []
                })
            }
            const products = await Product.aggregate([
                {$match:{_id:{$ne:mongoose.Types.ObjectId(id)},catagory:catagory}},
                {
                    $lookup:{
                        from:"catagories",
                        localField:"catagory",
                        foreignField:"title",
                        as:"mcat"
                    }
                },
                {
                    $addFields:{
                        "mcatone":{$arrayElemAt: ["$mcat", 0]}
                    }
                },
                {$lookup:{
                    from:"catagories",
                    let:{"mastercatagory":"$mcatone._id"},
                    pipeline:[
                        {$match:{$expr:{$eq:["$mastercatagory","$$mastercatagory"]}}},
                    
                        
                    ],
                    as:"subcatagories"
                }},
                {
                    $lookup:{
                        from:"ratingnreviews",
                        let:{"prod_id":{"$toString":"$_id"}},
                        //let:{productidstring:{"$toString":"$_id"}},
                        pipeline:[
                            // {$match:{
                            //     $expr:{
                            //         $eq:["$product_id","$$productidstring"]
                                    
                            //     }
                            // }}
                            {
                                $match:{
                                    $expr:{
                                        $eq:["$product_id","$$prod_id"]
                                    }
                                }
                            }
                            //{$match:{$expr:{"product_id":"$$product_id"}}},
                            // {$addFields:{"userIDOBJ":{"$toObjectId":"$user_id"}}},
                            // {
                            //     $lookup:{
                            //         from:"users",
                            //         localField:"userIDOBJ",
                            //         foreignField:"_id",
                            //         as:"userwhorated"
                            //     }
                            // }
                        ],
                        as:"reviews"
                    },
                    
                },
                { $addFields: {
                                    
                    avgRating:{
                        $avg: "$reviews.rating"
                    },
                    provideridObj:{
                        "$toObjectId":"$provider_id"
                    }
                }},
                {
                    $lookup:{
                        from:"sellers",
                        localField:"provideridObj",
                        foreignField:"_id",
                        as:"seller"
                    }
                },
                {
                    $lookup:{
                        from:"subscriptions",
                        let:{provider_id:"$provider_id"},
                        pipeline:[
                            {$match:{
                                $expr:{
                                    $and:[
                                        {
                                            $eq:["$provider_id","$$provider_id"]
                                            
                                        },{
                                            $lte:[new Date(),"$end_date"]
                                        }
                                    ]
                                }
                            }}
                        ],
                        as:"subs"
                    }
                },
                {
                    $lookup:{
                        from:"variationprices",
                        let:{productidstring:{"$toString":"$_id"}},
                        pipeline:[
                            {$match:{
                                $expr:{
                                    $eq:["$product_id","$$productidstring"]
                                    // $and:[
                                    //     {
                                    //         $eq:["$provider_id","$$provider_id"]
                                            
                                    //     },{
                                    //         $lte:[new Date(),"$end_date"]
                                    //     }
                                    // ]
                                }
                            }}
                        ],
                        as:"variations"
                    }
                },
                {
                    $addFields:{
                        pricerangeMin:{
                            $cond:{
                                if:{$gt:[{$size:"$variations"},0]},
                                then:{
                                    $min:"$variations.price"
                            
                            
                            },else:"$price"
                        }},
                        pricerangeMax:{
                            $cond:{
                                if:{$gt:[{$size:"$variations"},0]},
                                then:{
                                    $max:"$variations.price"
                            
                            
                            },else:"$price"
                        }},
                    
                    }
                },
            

            ]);
            // console.log("products",products)
            // const products1 = products?.filter((pro)=>pro?.subs?.length!=0)
            const products1 = products
            return res.send({
                status: true,
                message: "fetched successfully",
                data: products1
            })
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message,
                data: []
            })
        }
        
    },
    getAutoSuggestionProductList:async(req,res)=>{
        try{
            //mongoose.set("debug",true);
            var search = req.body.search;
            // const products = await Product.aggregate([
            //     {
            //         $match:{
            //             "title":"/"+search+"/"
            //         }
            //     }
            // ]);
            const products = await Product.find(
                {"title": { $regex: '.*'+search +'.*' },
                is_deleted:false 
            },{title:1,catagory:1});
            console.log(products);
            if(products.length > 0)
            {
                return res.send({
                    status: true,
                    message: "Succès",
                    data: products
                })
            }else{
                return res.send({
                    status: false,
                    message: "Pas de données",
                    data: products
                })
            }
        }catch(error)
        {
            return res.send({
                status: false,
                message: error.message,
                data: []
            })
        }
    },
   

    updateAdViewCount:async(req,res)=>{
        try{
          var {user_id,ad_id} = req.body;
          if(user_id)
          {
            var countRecord = await Product.count({_id:ad_id,provider_id:user_id});
          }else{
            var countRecord = 0;
          }
          
          //console.log(countRecord);
          if(countRecord == 0)
          {
            await Product.updateOne({_id:ad_id},{ $inc:{"view_count":1} }).then((result)=>{
              var return_response = {"error":false,errorMessage:"Succès"};
              return res.status(200).send(return_response);
            }).catch((error)=>{
              var return_response = {"error":true,errorMessage:error.message};
              return res.status(200).send(return_response);
            })
          }
        }catch(error){
          var return_response = {"error":true,errorMessage:error.message};
          return res.status(200).send(return_response);
        }
    },
    updateCatStatus:async(req,res)=>{
        try{
            //console.log(req.body);
            var {id,status} = req.body;
            await catagorymodel.updateOne({_id:id},{is_active:status}).then((result)=>{
                return res.send({
                    status:true,
                    message:"Enregistrement complet des succès actualisés"
                })
            }).catch((e)=>{
                return res.send({
                    status:false,
                    message:e.message
                })
            });
        }catch(e)
        {
            return res.send({
                status:false,
                message:e.message
            })
        }
    }
}