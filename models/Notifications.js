const mongoose = require('mongoose');
const NotificationSchema = new mongoose.Schema({
    to_id: {type:String},
    user_type: {type:String,default:'user'}, // user , seller , admin
    title: {type:String,default:''},
    message: {type:String,default:''},
    status: {type:String,default:''},
    notification_type: {type:String,default:''},
    date: {type:Date,default: new Date() },
    createdd_at: {type:Date,default: new Date() },
    from_email: {type:String},
})
module.exports = mongoose.model('Notification', NotificationSchema);

// {
//     _id: ObjectId("6402e33c4a5459d630d09194"),
//     to_id: '642d5d64f5a88587ffae0ab5',
//     message: 'hello test message',
//     status: 'read',
//     notification_type: 'push',
//     date: ISODate("2023-03-04T06:20:44.679Z"),
//     __v: 0
//   }
//   {
//     _id: ObjectId("640991814aa1ba43b5bfdff6"),
//     status: 'read',
//     notification_type: 'Message',
//     message: 'two message',
//     __v: 0
//   }
