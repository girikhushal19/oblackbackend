const mongoose=require("mongoose");
const EventRatingSchema=new mongoose.Schema({
    user_id:{type:String},
    org_id:{type:String},
    event_id:{type:String},
  
    rating:{type:Number},
    review:{type:String},
    media:{type:Array}
    
},{
    timestamps:true
});
module.exports=mongoose.model("EventRating",EventRatingSchema);