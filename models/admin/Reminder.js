const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const ReminderSchema=new Schema({
    user_id:{type:String,required:true},
    date:{type:Date,required:true},
    remindertype:{type:String,required:true},
})
module.exports=mongoose.model('Reminder',ReminderSchema);