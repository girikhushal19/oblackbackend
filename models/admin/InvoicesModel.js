const mongoose = require("mongoose");
const Schema = require("mongoose");

const InvoicesSchema = new mongoose.Schema({
  invoice_id_random: { type: String, default: null },
  shipment_id: [{ type: Schema.Types.ObjectId,ref:"shipments" }],
  user_id: [{ type: Schema.Types.ObjectId,ref:"user" }],
  shipment_price: { type: Number, default: 0 },
  tax_percent: { type: Number, default: 0 },
  tax_price: { type: Number, default: 0 },
  total_price: { type: Number, default: 0 },
  paid_status: { type: Number, default: 0 }, // 0 No action yet , 1 paid 
});


module.exports = mongoose.model("invoices", InvoicesSchema);