const mongoose=require("mongoose");
const RefundSchema=new mongoose.Schema({
    orderId:{type:String},
    products:{type:Array},
    vendor:{type:String},
    vendorId:{type:String},
    amount:{type:String},
    reason:{type:String},
    paymentgateway:{type:String},
    requestById:{type:String},
    requestByName:{type:String},
    status:{type:Boolean,default:false},
    is_rejected:{type:Boolean,default:false}
},
{
    timestamps:true
});
RefundSchema.statics.addOrupdate=async function(data){
    console.log("data",data)
    if(data.id){
        let refundOld=await this.updateOne({_id:mongoose.Types.ObjectId(data.id)},data);
        return refundOld;

    }else{
        let refund= this.create(data);
        return refund
    }
}
RefundSchema.statics.delete=async function(id){
  let deleted=await this.deleteOne({_id:mongoose.Types.ObjectId(data.id)})
  return deleted;
}
RefundSchema.statics.markAsDone=async function(id){
    let updated=await this.updateOne({_id:mongoose.Types.ObjectId(data.id)},{status:true})
    return updated;
}
RefundSchema.statics.markAsUnDone=async function(id){
    let updated=await this.updateOne({_id:mongoose.Types.ObjectId(data.id)},{status:false})
    return updated;
}
module.exports=mongoose.model("Refund",RefundSchema);