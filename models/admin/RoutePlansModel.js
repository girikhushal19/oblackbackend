const mongoose = require("mongoose");
const Schema = require("mongoose");
const RouteplansSchema = new mongoose.Schema({
  routeName: { type: String, default: null },
  driver_id:[{ type: Schema.Types.ObjectId,ref:"drivers" , default: null}],
  totalDistance: { type: Number, default: null },
  totalTime: { type: Number, default: null },
  totalHour: { type: Number, default: null },
  totalMin: { type: Number, default: null },
  totalStop: { type: Number, default: null },
  completedStop: { type: Number, default: null },
  missedStop: { type: Number, default: null },
  status: { type: Number, default: 0 }, //1 Assigned to driver 2 start 3 delivered 4 cancled
  trip_status:{ type: Number, default: 0 },//0 not assigned yet to driver  1 assigned to driver

  start_date: { type: String, default: null },
  start_time: { type: String, default: null },
  start_date_time: { type: String, default: null },
  routeDate: { type: Date, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now }
});
module.exports = mongoose.model("routeplans", RouteplansSchema);