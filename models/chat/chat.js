var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const chats = new mongoose.Schema(
    {
   
    message:String,
    images:Array, 
    by: String,
    senderId : String,
    senderName : String,
    statusc:{type:Boolean,default:false},
    statusp:{type:Boolean,default:false},
    senderProfile :String,
    recieverId : String,
    receiverName : String,
    receiverProfile : String,
    readByRecipients:Array,
    hide_chat_for_client:{type:Boolean,default:false},
    hide_chat_for_provider:{type:Boolean,default:false},
    },
    {
      timestamps: true,
    }
  );
var ChatSchema = new Schema({
    //senderID: {type: Schema.Types.ObjectId, ref: 'User'},
    //senderName: {type: String},
    //senderProfile: {type: String},
    //receiverID: {type: Schema.Types.ObjectId, ref: 'User'},
    //receiverName: {type: String},
    //receiverProfile: {type: String},
    //productID : {type: Schema.Types.ObjectId, ref: 'Product'},
    //productName : {type: String},
    //createdAt: {type: Date},
    //status: {type: String},
    //message : {type: String},
    //byMe : {type: String},
    //name : {type: String},
    hide_chat_for_client:{type:Boolean,default:false},
    hide_chat_for_provider:{type:Boolean,default:false},
    mainid :  {type: String},
    userID: {type: String},
    providerID: {type: String},
    roomId :  {type: String},
    users : {type : Array},
    chats : [chats],
    //countSender : {type : Number},
    count : {type : Number, default : 0},
    statusEventOne : {type : String ,default : null},
    statusEventTwo : {type : String ,default : null},
    statusDateFinal : {type : String ,default : null},
    status : {type : Boolean, default : false},
    updated_at: {
        type: Date
    }
    
});


module.exports = mongoose.model('chat', ChatSchema);
