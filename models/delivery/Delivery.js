const mongoose = require('mongoose');
const DeliverySchema = new mongoose.Schema({
   pickupdate:{type:Date},
   photos:{type:Array},
   pickuptime:{type:String},
   pointofpickup:{type:String},
   pointofdelivery:{type:String},
   latlongofpickup:{type:Object},
   numberofpackages:{type:Number},
   totalweight:{type:Number},
   maxlength:{type:Number},
   leplusheavy:{type:Number},
   productdesc:{type:String},
   primaryref:{type:String},
   failureofpickup:{
    status:{type:Boolean},
    reason:{type:String},
    desc:{type:String},
    photos:{type:Array},
    signature:{type:String}
}
})
module.exports = mongoose.model('Delivery', DeliverySchema);