const mongoose=require("mongoose");
const variationOptionSchema=new mongoose.Schema({
    option:{type:String,default:""}
})
const VariationsSchema=mongoose.Schema({
   variation_name:{type:String},
   product_id:{type:String},
   variation_option:{
    type:[variationOptionSchema],default:[]
   }

});
module.exports=mongoose.model("Variations",VariationsSchema);