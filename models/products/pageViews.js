const mongoose=require("mongoose");
const pageViewsSchema=mongoose.Schema({
   
   // product_id:{type:String,unique:true},
   provider_id:{type:String,unique:true},
   count:{type:Number,default:0},
   created_at:{type:Date,default:new Date()},
   updated_at:{type:Date,default:new Date()},
   
});
module.exports=mongoose.model("pageViews",pageViewsSchema);